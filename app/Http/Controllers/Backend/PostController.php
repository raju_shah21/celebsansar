<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Post;

class PostController extends Controller
{

	protected $view_path = "backend.newspost.";
	protected $folder = "post";
	protected $folder_path ;


		// sets folder path to store image
	public function __construct(){
	   $this->folder_path = public_path().DIRECTORY_SEPARATOR.'dashboard'.DIRECTORY_SEPARATOR.$this->folder.DIRECTORY_SEPARATOR ;
	 }


    public function index()
    {
        $posts = Post::all()->sortByDesc('id');
    	return view($this->view_path.'index',compact('posts'));
    }
    public function create()
    {
    	$categories = Category::pluck('category','id')->toArray();	// convert to array
    	// dd($categories);
    	return view($this->view_path.'create' , compact('categories'));
    }

    public function store(Request $request)
    {
    	
    	// dd($request->all());


    	if($request->hasFile('image')){


    	        

    	         $image_obj = $request->file('image');
    	         $image_name = rand(1000,9000).'_'.$image_obj->getClientOriginalName();
    	         $image_obj->move($this->folder_path , $image_name);

    	         // resize image 100*100 | 500*500 using config braodway dynamically

    	         // foreach ($this->main_image_dimensions as $dimension) {
    	         //   $img = Image::make($this->folder_path.DIRECTORY_SEPARATOR.$image_name)->resize($dimension['width'], $dimension['height']);
    	         //   $img->save($this->folder_path.DIRECTORY_SEPARATOR.$dimension['width'].'_'.$dimension['height'].'_'.$image_name);
    	         // }


    	         $request->request->add([
    	           'feature_image' => $image_name ,
    	         ]);

                  // dd($request->all());


    	}

    	$post = Post::create($request->all());

        //------------------categories upload =-----------------------
        $post->categories()->attach($request->get('category'));
// 

    	return redirect()->route('newspost.show')->with('success_message','NewsPost Created Successfully');

    }




    public function edit($id)
    {
        $row = Post::find($id);
        $row['post_categories'] = $row->categories;
        //dd($row['post_categories']);
        $row['category'] = Category::select('id','category')->get()->pluck('category','id') ;

       return view($this->view_path.'.edit',compact('row'));
        // dd($row);
    }


    public function update(Request $request , $id)
    {
        //dd($request->all());
        $row = Post::find($id);
            // check if new image is selected
            if($request->hasFile('new_image')){

                 $image_obj = $request->file('new_image');
                 $image_name = rand(1000,9000).'_'.$image_obj->getClientOriginalName();
                 $image_obj->move($this->folder_path , $image_name);



                 $request->request->add([
                   'feature_image' => $image_name ,
                 ]);

                    // delete previous image
                 if($row->feature_image){

                        if(file_exists($this->folder_path.$row->feature_image)){

                            unlink($this->folder_path.$row->feature_image);
                                  
                        }

                  }

            }// image block if end

                  // dd($request->all());

            $row->update($request->all());


            //$row->categories()->attach($request->get('category'));        // never use this doesn't work
            $row->categories()->sync($request->get('category'));



            // dd($row);
            // dd($row->categories);

            $request->session()->flash('success_message' , 'NewsPost Updated Successfully');
            return redirect()->route('newspost.show');
    }

    public function delete(Request $request , $id)
    {
        $row = Post::find($id);
        // dd($row);


        if($row->feature_image){        // remove image

           if(file_exists($this->folder_path.$row->feature_image)){

               unlink($this->folder_path.$row->feature_image);
                     
           }

         }

         $row->categories()->sync([]);      // remove pivot table product_categories

        $row->delete();                         // row delete

        $request->session()->flash('success_message' , 'NewsPost Deleted Successfully');
        return redirect()->route('newspost.show');
    }



}
