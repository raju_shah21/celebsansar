<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use LaravelVideoEmbed;
use Image;

class VideoController extends Controller
{

    protected $base_path = "backend.video.";
    protected $folder = "videos";
	protected $folder_path ;


	// sets folder path to store image
	public function __construct(){
	   $this->folder_path = public_path().DIRECTORY_SEPARATOR.'dashboard'.DIRECTORY_SEPARATOR.$this->folder.DIRECTORY_SEPARATOR ;
    }
     

    public function index()
    {
        // $url = "https://youtu.be/CHh-Zj70NS8";
        // return LaravelVideoEmbed::getYoutubeThumbnail($url);
        $rows = Video::all();
    	return view($this->base_path.'index',compact('rows'));
    }

    public function create()
    {
        return view($this->base_path.'create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        //  validate
        $request->validate([
            'title' => 'required|string|max:200',
            'url' => 'required',
            'body' => 'nullable',
            'image' => 'nullable',
        ]);

        // image store
        if($request->hasFile('video_image')){

            $image_obj = $request->file('video_image');
            $image_name = rand(1000,9000).'_'.$image_obj->getClientOriginalName();
            $image_obj->move($this->folder_path , $image_name);

            $request->request->add([
                'image' => $image_name ,
            ]);

        }else{

           $image_src = LaravelVideoEmbed::getYoutubeThumbnail($request->get('url'));
            $filename = rand(1000,9000).'_'.basename($image_src);
            // dd($filename);
            Image::make($image_src)->save(public_path('dashboard/videos/' . $filename));


            $request->request->add([
                'image' => $filename ,
            ]);
         
        }

        // dd($request->all());
        // row inserted into db
        Video::create($request->all());

    	return redirect()->route('video')->with('success_message','Video Added Successfully');

    }


    public function edit($id)
    {
        $row = Video::findOrfail($id);
        return view($this->base_path.'edit',compact('row'));
    }

    public function update(Request $request , $id)
    {
        // dd($request->all());
        $row = Video::findOrfail($id);
        $request->validate([
            'title' => 'required|string|max:200',
            'url' => 'required',
            'body' => 'nullable',
            'image' => 'nullable',
        ]);

        if($request->hasFile('video_image')){

            $image_obj = $request->file('video_image');
            $image_name = rand(1000,9000).'_'.$image_obj->getClientOriginalName();
            $image_obj->move($this->folder_path , $image_name);

            $request->request->add([
                'image' => $image_name ,
            ]);

               // delete previous image
                 if($row->image){

                        if(file_exists($this->folder_path.$row->image)){

                            unlink($this->folder_path.$row->image);
                                  
                        }

                  }

        }
        
        // if url is changed new thumbnai is generated or not
        if($row->url != $request->get('url')){
            if(!$request->hasFile('video_image')){
                
                     if($row->image){

                        if(file_exists($this->folder_path.$row->image)){

                            unlink($this->folder_path.$row->image);
                                  
                        }

                    }
                    $image_src = LaravelVideoEmbed::getYoutubeThumbnail($request->get('url'));
                    $filename = rand(1000,9000).'_'.basename($image_src);
                    Image::make($image_src)->save(public_path('dashboard/videos/' . $filename));


                    $request->request->add([
                        'image' => $filename ,
                    ]);
            }
        }

        $row->update($request->all());
         return redirect()->route('video')->with('success_message','Video Updated Successfully');

    }

    public function delete(Request $request , $id)
    {
        $row = Video::find($id);

        if($row->image){        // remove image

           if(file_exists($this->folder_path.$row->image)){

               unlink($this->folder_path.$row->image);
                     
           }

        }
        $row->delete();                         // row delete

        $request->session()->flash('success_message' , 'Video Deleted Successfully');
        return redirect()->route('video');
    }


}
