<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use App\Category;

class DashboardController extends Controller
{
    public function index()
    {
    	$post_count = Post::count();
    	$user_count = User::count();
    	$category_count = Category::count();
    	return view('backend/dashboard' , compact('post_count','user_count','category_count'));
    }
}
