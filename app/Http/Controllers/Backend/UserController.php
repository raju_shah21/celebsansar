<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{


    protected $view_path = "user." ;
    protected $folder_path = "backend.user.";



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view($this->folder_path.'index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $password = bcrypt($request->get('password'));
        $user = User::create([

            'firstname' => $request->get('firstname') ,
            'lastname' => $request->get('lastname') ,
            'email' => $request->get('email') ,
            'password' => $password 

        ]);

        $request->session()->flash('success_message' , 'News User Created Successfully');
        return redirect()->back();
    }

   
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $row = User::find($id);
        // dd($row);
         return view('backend/user/edit',compact('row'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row = User::find($id);

        // dd($row);
        // dd($request->all());

        $row->firstname = $request->get('firstname') ;
        $row->lastname = $request->get('lastname') ;
        $row->email = $request->get('email') ;
        $row->password = $request->get('password') == '' ? $row->password :
                                         bcrypt($request->get('password')) ;


        $row->save();

        request()->session()->flash('success_message' , 'User Updated Successfully');
        return redirect()->route($this->view_path.'show');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $row = User::find($id);
        // dd($row);
        $row->delete();
        request()->session()->flash('success_message' , 'User Deleted Successfully');
        return redirect()->route($this->view_path.'show');
    }
}
