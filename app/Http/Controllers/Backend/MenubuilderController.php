<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\MenuBuilder;

class MenubuilderController extends Controller
{
    
    protected $view_path = 'backend.menubuilder.';
    protected $base_route = 'menubuilder';




    public function index()
    {
        // $categories = Category::all()->pluck('category','id');
    	$categories = Category::select('category','id')->get();
    	// dd($categories);
           $menus = MenuBuilder::select('title','id')->where('status',1)->orderBy('position')->get();
    	return view($this->view_path.'index',compact('categories','menus'));
    }

    public function add(Request $request)
    {

         $response = [];

        $rows=[];
       

        $rows = Category::find($request->get('category') , ['id','category','slug']);
        // dd($rows);

        $newMenu=[];

        foreach ($rows as $key => $row) {

            // check if that category is already stored in menubuilder if yes skipp it
            $check = MenuBuilder::where('title','=',$row->category)->first();  

            if(!$check){

                 $newMenu = new MenuBuilder();
                 $newMenu->title = $row->category;
                 $newMenu->slug = $row->slug;
                 $count = MenuBuilder::count();
                 // dd($count);
                 $newMenu->position = ++$count*100;
                 $newMenu->save();


                 $row->id = $newMenu->id;
                 //dd($newMenuId[$key]);

            }else{
                 $rows->forget($key);           // remove that category which already exists in menubuilder
                 $response['already_exists'] = "Category is already in MenuBuilder";
            }
               // dd($rows);

        }

        $response['success'] = "nicely done";

         $response['html'] = view("backend.menubuilder.ajax.demo")->with(compact('rows',$rows))->render(); 
                     // view returns object , render html

         return response()->json(json_encode($response));   
    }


    public function store(Request $request)
    {
        // dd(request()->get('page'));
        if($request->has('page'))
        {
            $a=0;
            for($i=0 ; $i < count($request->get('page')) ; $i++ )
            {
                $a+=100;
                $row =  MenuBuilder::find($request->get('page')[$i]);
                $row->position = $a;
                $row->save();
            }
        }
        return response()->json(['success'=>'yep']);
    }


    public function delete($id)
    {
        $row = MenuBuilder::find($id);
        $row->delete();
        request()->session()->flash('success_message' , 'MenuBuilder Deleted Successfully');
             return redirect()->route($this->base_route );
    }


}
