<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{

    public function index()
    {
    	$categories = Category::all();
    	// dd($categories);
    	return view('backend/category/index',compact('categories'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'category'=>'required',
            'slug'=>'required'
        ]);
        $category = new Category();
        $category->category = $request->get('category');
        $category->slug = str_slug($request->get('slug'));
        $category->save();
    	$request->session()->flash('success_message' , 'Category Saved Successfully');
        return redirect()->route('category.show');
    	

    }

    public function edit($id)
    {
        $row = Category::find($id);
       // dd($row);
        return view('backend/category/edit',compact('row'));
    }


     public function update(Request $request , $id)
    {
        // dd($request->all());
        $row = Category::find($id);
         $request->validate([
            'category'=>'required',
            'slug'=>'required'
        ]);
        $row->category = $request->get('category');
        $row->slug = str_slug($request->get('slug'));
        $row->save();
        request()->session()->flash('success_message' , 'Category Updated Successfully');
        return redirect()->route('category.show');     
    }


     public function delete($id)
    {
        $row = Category::find($id);
        // dd($row);
        $row->delete();
        request()->session()->flash('success_message' , 'Category Deleted Successfully');
        return redirect()->route('category.show');
    }
}
