<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;

class BannerController extends Controller
{

    protected $base_path = "backend.banner.";
    // protected $folder = "videos";
	// protected $folder_path ;


	// sets folder path to store image
	public function __construct(){
	   $this->folder_path = public_path().DIRECTORY_SEPARATOR.'photos'.
                            DIRECTORY_SEPARATOR.'1'.DIRECTORY_SEPARATOR.
                            'banner'.DIRECTORY_SEPARATOR ;
    }
     
    
    public function index()
    {
        $headerBanner = Banner::where('position',"=","header")->get();

        $sliderLongBanner = Banner::where('position',"=","slider")->where('type','long')->get();
        $sliderShortBanner = Banner::where('position',"=","slider")->where('type','short')->get();

        $sectionFirstBanner = Banner::where('position',"=","section-1")->where('type','long')->get();

        $sectionSecondLongBanner = Banner::where('position',"=","section-2")->where('type','long')->get();
        $sectionSecondShortBanner = Banner::where('position',"=","section-2")->where('type','short')->get();

        $sectionThirdBanner = Banner::where('position',"=","section-3")->where('type','short')->get();

        $sectionFourthBanner = Banner::where('position',"=","section-4")->where('type','long')->get();


        // dd($headerBanner);
        return view($this->base_path.'index',compact('headerBanner','sliderLongBanner','sliderShortBanner',                        'sectionFirstBanner','sectionSecondLongBanner','sectionSecondShortBanner','sectionThirdBanner','sectionFourthBanner'));
    }


    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'banner_image'=>'required',
        ]);

        // image store
        if($request->hasFile('banner_image')){

            $image_obj = $request->file('banner_image');
            $image_name = rand(1000,9000).'_'.$image_obj->getClientOriginalName();
            $image_obj->move($this->folder_path.$request->get('position') , $image_name);

            $request->request->add([
                'image' => $image_name ,
            ]);

        }

        Banner::create($request->all());
        $request->session()->flash('success_message' , 'Banner Saved Successfully');
        return redirect()->route('banner');
    }



    public function changeStatus(Request $request)
    {
        $banner = Banner::find($request->get('id'));
        if($request->get('name') == 'live'){
            $banner->status = 1;
        }else{
            $banner->status = 0;
        }
        $banner->save();
        return response()->json(['success'=>'yep']);
    }


    public function linkSave(Request $request)
    {
        // dd($request->all());
        $banner = Banner::find($request->get('id'));
        $banner->link = $request->get('link');
        $banner->save();
        return response()->json(['success'=>'yep']);
    }


    public function update(Request $request)
    {
        dd($request->all());
    }



}
