<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    

    public function showRegistrationForm()
    {
        return view('backend/auth/register');
    }


    public function register(Request $request)
    {
        // dd($request->all());

        $request->validate([
                'firstname' => 'required|max:200',
                'lastname' => 'required|max:200',
                'email' => 'required|email|unique:users|max:200',
                'password' => 'required'
            ]);
         User::create([            
               'firstname' => $request['firstname'],
               'lastname' => $request['lastname'],            
               'email' => $request['email'],            
               'password' => bcrypt($request['password']),            
           ]);  
        return view('backend/auth/login');
    }
}
