<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Video;
use App\Banner;
use App\MenuBuilder;
use NepaliCalendar;
use LaravelVideoEmbed;


class FrontController extends Controller
{

    protected $view_path = 'frontend.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // dynamic navbar
        $menus = MenuBuilder::select('title','slug')->where('status',1)->orderBy('position')->get();




        //dd(Helper::sample_helper());
        // $date = \Carbon\Carbon::now()->format('d.m.Y');
        $nc = new NepaliCalendar();

        // dd(NepaliCalendar::today()); 
        // dd($nc->currentDate()); 

        //$rows = Category::where('slug',"=","news")->get();      // don't return collection like find rather array type
        $rows = Category::where('slug',"=","news")->first();      //  return model collection  easy
          // dd($rows[0]);
        $newsposts = $rows->posts()->take(3)->get();                             // collection array from pivot table
         // dd($newsposts);



        $rows = Category::where('slug',"=","tvnews")->first();      //  return model collection  easy
        // dd($rows[0]);
        $tvnewsposts = $rows->posts()->take(8)->get()->toArray();                             // collection array from pivot table
       // dd($tvnewsposts);
      

        $interview = Category::where('slug',"=","interview")->first();      //  return model 
        $interviewposts_news = $interview->posts()->take(3)->get();   

        //dd($interviewposts);


        //  slider post
        $slidercategory = Category::where('slug',"=","slider")->first();
        $sliderposts = $slidercategory->posts()->latest()->take(5)->get();

        // dd($sliderposts);



        // for event model section

        $eventcategory = Category::where('slug',"=","event")->first();
        $eventposts = $eventcategory->posts()->take(3)->get();

        $modelcategory = Category::where('slug',"=","beauty")->first();
        $modelposts = $modelcategory->posts()->take(1)->get();




        // for discussion section

        $literaturecategory = Category::where('slug',"=","literature")->first();
        $literatureposts = $literaturecategory->posts()->take(1)->get();

        $interviewcategory = Category::where('slug',"=","interview")->first();
        $interviewposts = $interviewcategory->posts()->take(5)->orderBy('id','desc')->get();
        // dd($interviewposts);
         // for discussion section



        // for video  section
        //$tvnewscategory = Category::where('slug',"=","tvnews")->first();
       // $tvnewsposts = $tvnewscategory->posts()->take(2)->get();
        $videos = Video::take(5)->latest()->get()->toArray();
        // dd($videos);
        // for video  section
        




        // for literature last section
        $foodcategory = Category::where('slug',"=","food")->first();
        $foodposts = $foodcategory->posts()->take(3)->get();
        //dd($foodposts);


        //  for banner
        $headerBanner = Banner::where('status',1)->where('position','header')->first();
        // dd($headerBanner);

        $sliderLongBanner = Banner::where('status',1)->where('position','slider')->where('type','long')->take(2)->get()->toArray();
        $sliderShortBanner = Banner::where('status',1)->where('position','slider')->where('type','short')->take(2)->get()->toArray();

        $sectionFirstBanner = Banner::select()->where('status',1)->where('position','section-1')->take(1)->get();

        $sectionSecondLongBanner = Banner::select()->where('status',1)->where('position','section-2')->where('type','long')
                                                        ->take(1)->get();
        $sectionSecondShortBanner = Banner::select()->where('status',1)->where('position','section-2')->where('type','short')
                                                        ->take(1)->get();

        $sectionThirdBanner = Banner::select()->where('status',1)->where('position','section-3')->where('type','short')
                                                        ->take(3)->get();     

         $sectionFourthanner = Banner::select()->where('status',1)->where('position','section-4')->where('type','long')
                                                        ->take(1)->get();        
                                                
        //$headerBanner = Banner::select()->where('status',1)->where('position','header')->get();
        //$headerBanner = Banner::select()->where('status',1)->where('position','header')->get();
        // dd($sliderLongBanner);




        return view($this->view_path.'index' , compact('menus','newsposts','tvnewsposts','interviewposts_news','sliderposts','eventposts' , 'modelposts' , 'literatureposts' , 'interviewposts' , 'foodposts' , 'videos','nc'))
            ->with(compact('headerBanner','sliderLongBanner','sliderShortBanner','sectionFirstBanner','sectionSecondLongBanner','sectionSecondShortBanner','sectionThirdBanner','sectionFourthanner'));
    }
 

    public function listCategoryPosts($slug)
    {

         // dynamic navbar
        $menus = MenuBuilder::select('title','slug')->where('status',1)->orderBy('position')->get();

        // dd($slug);
        $category = Category::where("slug","=" ,$slug)->first();
        $posts = $category->posts()->latest()->paginate(3);
        // dd($posts);

          $nc = new NepaliCalendar();            // to display nepali date

        return view($this->view_path.'category' , compact('posts','nc','menus'));
    }

   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         // dynamic navbar
        $menus = MenuBuilder::select('title','slug')->where('status',1)->orderBy('position')->get();
        $row = Post::find($id);
         $nc = new NepaliCalendar();            // to display nepali date
        return view($this->view_path.'singlepost' , compact('row','nc','menus'));
    }

    
    public function showVideo($id)
    {
        $row = Video::find($id);
         // dynamic navbar
        $menus = MenuBuilder::select('title','slug')->where('status',1)->orderBy('position')->get();
          $nc = new NepaliCalendar();

          $params = [
            'autoplay' => 1,
            'loop' => 1,
        ];

        $iframe = LaravelVideoEmbed::parse($row->url);

        return view($this->view_path.'video',compact('row','menus','nc','iframe'));
    }

  
}
