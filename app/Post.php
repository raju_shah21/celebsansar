<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = ['title','subtitle','author','highlight','paragraph','feature_image'];



    public function categories()
       {
           return $this->belongsToMany('App\Category');
       }
}
