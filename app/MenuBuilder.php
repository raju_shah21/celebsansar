<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuBuilder extends Model
{
	 protected $table = 'menu_builders';
     protected $fillable = ['id','title','slug'];
}
