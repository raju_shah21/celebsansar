<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});


Route::get('/' , [
	'as' => 'front' ,
	'uses' => 'FrontController@index'
]);
Route::get('/post/{id}' , [
	'as' => 'post' ,
	'uses' => 'FrontController@show'
]);
Route::get('/video/{id}' , [
	'as' => 'video.view' ,
	'uses' => 'FrontController@showVideo'
]);
Route::get('/category/{slug}' , [
	'as' => 'category' ,
	'uses' => 'FrontController@listCategoryPosts'
]);






	// prefix for url
	// namespace for controller location
	// middleware auth without login can't access these routes

Route::group([ 'prefix' => 'backend' , 'namespace'=>'Backend' ,'middleware' => 'auth'], function(){


		// for dashboard

		Route::get('/',[
			'as'=>'dashboard',
			'uses'=>'DashboardController@index'
		]);


		// for news post
		Route::get('newspost',[
			'as'=>'newspost.show',
			'uses'=>'PostController@index'
		]);
		Route::get('newspost/create',[
			'as'=>'newspost.create',
			'uses'=>'PostController@create'
		]);

		Route::post('newspost/create',[
			'as'=>'newspost.store',
			'uses'=>'PostController@store'
		]);


		Route::get('newspost/edit/{id}',[
			'as'=>'newspost.edit',
			'uses'=>'PostController@edit'
		]);
		Route::post('newspost/update/{id}',[
			'as'=>'newspost.update',
			'uses'=>'PostController@update'
		]);

		Route::get('newspost/delete/{id}',[
			'as'=>'newspost.delete',
			'uses'=>'PostController@delete'
		]);








		//  for category
		Route::get('category',[
			'as'=>'category.show',
			'uses'=>'CategoryController@index'
		]);
		Route::get('category/create',[
			'as'=>'category.create',
			'uses'=>'CategoryController@create'
		]);
		Route::post('category/store',[
			'as'=>'category.store',
			'uses'=>'CategoryController@store'
		]);
		Route::get('category/edit/{id}',[
			'as'=>'category.edit',
			'uses'=>'CategoryController@edit'
		]);
		Route::post('category/update/{id}',[
			'as'=>'category.update',
			'uses'=>'CategoryController@update'
		]);
		Route::get('category/delete/{id}',[
			'as'=>'category.delete',
			'uses'=>'CategoryController@delete'
		]);



		// for user
		Route::get('user',[
			'as'=>'user.show',
			'uses'=>'UserController@index'
		]);

		Route::post('user/store',[
			'as'=>'user.store',
			'uses'=>'UserController@store'
		]);

		Route::get('user/edit/{id}',[
			'as'=>'user.edit',
			'uses'=>'UserController@edit'
		]);

		Route::post('user/update/{id}',[
			'as'=>'user.update',
			'uses'=>'UserController@update'
		]);
		Route::get('user/delete/{id}',[
			'as'=>'user.delete',
			'uses'=>'UserController@delete'
		]);





		// for menubuilder
		Route::get('menubuilder',[
			'as' => 'menubuilder',
			'uses' => 'MenubuilderController@index'
		]);
		Route::post('menubuilder',[
			'as' => 'menubuilder.add',
			'uses' => 'MenubuilderController@add'
		]);
		Route::post('menubuilder/store',[
					'as' => 'menubuilder.store',
					'uses' => 'MenubuilderController@store'
		]);

		Route::get('menubuilder/delete/{id}',[
			'as' => 'menubuilder.delete' ,
			'uses' => 'MenubuilderController@delete'
		]);




		Route::get('video',[
			'as' => 'video',
			'uses' => 'VideoController@index'
		]);
		Route::get('video/create',[
			'as' => 'video.create',
			'uses' => 'VideoController@create'
		]);

		Route::post('video',[
			'as' => 'video.store',
			'uses' => 'VideoController@store'
		]);

		Route::get('video/edit/{id}',[
			'as'=>'video.edit',
			'uses'=>'VideoController@edit'
		]);

		Route::post('video/update/{id}',[
			'as'=>'video.update',
			'uses'=>'VideoController@update'
		]);
		Route::get('video/delete/{id}',[
			'as'=>'video.delete',
			'uses'=>'VideoController@delete'
		]);



		// for banner ad manager 
		Route::get('banner',[
			'as' => 'banner',
			'uses' => 'BannerController@index'
		]);

		Route::post('banner',[
			'as'=>'banner.store',
			'uses'=>'BannerController@store'
		]);

		// not completed
		Route::post('banner/update',[
			'as'=>'banner.update',
			'uses'=>'BannerController@update'
		]);

		// change live and  offline status
		Route::get('live',[
			'as'=>'status.change',
			'uses'=>'BannerController@changeStatus'
		]);

		Route::get('link/save',[
			'as'=>'link.save',
			'uses'=>'BannerController@linkSave'
		]);




});

// backend/dashboard group routes end



    Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');
