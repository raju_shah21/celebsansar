<?php

use Illuminate\Database\Seeder;

class MenuBuilderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$data = [

    		[
    			'title' => 'राजनीति' ,
               'position' => 100 ,
               'status' => 1 ,
               'slug' => 'politics'
    		],
    		[
    			'title' => 'फेसन' ,
               'position' => 200 ,
               'status' => 1 ,
               'slug' => 'fashion'
    		],
    		[
    			'title' => 'जिबनसैली' ,
               'position' => 300 ,
               'status' => 1 ,
               'slug' => 'lifestyle'
    		],
    		[
    			'title' => 'अन्तर्वार्ता' ,
               'position' => 400 ,
               'status' => 1 ,
               'slug' => 'interview'
    		],
    		[
    			'title' => 'फिट्नेस' ,
               'position' => 500 ,
               'status' => 1 ,
               'slug' => 'fitness'
    		],
    		[
    			'title' => 'खानपान' ,
               'position' => 600 ,
               'status' => 1 ,
               'slug' => 'food'
    		]
    		

    	];

    	foreach ($data as $row){
	         DB::table('menu_builders')->insert([
	            'title' =>  $row['title'],
	            'slug' =>   $row['slug'],
	            'status' =>   $row['status'],
	            'position' =>  $row['position'],
	            'created_at' => \Carbon\Carbon::now() ,
	            'updated_at' => \Carbon\Carbon::now()
	          ]);
	    }
    }
}
