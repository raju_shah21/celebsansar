@extends('layouts.app')

@section('title', $row->title )

@section('meta')

		<link rel="canonical" href="{{ Request::url() }}" />
		
		<meta property="fb:app_id" content="2728799067345818"/>
		<meta property="og:url"           content="{{ Request::url() }}" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="{{$row->title}}" />
		<meta property="og:description"   content="{{$row->highlight}}" />
		<meta property="og:image"         content="{{url('dashboard/post/'.$row->feature_image)}}" />

@endsection

	@section('content')

		<!-- 				 for fb comment 			-->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=2728799067345818&autoLogAppEvents=1';
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<!-- 				for fb comment 				-->

		<!-- date and social icons -->
        <section class="bg-primary" id="header">
          <div class="container">
             <i class="fa-2x far fa-calendar-alt"></i>
             <iframe scrolling="no" border="0" frameborder="0" marginwidth="0" marginheight="0" allowtransparency="true" src="https://www.ashesh.com.np/linknepali-time.php?dwn=only&font_color=fff&font_size=15&bikram_sambat=0&api=7221z8i366" width="165" height="22" style="padding-top: 3px"></iframe>

             <div class="float-right">
                 <i class="fab fa-twitter"></i>
                 <i class="fab fa-facebook-f"></i>
                 <i class="fab fa-youtube"></i>
                 <i class="fab fa-instagram"></i>
             </div>
           </div>
        </section>

              <!-- every section is in this container -->
        <div class="container" id="single-post-page-view">



				              <!-- 1. first banner ad  and logo-->
	
	      <section class="pt-2 pb-2" id="logo-ad">

	                 <div class="row d-flex">
	                   <div class="col-md my-col">
	                   		<a href="{{ route('front') }}">
	                     		<img src="{{ asset('img/logo.png') }}" alt="">
	                     	</a>
	                   </div>
	                   <div class="col-md my-col">
	                     <img src="http://www.khabarpauwa.com/upload/becf90694322f7faa8320172b1b9df18.gif" class="ml-auto img-fluid">
	                   </div>
	                 </div>
	                    
	             </section>


					<!-- navbar -->
	       		@include('frontend.partials.navbar')




						<!-- 2. second banner ad -->
	        <section class="mx-auto mt-4 mb-4 long-ad">
	            <div class="text-center">
	           	 <img src="https://img.setopati.org/uploads/bigyapan/1541915889.gif" alt="" class="img-fluid">
	            </div>
	        </section>




			<div id="news-big-title">
				<h1>{{ $row->title }}</h1>
			</div>


	        
	        <section class="single-news-post">

	          <div class="row">

	          	<! -- news content image share box and comment box -->
	            <div class="col-xl-8 col-lg-12">

					<!-- container share news -->
	            	<div class="sharenews mb-5 rounded-bottom">
	            		<div class="row">
	            			<div class="col-md container">
	            				<div class="media">
            				        <div class="media-body">
            				            <span class="main-title">
            				                 <a class="text-primary">
            				                     सेलेवसंसार संवाददाता
            				                 </a>
            				                 <br>
            				                 </span> <span class="designation text-muted">काठमाडौं, 
														@php 
														 $result = $nc->eng_to_nep($row->created_at->year,$row->created_at->month,$row->created_at->day);
														 print_r($result["month_name"] . " " . $result["date"] ." " .$result["year"]);
	 													@endphp
            				                 </span>
            				        </div>
	            				</div>
	            			</div>
	            			<div class="col-md  mt-2">
	            				<ul class="list-inline text-center">

	            					<li class="list-inline-item ml-auto"><h2 class="text-muted">Share</h2></li>

	            					<li class="list-inline-item bg-primary">
		            					<a href="https://twitter.com/intent/tweet?url={{ urlencode(Request::fullUrl()) }}"
		            					    target="_blank">
		            					   <i class="fab fa-twitter"></i>
		            					</a>
	            					</li>
	            					<li class="list-inline-item bg-primary">
		            					<a href="https://plus.google.com/share?url={{ urlencode(Request::url()) }}"
		            					    target="_blank">
		            					   	<i class="fab fa-google-plus-g"></i>
		            					</a>
	            					</li>

	            					<li class="list-inline-item bg-primary">

		            					<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}"
		            					    target="_blank">
		            					    <i class="fab fa-facebook-f"></i>
		            					</a>

	            					</li>
	            				

	            					<!--

	            						add this plugin 
	            					<iframe src="https://www.facebook.com/plugins/like.php?href={{ Request::url()}}&width=51&layout=box_count&action=like&size=small&show_faces=true&share=true&height=65&appId" 
	            					width="51" height="65" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

	            					-->
	            				</ul>
	            			</div>
	            		</div>
	            	</div>
					<!-- container share news -->



	                <!--         single news item image start      -->
	                <figure>
	                	@if(file_exists(public_path('/dashboard/post/'.$row->feature_image)))
		                <img src="{{ asset('dashboard/post/'.$row->feature_image) }}" alt="Post Image" class="img-fluid">
		                @else
		                <img src="{{ asset('dashboard/post/default.png') }}" alt="1140x725" class="img-fluid">
						@endif
		                <figcaption class="caption-line">
		                </figcaption>
	                </figure>
	                    <!--         single news item image end      -->

					
						<! -- news body paragraph -->
	                 <div class="news-body p-5">
						<p style="text-align: justify;">
							{!! $row->paragraph !!}
						</p>
	                 </div>

	                 <div class="comment-on-news text-center mb-5">
	                 	<span id="comment">प्रतिक्रिया दिनुहोस्</span>

	                 </div>




	                 <!-- facebook comment plugin -->


	                 <!-- <div class="fb-comments mx-auto" data-href="{{ urlencode(Request::url()) }}" data-numposts="5"></div> -->
					<div class="fb-comments mx-auto" data-href="{{ Request::url() }}" data-numposts="5"></div>
	                <!--  
	                   <div class="fb-comments mx-auto" data-href="{{ urlencode(Request::fullUrl()) }}" data-numposts="5"></div> -->

	                 <!-- facebook comment plugin -->


	            </div>


	            <div class="col-xl-3 col-md-6 offset-xl-1 ">

	            	<div class="right-side-news">
		            	<div class="text-center" id="border-muted">
		                 	<span class="mb-4 sidebar-heading">ताजा समाचार</span>
		                 </div>
		                 <ul class="list-unstyled text-center">
		                 	<li>
		                 		<a href="">
		                 			<span class="media-body">
										<span class="main-title">पर्यटनमन्त्री रविन्द्र अधिकारीसहित, दुई पूर्वमन्त्रीलाई कारबाही गर्न सिफारिस</span>
									</span>
								</a>
							</li>
		                 	<li>
		                 		<a href="">
		                 			<span class="media-body">
										<span class="main-title">फेडररले सेरेना विलियम्सलाई हराए</span>
									</span>
								</a>
							</li>
		                 	<li>
		                 		<a href="">
		                 			<span class="media-body">
										<span class="main-title">फेडररले सेरेना विलियम्सलाई हराए</span>
									</span>
								</a>
							</li>
		                 	<li>
		                 		<a href="">
		                 			<span class="media-body">
										<span class="main-title">फेडररले सेरेना विलियम्सलाई हराए</span>
									</span>
								</a>
							</li>
		                 </ul>
		            </div>


		            <div id="single-page-side-ad" class="d-flex flex-column justify-content-center">
		               <img src="https://img.setopati.org/uploads/bigyapan/1534756685.gif" alt="" class="short-ad mb-3">
		               
		               <img src="https://img.setopati.org/uploads/bigyapan/1534756685.gif" alt="" class="short-ad">
		            </div>
	 
	            </div>
	          </div>       

	        </section>





	        <section class=" mt-4 mb-4 long-ad">
	            <div class="text-center">
	            <img src="https://www.janatapati.com/uploads/advertisements/1535688906.gif" alt="" class="img-fluid">
	            </div>
	        </section>
















	        <section class="mx-auto m-3 long-ad">
	            <div class="text-center">

	            <img src="https://www.janatapati.com/uploads/advertisements/1545811308.gif" alt="" class="img-fluid">
	            </div>
	        </section>






	        <section class="mx-auto m-3 long-ad">
	          <div class="text-center">
	            <img src="https://img.setopati.org/uploads/bigyapan/1541915889.gif" alt="" class="img-fluid">
	          </div>
	        </section>




	    

	        <section class="mx-auto m-3 long-ad">
	            <div class="text-center">

	                <img src="https://www.janatapati.com/uploads/advertisements/1545811308.gif" alt="" class="img-fluid">
	            </div>
	        </section>






	   










	      






	     




        </div>  <!-- container end -->


	 
		<!-- footer -->
		@include('frontend.partials.footer')

	


	<!-- social plugin addthis -->

	<!-- Go to www.addthis.com/dashboard to customize your tools
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c4966ac81e209ee"></script>
	 -->

	 <!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>


	@endsection




      

