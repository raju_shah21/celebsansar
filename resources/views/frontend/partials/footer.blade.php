
	 	<section class="footer p-5">
	        <div class="container">
	          <div class="row d-flex justify-content-center">
	            <div class="col-lg-8 col-md-12 my-col">
	              <div class="first-box footer-box">

	                      <div class="mb-3 text-center border-muted">
	                          <i class="left-icon fas fa-envelope"></i>
	                          <span class="footer-heading">सम्पर्क</span>
	                      </div>
	                    
	                    	<div class="text-center text-primary">
	                         <img src="{{ asset('img/logo.png') }}" class="pb-3 img-fluid">


	                         <br><br>		                        
		                      
		                        <span class=" text-center"><h4><b>राईटपाथ बिजिनेस नेटवर्क प्रा‍. लि.</b></h4></span>
		                     
		                        <span class="description">
		                        	नयाँबानेश्वर ,
		                        	 काठमाडौँ , नेपाल
		                            <br>
		                            सम्पर्क नम्बर : &nbsp;०१४४२७५०१ ,&nbsp;
		                           ९८६०४९९००५
		                        </span>

			                </div>
	                       
	               </div>
	            </div>
	        
	            <div class="col-lg-4 col-md-12 my-col">
	              <div class="third-box footer-box">
	                  <div class="text-center mb-3 border-muted">
	                      <i class="fas fa-users"></i><span class="footer-heading">हाम्रो टिम </span>
	                     
	                  </div>
	                     <ul class="list-unstyled text-center">
	                        
	                          <li>
	                              <span class="media-body">
	                                <span class="main-title">प्रकाशक</span>
	                                <span class="description">
	                                 शोभा सापकोटा
	                                </span>
	                              </span>
	                          </li>
	                          <li>
	                              <span class="media-body">
	                                <span class="main-title">सम्पादक/संचालक</span>
	                                <span class="description">
	                                  आश्मा बम ( मो. ९८६०४९९००५)
	                                </span>
	                              </span>
	                          </li>
	                          <li>
	                              <span class="media-body">
	                                <span class="main-title">सम्वाददाता </span>
	                                <span class="description">
	                                  गीता थापा / सम्रिद्धा के सी
	                                </span>
	                              </span>
	                          </li>
	                          <li>
	                              <span class="media-body">
	                                <span class="main-title">संयोजक (बजार ब्यबस्थापक )</span>
	                                <span class="description">
	                                 शान्ति शर्मा ( मो. ९८६०१०३३२५ )
	                                </span>
	                              </span>
	                          </li>
	                         
	                     </ul>
	                </div>
	            </div>
	          </div>
	        </div>
	      </section>

