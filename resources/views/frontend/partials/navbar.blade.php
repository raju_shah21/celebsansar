 <nav class="navbar navbar-expand-lg navbar-light bg-light nav-border">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav mx-auto">


	    	@foreach($menus as $menu)

					
				<li class="nav-item ">
				  <a class="nav-link" href="{{ route('category',$menu->slug)  }}">{{ $menu->title }}</a>
				</li>


	    	@endforeach


	    </ul>
	  </div>
</nav>