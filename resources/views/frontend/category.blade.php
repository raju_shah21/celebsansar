@extends('layouts.app')

  @section('title','सेलेवसंसार') 

    @section('content')

        <section class="bg-primary" id="header">
            <div class="container">
                <i class="fa-2x far fa-calendar-alt"></i>
                <iframe scrolling="no" border="0" frameborder="0" marginwidth="0" marginheight="0" allowtransparency="true" src="https://www.ashesh.com.np/linknepali-time.php?dwn=only&font_color=fff&font_size=15&bikram_sambat=0&api=7221z8i366" width="165" height="22" style="padding-top: 3px"></iframe>

                <div class="float-right">
                    <i class="fab fa-twitter"></i>
                    <i class="fab fa-facebook-f"></i>
                    <i class="fab fa-youtube"></i>
                    <i class="fab fa-instagram"></i>
                </div>
            </div>
        </section>

        <!-- every section is in this container -->
        <div class="container">
            
                <!-- logo and banner ad -->
            <section class="pt-2 pb-2" id="logo-ad">
               <div class="row d-flex">
                 <div class="col-md my-col">
                   <a href="{{ route('front') }}">
                    <img src="{{ asset('img/logo.png') }}" alt="">
                    </a>
                 </div>
                 <div class="col-md my-col">
                   <img src="http://www.khabarpauwa.com/upload/becf90694322f7faa8320172b1b9df18.gif" class="ml-auto img-fluid">
                 </div>
               </div>         
           </section>



                <!-- navbar -->
            @include('frontend.partials.navbar')





            <section class="mx-auto mt-4 mb-4">
                <div class="text-center">
                    <img src="https://img.setopati.org/uploads/bigyapan/1541915889.gif" alt="" class="img-fluid">
                </div>
            </section>





            <section id="single-category-section">

                <div class="row">

                    <! -- news content image share box and comment box -->
                    <div class="col-xl-9 col-lg-12">

                      






                        <!-- category -->


                        @foreach($posts as $post)

                            <div class="mt-5 category-post">
                                <a href="{{ route('post',$post->id) }}" class="row">

                                    <div class="col-md-6 d-flex justify-content-center">
                                        <img src="{{ asset('dashboard/post/' . $post->feature_image) }}" 
                                         class="img-thumbnail rounded">
                                    </div>

                                    <div class="col-md-5 mt-4">
                                      <div class="media-body">
                                        <h3 class="main-title">{{ $post->title }}</h3>

                                       {!!  str_limit($post->paragraph,100) !!}
                                        <br>
                                        <div class="small-content">
                                            <span class="far fa-clock"></span>
                                            <span class="dates text-primary">
                                                @php 
                                                 $result = $nc->eng_to_nep($post->created_at->year,$post->created_at->month,$post->created_at->day);
                                                 print_r($result["month_name"] . " " . $result["date"] .", " .$result["year"]);
                                                @endphp
                                            </span>
                                            &nbsp;&nbsp;
                                            <i class="fas fa-user"></i>
                                            <span>{{ $post->author }}</span>
                                        </div>
                                     </div>
                                    </div>

                                </a>
                            </div>

                        @endforeach


                        <!-- category -->

                        <div class="mt-5 d-flex justify-content-center">
                             {{ $posts->links("pagination::bootstrap-4") }}
                        </div>
                    </div>




                    <div class="col-xl-3 col-md-6">

                        <div class="right-side-news">
                            <div class="text-center" id="border-muted">
                                <span class="mb-4 sidebar-heading">ताजा समाचार</span>
                            </div>
                            <ul class="list-unstyled text-center">
                                <li>
                                    <a href="">
        		                 			<span class="media-body">
        										<span class="main-title">पर्यटनमन्त्री रविन्द्र अधिकारीसहित, दुई पूर्वमन्त्रीलाई कारबाही गर्न सिफारिस</span>
        									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
        		                 			<span class="media-body">
        										<span class="main-title">फेडररले सेरेना विलियम्सलाई हराए</span>
        									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
        		                 			<span class="media-body">
        										<span class="main-title">फेडररले सेरेना विलियम्सलाई हराए</span>
        									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
        		                 			<span class="media-body">
        										<span class="main-title">फेडररले सेरेना विलियम्सलाई हराए</span>
        									</span>
                                    </a>
                                </li>
                            </ul>
                        </div>


                        <div id="single-page-side-ad" class="text-center">
                            <img src="https://img.setopati.org/uploads/bigyapan/1534756685.gif" alt="" class="short-ad img-fluid">
                            <hr>
                            <img src="https://img.setopati.org/uploads/bigyapan/1534756685.gif" alt="" class="short-ad img-fluid">
                        </div>

                    </div>
                </div>


                <!-- pagination-->
             <!--    <div class="text-center m-5">
                    <div class="pagination">
                        <a href="#">&laquo;</a>
                        <a href="#">1</a>
                        <a class="active" href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">&raquo;</a>
                    </div>
                </div> -->
                <!-- pagination -->

            </section>





        </div>

        <!-- footer -->
         @include('frontend.partials.footer')

    @endsection

