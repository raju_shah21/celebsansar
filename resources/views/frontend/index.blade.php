@extends('layouts.app')

  @section('title','सेलेवसंसार') 


  @section('meta')
  <style>
    #myBtn {
      display: none; /* Hidden by default */
      position: fixed; /* Fixed/sticky position */
      bottom: 20px; /* Place the button at the bottom of the page */
      right: 30px; /* Place the button 30px from the right */
      z-index: 99; /* Make sure it does not overlap */
      border: none; /* Remove borders */
      outline: none; /* Remove outline */
      background-color: red; /* Set a background color */
      color: white; /* Text color */
      cursor: pointer; /* Add a mouse pointer on hover */
      padding: 15px; /* Some padding */
      border-radius: 10px; /* Rounded corners */
      font-size: 18px; /* Increase font size */
    }

    #myBtn:hover {
      background-color: #0075BC; /* Add a dark-grey background on hover */
    }
  </style>
  @endsection

    @section('content')


        <section class="bg-primary" id="header">
          <div class="container">
             <i class="fa-1x far fa-calendar-alt"></i>
             <iframe scrolling="no" border="0" frameborder="0" marginwidth="0" marginheight="0" allowtransparency="true" src="https://www.ashesh.com.np/linknepali-time.php?dwn=only&font_color=fff&font_size=15&bikram_sambat=0&api=7221z8i366" width="165" height="22" style="padding-top: 8px"></iframe>

             <div class="float-right">
                 <i class="fab fa-twitter"></i>
                 <i class="fab fa-facebook-f"></i>
                 <i class="fab fa-youtube"></i>
                 <i class="fab fa-instagram"></i>
             </div>
           </div>
        </section>

              <!-- every section is in this container -->
        <div class="container">

          <section class="pt-2 pb-2" id="logo-ad">

              <div class="row d-flex">
                <div class="col-md my-col">
                  <a href="{{ route('front') }}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
                </div>
                <div class="col-md my-col">
                  <a href="{{ $headerBanner->link }}" target="_blank">
                    <img src="{{ asset('photos/1/banner/header/'.$headerBanner->image) }}" class="ml-auto img-fluid">
                  </a>
                </div>
              </div>
                 
          </section>



            <!-- navbar -->
         @include('frontend.partials.navbar')






          <section class="mx-auto mt-4 mb-4">
              <div class="text-center">
                @if(isset($sliderLongBanner))   
                @for($i=0;$i<=0;$i++)
                <a href="{{ $sliderLongBanner[$i]['link'] }}" target="_blank">
                  <img src="{{ asset('photos/1/banner/slider/'.$sliderLongBanner[$i]['image']) }}" alt="" class="img-fluid">
                </a>
                @endfor
                @endif
              </div>
          </section>



          <!-- carousel -->
          <section class="pb-3" id="carousel">

            <div class="row">
              <div class="col-lg-8 col-md-12">

                  <!--         carousel start      -->
                            

                  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">

                     <?php
                        $active = true;
                     


                          foreach($sliderposts as $sliderpost){

                            ?>
                            <div class="carousel-item {{ $active?"active":"" }}">
                              <a href="{{ route('post',$sliderpost->id) }}">
                                 <img src="{{ asset('dashboard/post/'.$sliderpost->feature_image) }}" 
                                 alt="First slide" class="img-fluid">
                             
                                <div class="carousel-caption jumbotron">
                                    
                                    <p class="font-weight-bold">{{ $sliderpost->title }}</p>
                                </div>
                               </a>
                            </div>

                          <?php

                            $active = false;

                          }
                        

                    ?>
                   
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>

                
                      <!--         carousel end      -->
              </div>


              <div class="col-md text-center">
                  @if(isset($sliderShortBanner))   
                    @for($i=0;$i<=1;$i++)
                     <a href="{{ $sliderLongBanner[$i]['link'] }}" target="_blank">
                      <img src="{{ asset('photos/1/banner/slider/'.$sliderShortBanner[$i]['image']) }}" alt="" class="short-ad img-fluid">
                     </a>
                      <hr>
                   @endfor
                  @endif  
   
              </div>
            </div>       

          </section>





          <section class="mb-4 long-ad">
              <div class="text-center">
                 @if(isset($sliderLongBanner))   
                  @for($i=1;$i<=1;$i++)
                   <a href="{{ $sliderLongBanner[$i]['link'] }}" target="_blank">
                    <img src="{{ asset('photos/1/banner/slider/'.$sliderLongBanner[$i]['image']) }}" alt="" class="img-fluid">
                   </a>
                 @endfor
                @endif    
              </div>
          </section>






          <!-- news heading -->
          <section class="mb-5 mt-5">
              <div class="text-center">

                  <ul class="list-inline">

                    <li class="list-inline-item">
                      
                        <i class="fa-3x far fa-newspaper border-left px-4" style="position: relative;top: 10px;color:#0075BC"></i>
                  
                    </li>

                    <li class="list-inline-item"><span class="category px-5">समाचार</span></li>
                  </ul>
                
                      
                        
                        
                     
                
              </div>
          </section>








          <section id="news" class="mb-5">


            <div class="row first-row" style="padding-bottom: 10px;">
              
                @if(isset($tvnewsposts))            
                @for($i=0;$i<=2;$i++)
                <div class="col-md-4">
                  <div class="card text-white news-item-big">
                    <a href="{{ route('post',$tvnewsposts[$i]['id']) }}" class="card-block clearfix">
                      <figure>
                      <img src="{{ asset('dashboard/post/'.$tvnewsposts[$i]['feature_image']) }}" class="card-img" height="230px">
                      </figure>
                    </a>
                    <div class="card-img-overlay h-100 d-flex flex-column justify-content-end">
                      <p class="card-text">{{ $tvnewsposts[$i]['title'] }}</p>
                      {{-- <p class="card-text text-muted">Last updated 3 mins ago</p> --}}
                    </div>
                  </div>
                </div>
                @endfor
                 @endif
            </div>



            <div class="row mt-5 second-row">
              
              @if(isset($tvnewsposts))            
                @for($i=3;$i<=6;$i++)

                <div class="col-lg-3 col-md-6">
                  <div class="news-item-small">
                    <a href="{{ route('post',$tvnewsposts[$i]['id']) }}"><img src="{{ asset('dashboard/post/' . $tvnewsposts[$i]['feature_image']) }}" >


                    <div class="post-heading mt-4 d-flex">
                      <span class="main-title text-center">
                          {{ $tvnewsposts[$i]['title'] }}
                      </span>
                    </div>

                    </a>

                  </div>      <!-- news-item-small end-->
                </div>
           
               @endfor
                 @endif

            </div>
 

          </section>








        <section class="mx-auto m-3 long-ad">
            <div class="text-center">

               @if(isset($sectionFirstBanner))   
                @foreach($sectionFirstBanner as $banner)
                 <a href="{{ $banner->link }}" target="_blank">
                  <img src="{{ asset('photos/1/banner/section-1/'.$banner->image) }}" alt="" class="img-fluid">
                 </a>
               @endforeach
              @endif   

            </div>
        </section>
        

        <!-- videos heading -->
        <section class="mb-5 mt-5">
            <div class="text-center">
                <table align="center">
                    <tr>
                        <td><i class="fa-7x fas fa-video border-left pl-3 pr-2"></i></td>
                        <td><span class="category px-5">भिडियो</span></td>
                    </tr>
                </table>
            </div>
        </section>

        <section id="videos" class="mb-5">


            <div class="row">

                <div class="col-md-8 ">

                   <iframe src="//www.youtube.com/embed/X3FBhD8BOH0?wmode=transparent" type="text/html" frameborder="0" allowfullscreen="" class="embed-responsive-item" style="width:100%;height:400px;"></iframe>
                </div>



                <div class="col-md-4 d-flex justify-content-center mt-2">

                  @if(isset($sectionSecondShortBanner))   
                     @for($i=0;$i<=0;$i++)
                      <a href="{{ $sectionSecondShortBanner[$i]['link'] }}" target="_blank">
                       <img src="{{ asset('photos/1/banner/section-2/'.$sectionSecondShortBanner[$i]['image']) }}" alt="" class="img-fluid">
                      </a>
                    @endfor
                   @endif   

                </div>

            </div>
        </section>


        <section class="mx-auto m-3 long-ad">
            <div class="text-center">
                @if(isset($sectionSecondLongBanner))   
                   @for($i=0;$i<1;$i++)
                    <a href="{{ $sectionSecondLongBanner[$i]['link'] }}" target="_blank">
                     <img src="{{ asset('photos/1/banner/section-2/'.$sectionSecondLongBanner[$i]['image']) }}" alt="" class="img-fluid">
                    </a>
                  @endfor
                 @endif   
            </div>
        </section>




      </div>

        <section id="event-model" class="ptb-5 container-fluid" style="background-color: #FAFAFA">
          
          <div class="container">
            <div class="row">

              <div class="col-lg-7 col-md-12">

                    <div class="text-center mb-4">
                      <table align="center">
                        <tr>
                            <td><i class="far fa-calendar-check fa-7x border-left pl-3 pr-2"></i></td>
                            <td><span class="category">इभेन्ट</span></td>
                        </tr>
                      </table>
                    </div>
                    <br>



                    @foreach($eventposts as $eventpost)

                      <div class="single-media mb-3">
                        <a href="{{ route('post',$eventpost->id) }}">
                        
                         <img src="{{ asset('dashboard/post/' . $eventpost->feature_image) }}"
                             alt="" width="150" height="100px">

                        <span class="media-body p-3">
                          <div class="media-title">{{ $eventpost->title }}</div>
                          
                          <span class="time-date float-left text-danger">
                            <i class="far fa-clock"></i>
                              @php 
                               $result = $nc->eng_to_nep($eventpost->created_at->year,$eventpost->created_at->month,$eventpost->created_at->day);
                               print_r($result["month_name"] . " " . $result["date"] ." " .$result["year"]);

                              @endphp


                          </span>
                        </span>
                         </a>
                      </div>

                    @endforeach


              </div>     <!--  col end -->

              <div class="col-lg-5 col-md-10">

                <div class="text-center mb-4">
                  <table align="center">
                    <tr>
                        <td><i class="far fa-calendar-check fa-7x border-left pl-3 pr-2"></i></td>
                        <td><span class="category">मोडल</span></td>
                    </tr>
                  </table>
                </div>
                <br>



                @foreach($modelposts as $modelpost)

                <div class="card text-white" id="model-section">
                  <a href="{{ route('post',$modelpost->id) }}" class="card-block clearfix">
                     <img  src="{{ asset('dashboard/post/'.$modelpost->feature_image) }}"
                     alt="Card image" height="300px" width="100%">
                  </a>
                  <div class="card-img-overlay d-flex flex-column justify-content-end text-center">
                    <p class="card-text">{{ $modelpost->title }}</p>
                    <p class="card-text"> 
                      @php
                         $result = NepaliCalendar::getNepaliDate($modelpost);
                         echo $result["month_name"] . " " . $result["date"] ." " .$result["year"];
                      @endphp
                     
                    </p>
                  </div>
                </div>

                @endforeach
              </div> <!--  col end -->

            </div>      <!--  row end -->
          </div>
        </section>


      <div class="container">

        <section id="discussion" class="ptb-5 container-fluid">
            <div class="icon-seperator text-center">
              <table align="center">
                <tr>
                    <td><i class="far fa-calendar-check fa-7x border-left pl-3 pr-2"></i></td>
                    <td><span class="category"><a href="{{ route('category','interview') }}">अन्तर्वार्ता</a></span></td>
                </tr>
              </table>
            </div>

            <div class="row">

              <div class="col-lg  card-bottom-padding">

                  @if(isset($interviewposts))            
                  @for($i=0;$i<1;$i++)
                  <div class="card text-white">
                    <a href="{{ route('post',$interviewposts[$i]['id']) }}">
                      <img class="card-img" src="{{ asset('dashboard/post/'.$interviewposts[$i]['feature_image']) }}" alt="Card image" height="350px">
                    
                    <div class="card-img-overlay d-flex flex-column justify-content-end">
                      <p class="card-text text-center" style="font-size: 26px !important; font-weight: 800;">{{ $interviewposts[$i]['title'] }}</p>
                    </div>
                    </a>
                  </div>
                  @endfor
                 @endif
              </div>

              <div class="col-lg col-md-12">
                 <div class="row second-row">
                    @if(isset($interviewposts))            
                  @for($i=1;$i<5;$i++)
                  <div class="col-lg-6 col-md-6 ">
                    <div class="card text-white">
                      <a href="{{ route('post',$interviewposts[$i]['id']) }}">
                        <img class="card-img" src="{{ asset('dashboard/post/'.$interviewposts[$i]['feature_image']) }}" alt="Card image" >
                      <div class="card-img-overlay d-flex flex-column justify-content-end">
                        <p class="card-text text-center">{{ $interviewposts[$i]['title'] }}</p>
                      </div>
                      </a>
                    </div>
                  </div>
                 @endfor
                 @endif
                </div> 
              </div>  <!-- second col end -->

            </div>    <!-- row end -->

                <!-- banner-ad area  -->
            <div class="row mt-5 d-flex justify-content-center" id="discussion-ad-group">
              @if(isset($sectionThirdBanner))
              @foreach($sectionThirdBanner as $banner)
                <div class="col-md-4">
                   <img src="{{ asset('photos/1/banner/section-3/'.$banner->image) }}" alt="" class="img-fluid">
                </div>
              @endforeach
              @endif
            
            </div>
        </section>



        </div>      



        <section id="video" class="ptb-5" style="background-color: #FAFAFA;">
          <div class="container">
            <div class="icon-seperator text-center">
              <table align="center">
                <tr>
                    <td><i class="far fa-calendar-check fa-7x border-left pl-3 pr-2"></i></td>
                    <td><span class="category">भिडियो</span></td>
                </tr>
              </table>
            </div>

            <div class="row">
                @if(isset($videos))            
                @for($i=0;$i<2;$i++)
               <div class="col-md">
                <div class="news-item-big">
                    <a href="{{ route('video.view',$videos[$i]['id']) }}">
                     <img src="{{ asset('dashboard/videos/'.$videos[$i]['image']) }}" alt="" class="card-img" height="300">

                      <span class="video-play">
                        <i class="fas fa-video"></i>
                      </span>

                      <div class="mt-3 text-center">
                        <span class="main-title">
                          {{ $videos[$i]['title'] }}
                        </span>
                      </div>
                    </a>
                  </div>
              </div>
               @endfor
                 @endif
            </div>


            <div class="row pt-5">
                @if(isset($videos))            
                @for($i=2;$i<5;$i++)
                <div class="col-md-4">
                  <div class="news-item-small">
                      <a href="{{ route('video.view',$videos[$i]['id']) }}">
                        <img src="{{ asset('dashboard/videos/'.$videos[$i]['image']) }}" alt="">

                        <span class="video-play">
                          <i class="fas fa-video"></i>
                        </span>
                        <div class="text-center mt-3">
                          <span class="main-title">
                              {{ $videos[$i]['title'] }}   
                          </span>
                        </div>
                      </a>
                    </div>
                </div>
             @endfor
                 @endif
            </div>


          </div>  <!-- container end -->
        </section>


      <div class="container">

        <section class="mx-auto mt-4 mb-4 long-ad">
          <div class="text-center">

             @if(isset($sectionFourthanner))   
                @for($i=0;$i<=0;$i++)
                 <a href="{{ $sectionFourthanner[$i]['link']  }}" target="_blank">
                  <img src="{{ asset('photos/1/banner/section-4/'.$sectionFourthanner[$i]['image']) }}" alt="" class="img-fluid mb-4">
                 </a>
               @endfor
              @endif   



          </div>
        </section>






        <section id="literature" class="ptb-5">
            <div class="icon-seperator text-center">
              <table align="center">
                <tr>
                    <td><i class="far fa-calendar-check fa-7x border-left pl-3 pr-2"></i></td>
                    <td><span class="category">लेख/साहित्य</span></td>
                </tr>
              </table>
            </div>

            <div class="row">

              @foreach($foodposts as $post)
              <div class="col-md">
                <div class="news-item-small">
                    <a href="{{ route('post',$post->id) }}">
                      <img src="{{ asset('dashboard/post/'.$post->feature_image) }}">
                   

                      <div class="mt-3 text-center">
                        <span class="main-title">
                            {{ $post->title }}
                        </span>
                      </div>
                    </a>

                  </div>
              </div>
              @endforeach


            </div>  <!-- row end -->
        </section>



      </div>  <!-- container end -->



        <!-- button to go on top when clicked -->
      <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
      



      @include('frontend.partials.footer')






  


     
  @endsection 





@section('js')

<script>
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function() {scrollFunction()};

  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("myBtn").style.display = "block";
    } else {
      document.getElementById("myBtn").style.display = "none";
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }
</script>
  
  <script>  
    $(".news-item-big").click(function() {
      console.log('try');
      window.location = $(this).find("a").attr("href"); 
      return false;
    });

    $("#model-section").click(function() {
      window.location = $(this).find("a").attr("href"); 
      return false;
    });

    $(".card").click(function() {
      window.location = $(this).find("a").attr("href"); 
      return false;
    });

    $(".carousel-item").click(function() {
      {{-- console.log('try'); --}}
      window.location = $(this).find("a").attr("href"); 
      return false;
    });
  </script>
@endsection
