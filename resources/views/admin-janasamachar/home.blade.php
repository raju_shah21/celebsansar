
use Carbon\Carbon;
@extends('admin.layouts.app')
@section('title','Dashboard')

@section('content')
<h1>Dashboard</h1>

<div class="row tile_count" style="height:100px">
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
            <span class="count_top"><i class="fa fa-book"></i> Total Posts</span>
            <div class="count">{{App\Post::countPosts()}}</div>
        </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
            <span class="count_top"><i class="fa fa-clone"></i> Total Pages</span>
            <div class="count">{{App\Post::countPages()}}</div>
        </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
            <span class="count_top"><i class="fa fa-tag"></i> Total Categories</span>
            <div class="count">{{App\Category::count()}}</div>
        </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
            <span class="count_top"><i class="fa fa-user"></i> Users</span>
            <div class="count">{{App\User::count()}}</div>
        </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
            <span class="count_top"><i class="fa fa-bullhorn"></i> Total Advertisement</span>
            <div class="count green">{{App\Ads::count()}}</div>
        </div>
    </div>


    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
        <div class="left"></div>
        <div class="right">
            <span class="count_top"><i class="fa fa-caret-square-o-right"></i> Total Programs</span>
            <div class="count">{{App\Program::count()}}</div>
        </div>
    </div>

</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-4">

  </div>
  <div class="col-md-4">

  </div>
  <div class="col-md-4 right">
    <?php
    $now = new \Carbon\Carbon('now');
    $expireAds = App\Ads::select('name','filename','place_id','valid_date')->where('valid_date','<=',$now)->take(5)->get();

    ?>
    @if(count($expireAds)>0)
<h4>Date Expired Advertisment</h4>
<p>Please renew to display. <i style="color:brown"><u>click ads to renew date<u></i></p>
    <div class="list-group">
      @foreach($expireAds as $value)
        <a class="list-group-item " href="{{url('ads/list/'.$value->place_id)}}">
          <img style="width:100%;height:auto" src="{{url('uploads/'.$value->filename)}}" alt="{{$value->name}}">
        </a>
        <p class="mute">{{$value->name or null}}: expired at {{substr($value->valid_date,0,10)}}</p>
      @endforeach
    @endif
    </div>
  </div>
</div>
<!-- /top tiles -->
@endsection
