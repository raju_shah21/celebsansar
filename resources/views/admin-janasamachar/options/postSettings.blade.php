@extends('admin.layouts.app')
@section('title','Themes Option')
@section('content')
    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Home Page Management</h2>

            </div>

            <div class="pull-right">


            </div>

        </div>

    </div>


    <div class="col-md-12">
        <div class="box-header">
            <h3 class="box-title">Posts Option</h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-6">
                <?php $ar = array($sec0,$sec1,$sec2,$sec3,$sec4,$sec5,$sec6,$sec7,$sec8,
                        $sec9,$sec10,$sec11,$sec12,$sec13,$sec14,$sec15,$sec16);?>
                <form class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <fieldset>
                        @for($i=0; $i<16;$i++)
                            <div class="form-group">
                                <label class="col-md-4 control-label">Section {{$i}}</label>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <select name="sec{{$i}}" class="form-control">

                                        {{--<option>Choose Category {{$sec0}}</option>--}}
                                        @if(!$categories->isEmpty())
                                            @foreach($categories as $value)

                                                <option value="{{$value->slug}}"  <?=($ar[$i] == $value->slug)? "selected='selected'":"" ?> >{{ $value->name }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                        @endfor




                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-4">
                                <button id="saveOptions" type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-send"></span></button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="col-md-6">
            <img src="{{url('images/sample.png')}}" width="100%">
            </div>



    </div>
    <!-- /.box-body -->
    </div>


@endsection
@section('styles')

@endsection
@section('scripts')

    <script>

        (function(){

            //social links
            $("#saveOptions").click(function(e){
                e.preventDefault();
                var data = $('form').serialize();
                console.log(data);
                var url = "{{ url('option/store') }}";
                $.ajax({
                    url: url,
                    dataType: "json",
                    type: "post",
                    data: data,
                    success: function (data) {
                        if(data.code == 200)
                            alert("success");
                        else
                            alert('error occured');

                    }
                })
            })
        })();

    </script>
@endsection
