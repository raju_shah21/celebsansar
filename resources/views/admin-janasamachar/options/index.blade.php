@extends('admin.layouts.app')
@section('title','Themes Option')
@section('content')
  <div class="row">

	    <div class="col-lg-12 margin-tb">

	        <div class="pull-left">

	            <h2>Themes Management</h2>

	        </div>

	        <div class="pull-right">


	        </div>

	    </div>

	</div>

	<div class="">
			<div class="box-header">
					<h3 class="box-title">Theme Option</h3>
					<p class ="text-muted">Manage your setting</p>
          
			</div>
			<!-- /.box-header -->
			<div class="box-body">

        <div class="col-xs-2"> <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left"><!-- 'tabs-right' for right tabs -->
            <li class="active"><a href="#home" data-toggle="tab">Home</a></li>

            <li><a href="#profile" data-toggle="tab">Profile</a></li>
            <li><a href="#styles" data-toggle="tab">Styles</a></li>

          </ul>
        </div>
        <div class="col-xs-10">
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="home">
                @include('admin.options.includes.homePage')
              </div>

              <div class="tab-pane" id="profile">
                  @include('admin.options.includes.profile')
              </div>
              <div class="tab-pane" id="styles">
                @include('admin.options.includes.styles')
              </div>

              </div>

            </div>
        </div>

			</div>
			<!-- /.box-body -->
	</div>


@endsection
@section('styles')
<style>

.tabs-left, .tabs-right {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}
</style>

{!! HTML::style('admin/assets/dropzone/dropzone.css') !!}
{!! HTML::style('plugins/colorpicker/bootstrap-colorpicker.min.css') !!}

@endsection
@section('scripts')
    {!! HTML::script('admin/assets/dropzone/dropzone.js') !!}
    {!! HTML::script('plugins/colorpicker/bootstrap-colorpicker.min.js') !!}

    <script>
    $(function(){
        $('.color-picker').colorpicker();
    });


    (function(){
        //home setting
        //change contactInfo at footer homepage
        $("#footerContactInfo").click(function(e){
            e.preventDefault();
            var data = $("#contactInfo").val();
            var url = "{{ url('option/contactInfo') }}";
            $.ajax({
                url: url,
                dataType: "json",
                type: "post",
                data: {'footerContactInfo':data},
                success: function (data) {
                    if(data.code == 200)
                        alert("success");
                    else
                        alert('error occured');

                }
            })
        })

        $("#saveColor").click(function(e){
            e.preventDefault();
            var data = $("#colorSetting").serialize();
            console.log(data);
            var url = "{{ url('option/store') }}";
            $.ajax({
                url: url,
                dataType: "json",
                type: "post",
                data: data,
                success: function (data) {
                    if(data.code == 200)
                        alert("success");
                    else
                        alert('error occured');

                }
            })
        });

        $('#profile1').submit(function(e){
            e.preventDefault();
            var data = $(this).serialize();
            console.log(data);
            var url_contact = "{{url('option/contactInfo')}}";
            $.ajax({
                dataType:'json',
                type: 'post',
                url: url_contact,
                data: data,
                success:function(data){
                    alert('success');
                }
            });
        });
//social links
        $("#socialShare").click(function(e){
            e.preventDefault();
            var data = $(this).serialize();
            console.log(data);
            var url = "{{ url('option/store') }}";
            $.ajax({
                url: url,
                dataType: "json",
                type: "post",
                data: data,
                success: function (data) {
                    if(data.code == 200)
                        alert("success");
                    else
                        alert('error occured');

                }
            })
        })

        //social links
        $("#saveOptions").click(function(e){
            e.preventDefault();
            var data = $(this).serialize();
            console.log(data);
            var url = "{{ url('option/store') }}";
            $.ajax({
                url: url,
                dataType: "json",
                type: "post",
                data: data,
                success: function (data) {
                    if(data.code == 200)
                        alert("success");
                    else
                        alert('error occured');

                }
            })
        })
    })();

</script>
@endsection
