<?php $form = array(
  ['Header Background Color','headerBgColor',$headerBgColor],
  ['Footer Background Color','footerBgColor',$footerBgColor],
  ['Slider Caption Background color','sliderCaptionBgColor',$sliderCaptionBgColor]
);
//dd($forms);
$count = count($form);
?>
<form class="form-horizontal" id="colorSetting">

   <fieldset>

      @for($i=0; $i<$count; $i++)

          <div class="form-group">
             <label class="col-md-5 control-label">{{ $form[$i][0] }}</label>
             <div class="col-md-6 inputGroupContainer">
                <div class="input-group">
                   <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i></span>
                   <input class="color-picker" name="{{ $form[$i][1] }}" placeholder="{{ $form[$i][0] }}"  type="text" value="{{ $form[$i][2] or null}}">
                </div>

             </div>
          </div>

      @endfor
</form>
