<form class="form-horizontal" id="socialShare">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
   <fieldset>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-4 control-label">Facebook</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-share-alt"></i></span>
               <input  name="facebook" placeholder="Facebook link" class="form-control"  type="text" value="{{ $facebook or null}}">
            </div>
         </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-4 control-label" >Google Plus</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-share-alt"></i></span>
               <input name="googlePlus" placeholder="Google Plus Link" class="form-control"  type="text" value="{{ $googlePlus or null}}">
            </div>
         </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-4 control-label">Twitter</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-share-alt"></i></span>
               <input name="twitter" placeholder="Twitter Link" class="form-control"  type="text" value="{{ $twitter or null}}">
            </div>
         </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-4 control-label">LinkIn Link</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-share-alt"></i></span>
               <input name="linkIn" placeholder="LinkIn Link" class="form-control" type="text" value="{{ $linkIn or null}}">
            </div>
         </div>
      </div>

      <div class="form-group">
         <label class="col-md-4 control-label"></label>
         <div class="col-md-4">
            <button id="shareMe" type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-send"></span></button>
         </div>
      </div>
   </fieldset>
</form>
