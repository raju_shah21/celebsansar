<div class="container">
<div class="col-md-6">
  <h2>Header</h2>
  <hr />
<h4>Change Site Logo</h4>
          <div class="" >


              {!! Form::open(['url' => route('optionFile.upload'), 'class' => 'dropzone', 'files'=>true, 'id'=>'my-awesome-dropzone']) !!}

              <div class="dz-message">

              </div>

              <div class="fallback">
                  <input name="file" type="file" multiple />
              </div>

              <div class="dropzone-previews" id="dropzonePreview"></div>

              <h4 style="text-align: center;color:#428bca;">Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4>

              {!! Form::close() !!}

          </div>
          <div class="jumbotron" style="padding: 30px 0;">
              <ul>
                  <li style="list-style: none;"><img src="<?php echo (!empty($siteLogo))?url($siteLogo):"option not found";?>" alt="{{ $siteName or 'Site Name'}}"  /></li>
                  <li style="list-style: none;">Preview of current Site Logo</li>
              </ul>

          </div>

          <h2>Footer</h2>
          <hr />
        <h4>Change Footer contact</h4>
        <div class="form-group">

           <div class="inputGroupContainer">
              <div class="input-group">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                 <textarea id="contactInfo" class="form-control" name="footerContactInfo" placeholder="Your contact info">{{ $footerContactInfo or null }}</textarea>
              </div>
           </div>
           <div class="form-group">
                 <button id="footerContactInfo" type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-send"></span></button>
           </div>
        </div>
        <div class="jumbotron" style="padding: 30px 20px;">
          Phone : +977 9843408105 {{"<br />"}}<br />
          Email : contact@dbrostech.com {{"<br />"}}<br />
          Address : Swyambhu, Kathmandu {{"<br />"}}<br />
          <b>notice: use {{"<br />"}} for next line as show above.</b>
        </div>

      </div>
  </div>
