<div class="container">
<div class="col-md-6">
  <h2>Color Settings</h2>
  <hr />

<form class="form-horizontal" id="colorSetting">

   <fieldset>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-5 control-label">Header Background color</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i></span>
               <input class="color-picker"  name="headerBgColor" placeholder="Header Background Color"  type="text" value="{{ $headerBgColor or null}}">
            </div>

         </div>
      </div>
      <div class="form-group">
         <label class="col-md-5 control-label">Footer Background color</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i></span>
               <input class="color-picker"  name="footerBgColor" placeholder="Footer Background Color"   type="text" value="{{ $footerBgColor or null}}">
            </div>
         </div>
      </div>


      <div class="form-group">
         <label class="col-md-5 control-label">Slider Caption Background color</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i></span>
               <input class="color-picker"  name="sliderCaptionBgColor" placeholder="Menu Background Color"   type="text" value="{{ $sliderCaptionBgColor or null}}">
            </div>
         </div>
      </div>
      <h2>Menu Settings</h2>
      <hr />
      <div class="form-group">
         <label class="col-md-5 control-label">Menu Background color</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i></span>
               <input class="color-picker"  name="menuBgColor" placeholder="Menu Background Color"   type="text" value="{{ $menuBgColor or null }}">
            </div>
         </div>
      </div>
      <div class="form-group">
         <label class="col-md-5 control-label">Menu Font Color</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i></span>
               <input class="color-picker"  name="menuFontColor" placeholder="Menu Font Color"   type="text" value="{{ $menuFontColor or null }}">
            </div>
         </div>
      </div>
      <div class="form-group">
         <label class="col-md-5 control-label">Menu Hover Font Color</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i></span>
               <input class="color-picker"  name="menuHoverFontColor" placeholder="Menu Hover Font Color"   type="text" value="{{ $menuHoverFontColor or null }}">
            </div>
            <p>
              change color while hover menu
            </p>
         </div>
      </div>
      <div class="form-group">
         <label class="col-md-5 control-label">Menu Hover Background Color</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i></span>
               <input class="color-picker"  name="menuHoverBgColor" placeholder="Menu Hover Font Color"   type="text" value="{{ $menuHoverBgColor or null }}">
            </div>

         </div>
      </div>
      <div class="form-group">
         <label class="col-md-4 control-label"></label>
         <div class="col-md-4">
            <button id="saveColor" type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-send"></span></button>
         </div>
      </div>
    </fieldset>
  </form>
      </div>
  </div>
