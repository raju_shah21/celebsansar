<form class="form-horizontal" id="profile1">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
   <fieldset>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-4 control-label">Site Name</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
               <input  name="siteName" placeholder="Site Name" class="form-control"  type="text" value="{{ $siteName or null}}" required>
            </div>
         </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-4 control-label" >Site Slogan</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
               <input name="siteSlogan" placeholder="Site Slogan" class="form-control"  type="text" value="{{ $siteSlogan or null}}">
            </div>
         </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-4 control-label">E-Mail</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
               <input name="email" placeholder="E-Mail Address" class="form-control"  type="email" value="{{ $email or null}}">
            </div>
         </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-4 control-label">Phone #</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
               <input name="phone" placeholder="+977 9876543210" class="form-control" type="text" value="{{ $phone or null}}">
            </div>
         </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
         <label class="col-md-4 control-label">Address</label>
         <div class="col-md-6 inputGroupContainer">
            <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
               <input name="address" placeholder="Address" class="form-control" type="text" value="{{ $address or null}}">
            </div>
         </div>
      </div>

      <div class="form-group">
         <label class="col-md-4 control-label"></label>
         <div class="col-md-4">
            <button type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-send"></span></button>
         </div>
      </div>
   </fieldset>
</form>
