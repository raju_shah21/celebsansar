@extends('admin.layouts.app')
@section('title','Posts | Add New')
@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="box-warnning">
                <div class="box-header with-border">
                    <h3 class="box-title">Pages</h3>
                </div>
                <form id="pageMenu">
                    <input name="type" value="page" type="hidden"/>
                    <div class="box-body" style="max-height: 500px; overflow: auto;" >
                        <ul class="todo-list">
                            @if(!empty($pages))
                            @foreach($pages as $value)
                                <li class="ui-state-default" id ="{{ $value->id }}" name="cid[]">
                                    <input type="checkbox" value="{{ $value->id }}" name="cid[]">
                                    <span>{{ $value->title }}</span>
                                </li>
                            @endforeach
                                @endif
                        </ul>
                        <div class="box-footer">
                            <button id="savePageMenu" class="btn btn-primary" type="submit">Save Menu</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <div class="col-md-3">
            <div class="box-warnning">
                <div class="box-header with-border">
                    <h3 class="box-title">Categories</h3>
                </div>
                <form id="catMenu">
                    <input name="type" value="category" type="hidden"/>
                    <div class="box-body" style="max-height: 500px; overflow: auto;" >
                        <ul class="todo-list">
                            @if(!empty($categories))
                            @foreach($categories as $category)
                                <li class="ui-state-default" id ="{{ $category->id }}" name="cid[]">
                                    <input type="checkbox" value="{{ $category->id }}" name="cid[]">
                                    <span>{{ $category->name }}</span>
                                </li>
                            @endforeach
                                @endif
                        </ul>
                    </div>
                    <div class="box-body">
                        <label>Choose menu</label>
                        <select name="menuId">
                            @if(!empty($menus))
                            @foreach($menus as $value)
                                <option value="{{ $value->id}}">{{ $value->menuName }}</option>
                            @endforeach
                                @endif
                        </select>
                        <div class="box-footer">
                            <button id="saveMenu" class="btn btn-primary" type="submit">Save Menu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-4">
            <div class="box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Menu</h3>
                </div>
                <div class="box-body" >
                    <div class="box-body" style="max-height: 500px; overflow: auto;" >

                        <ul class="todo-list" id="sortable">
                            @if(!empty($nav))
                            @foreach($nav as $value)

                                <li class="ui-state-default" id ="{{ $value->id }}" name="cid[]">
                                    <span>{{ $value->label }}</span>
                                    <div class="tools">
                                        <i class="fa fa-trash-o" id="{{  $value->id }}"></i>
                                    </div>
                                </li>

                            @endforeach
                                @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('styles')
    {{ HTML::style("admin/css/jquery-ui.css")}}
@endsection
@section('scripts')

    {{ HTML::script("admin/js/jquery-ui.js")}}

    <script>
        $(function () {
            $("#saveMenu").click(function(e){
                e.preventDefault();

                var url = '{{ url("menu/store") }}';
                var data = [];
                var cat = $('#category option:selected');
                data = new FormData(document.getElementById("catMenu"));

                $.ajax({
                    url: url,
                    type:'post',
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    data:data,
                    success:function(response){
                        if(response.code == 200){
                            alert('success');

                            $('input[type=checkbox]').each(function()
                            {
                                this.checked = false;
                            });

                            //location.reload();
                        }
                        if(response.code == 'update'){
                            alert('successfully updated');
                        }
                    }

                });
            })
//for page menu
            $("#savePageMenu").click(function(e){
                e.preventDefault();

                var url = '{{ url("menu/store") }}';
                var data = [];

                data = new FormData(document.getElementById("pageMenu"));

                $.ajax({
                    url: url,
                    type:'post',
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    data:data,
                    success:function(response){
                        if(response.code == 200){
                            alert('success');

                            $('input[type=checkbox]').each(function()
                            {
                                this.checked = false;
                            });

                            //location.reload();
                        }
                        if(response.code == 'update'){
                            alert('successfully updated');
                        }
                    }

                });
            })


            $('#sortable').sortable({
                update: function () {
                    var data = save_new_order()
                    $.ajax({
                        url:"{{ url("menuOrder") }}",
                        type:'post',
                        data: data,
                    })
                }
            });

            function save_new_order() {
                var a = [];
                $('#sortable').children().each(function (i) {
                    a.push($(this).attr('id'));
                });
                var s={start:a}
                return s;
            }
        });


    </script>
    <script type="text/javascript">
        (function(){

            //delete
            $(".fa-trash-o").click(function(){
                var id = $(this).attr("id");

                var url = "{{ url('menus/destroy') }}";
                $.ajax({
                    url:url,
                    type:'post',
                    dataType:'JSON',
                    data:{id:id},
                    success:function(data){
                        if(data.code = 200){
                            $("li#"+id).remove();
                            console.log(data.success)
                        }else{
                            console.log(data.error);
                        }

                    }
                })
            });


            // $('.action').change(function() {
            //  var id = $(this).val();
            //      $.ajax({
            //        url:"{{ url('menus/destroy') }}",
            //        type:'post',
            //        data: {'id':id},
            //        success:function(data){
            //          if(data.code = 200){
            //                     $("li#"+id).remove();
            //                    console.log(data.success)
            //                 }else{
            //                     console.log(data.error);
            //                 }
            //        }
            //      })
            //  });

        })();

    </script>
@endsection