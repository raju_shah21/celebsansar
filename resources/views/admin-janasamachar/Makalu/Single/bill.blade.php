<div class="hidden-div" id="bill-div" hidden="hidden">
    <div class="col-md-6" style="background: white; margin-top:30px;border: 2px solid lightgray;">
        <h3>Order Bills</h3>
        <hr>
        <table class="table bordered-table" id="bill-table">
            <thead>
            <tr>
                <th>SN</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <thead>
            <tr>
                <th colspan="4" class="text-right">Total</th>
                <th id="sub-total"></th>
            </tr>
            <tr>

                <th colspan="4" class="text-right">Delivery Charge</th>
                <th id="ship-cost"></th>
            </tr>
            <tr>
                <th colspan="4" class="text-right">Total</th>
                <th id="total-price"></th>
            </tr>
            </thead>

        </table>
    </div>
    <div class="col-md-6" id="status-div" style="background: white; margin-top:30px;border: 2px solid lightgray;">
        <h1>Status</h1>

        <textarea onkeyup="updateStatus(event, $(this));" name="update_status" style="width: 100%; height: 80px;" placeholder="UPDATE STATUS..."></textarea>
        <hr>
        <ul>

        </ul>

    </div>

    <i onclick="closeParent(event, $(this));" class="fa fa-close fa-3x show-pointer delete" style="position: absolute; top:10px;right:10px;"></i>

</div>