@extends('admin.layouts.app')
@section('title','product')
@section('styles')
    @include('admin.Makalu.Single.style')
@endsection

@section('content')

    <div class="col-md-12">
        <h1>Rent Agreement</h1>
        <hr>
        <form action="{{url('admin/single/post')}}" method="post">
            {{csrf_field()}}
            <input type="text" name="type" value="rent" hidden="hidden">
            <textarea name="text" id="editor">
                <?php
                if($find != null){
                    echo $find->text;
                }
                ?>
            </textarea>
            <input type="submit" value="SUBMIT">
        </form>
    </div>





@endsection

@section('scripts')

    <script src="{{url('plugins/ckeditor/ckeditor.js')}}"></script>
    @include('admin.Makalu.Single.script')

@endsection