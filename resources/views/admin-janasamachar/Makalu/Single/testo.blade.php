@extends('admin.layouts.app')
@section('title','product')
@section('styles')
    <link rel="stylesheet" href="{{url('assets/css/tableFilter.css')}}">
    @include('admin.Makalu.Single.style')
@endsection

@section('content')

    <div class="col-md-12">
        <div id="example_wrapper" class="dataTables_wrapper">
            <table class="table table-bordered table-hover" id="example">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Image</th>
                    <th>Comment</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $l)
                    <tr class="{{$l->approve == true ? 'success':'danger'}}">
                        <td>{{$l->name}}</td>
                        <td>{{$l->email}}</td>
                        <td><img src="{{$l->getImage()}}" alt="" width="50px" height="50px"></td>
                        <td>{{$l->text}}</td>
                        <td>
                            @if($l->approve == false)
                            <a href="{{url('admin/single/testo/approve/'.$l->id)}}">Approve</a>
                            @endif
                            <a href="{{url('admin/single/testo/delete/'.$l->id)}}">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>





@endsection

@section('scripts')
    <script src="{{url('assets/js/tableFilter.js')}}"></script>

    @include('admin.Makalu.Single.script')

    <script>
        (function () {
            $('#example').DataTable();
        })();
    </script>
@endsection