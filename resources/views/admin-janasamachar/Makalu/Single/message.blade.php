@extends('admin.layouts.app')
@section('title','product')
@section('styles')
    <link rel="stylesheet" href="{{url('assets/css/tableFilter.css')}}">
    @include('admin.Makalu.Single.style')
@endsection

@section('content')

    <div class="col-md-12">
        <p>Unread Messages: {{$count}}</p>
        <div id="example_wrapper" class="dataTables_wrapper">
            <table class="table table-bordered table-hover" id="example">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $l)
                    <tr class="{{$l->status == true ? 'success':'danger'}}">
                        <td>{{$l->id}}</td>
                        <td>{{$l->name}}</td>
                        <td>{{$l->email}}</td>
                        <td>{{$l->address}}</td>
                        <td>{{$l->contact}}</td>
                        <td>{{$l->subject}}</td>
                        <td>{{$l->message}}</td>
                        <td>
                            @if($l->status == false)
                                <a href="{{url('admin/single/msg/watch/'.$l->id)}}"><i class="fa fa-eye"></i></a>
                            @endif
                            <a href="{{url('admin/single/msg/delete/'.$l->id)}}">Delete</a>
                            @if($l->client == false)
                                    <a href="{{url('admin/single/msg/client/'.$l->id)}}"><i class="fa fa-user"></i></a>
                                @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>





@endsection

@section('scripts')
    <script src="{{url('assets/js/tableFilter.js')}}"></script>

    @include('admin.Makalu.Single.script')

    <script>
        (function () {
            $('#example').DataTable();
        })();
    </script>
@endsection