<style>
    .hidden-div {
        width: 100%;
        height: 100%;
        overflow: auto;
        background: rgba(0, 0, 0, 0.52);
        position: fixed;
        top: 0;
        left: 0;
        z-index: 999;
    }

</style>