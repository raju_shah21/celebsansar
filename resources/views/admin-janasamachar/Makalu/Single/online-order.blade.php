@extends('admin.layouts.app')
@section('title','product')
@section('styles')
    <link rel="stylesheet" href="{{url('assets/css/tableFilter.css')}}">
    @include('admin.Makalu.Single.style')
@endsection

@section('content')

    <div class="col-md-12">
        <p>Unread Orders: {{$count}}</p>
        <div id="example_wrapper" class="dataTables_wrapper">
            <table class="table table-bordered table-hover" id="example">
                <thead>
                <tr>
                    <th>Order Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Payment Method</th>
                    <th>Info</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $l)
                    <tr class="{{$l->sold == true ? 'info':($l->watch == true ? 'success':'danger')}}">
                        <td>{{$l->o_id}}</td>
                        <td>{{$l->name}}</td>
                        <td>{{$l->email}}</td>
                        <td>{{$l->address}}</td>
                        <td>{{$l->contact}}</td>
                        <td>{{$l->payment_method}}</td>
                        <td>{{$l->info}}</td>
                        <td order-id="{{$l->o_id}}" ship="{{$l->ship}}">
                                <a href="" onclick="watchOrder(event, $(this));"><i class="fa fa-eye"></i></a>
                            @if($l->sold == false && $l->watch == true)
                            <a href="" onclick="soldOrder(event, $(this));">SOLD</a>
                                @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

@include('admin.Makalu.Single.bill')
@endsection

@section('scripts')
    <script src="{{url('assets/js/tableFilter.js')}}"></script>

    @include('admin.Makalu.Single.script')

    <script>

        function soldOrder(e,sib) {
            e.preventDefault();
            var o_id = sib.parent('td').attr("order-id");
            var ship = sib.parent('td').attr("ship");

            $.get(e_url('admin/single/soldOrder'),{'id':o_id,'ship':ship}).then(function (res) {
                location.reload();
            });
        }

        function watchOrder(e, sib) {
            e.preventDefault();
            var o_id = sib.parent('td').attr("order-id");
            var ship = sib.parent('td').attr("ship");
            $('#status-div').attr("order-id",o_id);
            $.get(e_url('admin/single/bills/'+o_id)).then(function (res) {
                $('#bill-div').show();
              var sub_total = 0;
                var l = res.bills.length;
                $('#bill-table tbody').empty();
                for(var i=0; i<l; i++){
                $('#bill-table tbody').append('<tr style="font-style: italic; color:#810000;"> ' +
                    '<td>'+(i+1)+'</td> ' +
                    '<td>'+res.bills[i].name+'</td> ' +
                    '<td>'+res.bills[i].quantity+'</td> ' +
                    '<td>'+res.bills[i].rate+'</td> ' +
                    '<td>'+res.bills[i].total+'</td> ' +
                    '</tr>');
                sub_total += res.bills[i].total;
                }
                    $('#sub-total').empty().text('Rs.'+sub_total);
                var ship_cost = 0;
                if(ship == 1){
                    var ship_cost = parseInt(sub_total*0.05);
                }
                $('#ship-cost').empty().text('Rs.'+ship_cost);
                $('#total-price').empty().text('Rs.'+(sub_total+ship_cost));
                sib.parent('td').parent('tr').addClass('success');


                $('#status-div ul').empty();
                var l1 = res.status.length;
                for(var i=0; i<l1; i++){
                    $('#status-div ul').append('<li>'+res.status[i].created_at+' -> '+res.status[i].status+'</li>')
                }
            });

        }


        function updateStatus(e, sib) {
         e.preventDefault();
         if(e.keyCode == 13){
             var o_id = sib.parent('div').attr("order-id");
             var txt = sib.val();
             showSpin();
             $.get(e_url('admin/single/updateStatus'),{'id':o_id,'txt':txt}).then(function (res) {
                 hideSpin();
                 if(res.error==0){
                     $('#status-div ul').prepend('<li>now -> '+txt+'</li>')

                 }else{
                     showErrorMsg(res);
                 }

             });
         }


        }
        (function () {
            $('#example').DataTable();
        })();
    </script>
@endsection