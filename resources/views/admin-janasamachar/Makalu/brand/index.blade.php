@extends('admin.layouts.app')
@section('title','product')
@section('styles')
    <link rel="stylesheet" href="{{url('assets/css/tableFilter.css')}}">
    <style>

    </style>

    @include('Admin.Makalu.brand.style')
@endsection

@section('content')



<div class="col-md-6">
    <h1>Create Brand Form</h1>
    <hr>
    <form action="{{url('admin/brand/create')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="text" name="id" hidden="hidden">
        <input type="text" name="name" class="form-control" style="text-transform: uppercase;" placeholder="BRAND NAME ....">
        <br>

        <input type="file" name="image" class="form-control"> <br>
        <input type="submit" value="submit" class="form-control">
    </form>


</div>
<div class="col-md-6">
    <h1>List of Active Brand</h1>
     <ul class="list-group">
@foreach($list as $l)
         <li class="list-group-item" id="list{{$l->id}}">{{$l->name}}
         <span class="badge" brand-id="{{$l->id}}">
             <a href="" onclick="editBrand(event, $(this));" style="color:white;">edit</a> <a href="" onclick="deleteBrand(event, $(this));" style="color:white;">Delete</a>
         </span>
         </li>
    @endforeach
     </ul>

</div>


@endsection

@section('scripts')
    <script src="{{url('assets/js/tableFilter.js')}}"></script>
    @include('Admin.Makalu.brand.script')

@endsection