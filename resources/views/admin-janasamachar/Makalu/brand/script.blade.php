<script>


    var myApp = angular.module("myModule",[]);

    function deleteBrand(e, sib) {
        e.preventDefault();
        var id = sib.parent('span').attr("brand-id");
        showSpin();
        $.get(e_url('admin/brand/delete/'+id)).then(function(res){
            hideSpin();
            if(res.error == 0){
                $('#list'+id).remove();
            }else{
                showErrorMsg(res);
            }
        });
    }


    function editBrand(e,sib) {
        e.preventDefault();
        var id = sib.parent('span').attr("brand-id");
        showSpin();
        $.get(e_url('admin/brand/edit/'+id)).then(function(res){
            hideSpin();
            if(res.error == 0){
                $('input[name=name]').val(res.find.name);
                $('input[name=id]').val(res.find.id);
                $('input[type=submit]').val('Update');
            }else{
                showErrorMsg(res);
            }
        });
    }
    (function () {



    })();
    
</script>