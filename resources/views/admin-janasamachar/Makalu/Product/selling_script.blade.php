<script>
    var myApp = angular.module("myModule",[], function ($interpolateProvider) {
        $interpolateProvider.startSymbol("[[");
        $interpolateProvider.endSymbol("]]");
    });

    myApp.controller("myCtrl", function ($http,$scope,$log) {


    });

    function calculateTSP() {
        var l = $('#example_selling').find('tbody').children('tr').length;
        var sp = 0;
        for(var i=0; i<l; i++){
           var sp = sp+parseFloat($('#example_selling').find('tbody').children('tr:eq('+i+')').children('td:eq(5)').text());
        }

        $('#tsp').empty().text('Rs. '+sp);

    }

    function returnProduct(e, sib) {
        e.preventDefault();
        var id = sib.parent('td').attr("p-id");
        showSpin();
        $.get(e_url('admin/product/returnProduct/'+id)).then(function (res) {
            hideSpin();
            if(res.error == 0){
                sib.parent('td').parent('tr').remove();
            }else{
                alert('Some Error Could not cancel order. Please Try again');
            }
        })
    }

    (function () {

    calculateTSP();

        $('#example_selling').DataTable();

        $('input[type=search]').keyup(function () {
          calculateTSP();
        });
    })();
</script>