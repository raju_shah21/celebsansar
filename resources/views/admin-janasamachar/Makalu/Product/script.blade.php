<script>


    var myApp = angular.module("myModule",[]);

    var product_form_div = $('#product-form-div');
    var add_product_div = $('#add-product-div');
    var srd_div = $('#srd-div');




    function addMore(e, sib) {
        e.preventDefault();
        product_form_div.show();
        add_product_div.hide();
        srd_div.hide();
        resetForm('product-form');
        CKEDITOR.instances['editor'].setData('Add Info..');

    }

    function addProduct(e,sib) {
        e.preventDefault();

        product_form_div.hide();
        add_product_div.show();
        srd_div.hide();
        var p_id = sib.parent('td').attr("product-id");
        var form_id = 'product-form1';
        var product_name = $('#product'+p_id+' td:eq(0)').text();
        $('#'+form_id+' input[name=product_id]').val(p_id);
        $('#'+form_id+' h1').empty().text('ADD PRODUCT '+product_name);
    }

    function submitSRD(e,sib) {
        e.preventDefault();
        var form_id = sib.attr("id");
        var data = sib.serialize();
        var url = e_url('admin/product/addSRD');
        $.post(url,data).then(function (res) {
            if(res.error == 0){
                showSuccessMsg(res);
                setTimeout(function () {
                    location.reload();
                },5000);
            }else{
                showErrorMsg(res);
            }


        })


    }
    function addSRD(e,sib) {
        e.preventDefault();
        product_form_div.hide();
        add_product_div.hide();
        srd_div.show();
        var p_id = sib.parent('td').attr("product-id");
        var form_id = 'srd-form';
        $('#'+form_id+' input[name=product_id]').val(p_id);
    }


    function submitProduct(e, sib) {
        e.preventDefault();
        var data = sib.serialize();
        console.log(data);
        var url = e_url('admin/product/addMore');
        $.post(url,data).then(function (res) {
            if(res.error == 0){
                showSuccessMsg(res);
                setTimeout(function () {
                    location.reload();
                },5000);
            }else{
                showErrorMsg(res);
            }
        })
    }




    function submitForm(e, sib) {
        e.preventDefault();
        CKupdate();
        var form_id = sib.attr("id");
        var data = new FormData(document.getElementById(form_id));
        var url = e_url('admin/product/post');
        showSpin();
        $.ajax({
           dataType: 'json',
            type: 'post',
            url: url,
            data: data,
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            success:function (res) {
               hideSpin();
                if(res.error == 0){
                    showSuccessMsg(res);
                    setTimeout(function () {
                        location.reload();
                    },5000);
                }else{
                    showErrorMsg(res);
                }
            }
        });
    }
    
    
    function editProduct(e, sib) {
        var p_id = sib.parent('td').attr("product-id");
        var url = e_url('admin/product/edit/'+p_id);
        var form_id = 'product-form';

        showSpin();
        $.get(url).then(function (res) {

            hideSpin();
            if(res.error == 0){

                product_form_div.show();
                add_product_div.hide();
                srd_div.hide();
                $('#'+form_id+' input[name=id]').val(res.find.id);
                $('#'+form_id+' input[name=name]').val(res.find.name);
                $('#'+form_id+' input[name=rent]').val(res.find.rent);
                $('#'+form_id+' input[name=bar_code]').val(res.find.bar_code);
                $('#'+form_id+' input[name=profit_retail]').val(res.find.profit_retail);
                $('#'+form_id+' input[name=offer]').val(res.find.offer);
                $('#'+form_id+' input[name=profit_whole_sale]').val(res.find.profit_whole_sale);
               // $('#'+form_id+' textarea[name=info]').val(res.find.info);
                CKEDITOR.instances['editor'].setData(res.find.info);
                $('#'+form_id+' select[name=brand]').val(res.find.brand);
                $('#'+form_id+' select[name=category]').val(res.find.category);
            }else{
                showErrorMsg(res);
            }
        })
    }

    function softDeleteProduct(e,sib) {
        e.preventDefault();
        var p_id = sib.parent('td').attr("product-id");
        var url = e_url('admin/product/softDelete/'+p_id);
        $.get(url).then(function (res) {
            if(res.error == 0){
                $('#product'+p_id).remove();
            }else{
                showErrorMsg(res);
            }
        })

    }


    function removeRow(e,sib) {
        e.preventDefault();
        sib.parent('td').parent('tr').remove();
        calculateTotalCost();
    }
    



    function addToCart(e, sib) {
        e.preventDefault();
        var type = $('select[name=saleType]').val();

        if(type == 'retail'){
            var cost = sib.parent('td').attr("retail");
        }else if(type == 'wholesale'){
            var cost = sib.parent('td').attr("wholesale");
        }else{
            var cost = sib.parent('td').attr("rent");
        }

        if(cost == 0 || cost == 'na'){
            alert('This product is not available');

        }else{
            var name = sib.parent('td').attr("product-name");

            var p_id = sib.parent('td').attr("product-id");
            var cp = sib.parent('td').attr("cp")

            $('#cart-table tbody').append('<tr> ' +
                '<td style="padding:0;">'+name+' <input type="text" name="product_id[]" hidden="hidden" value="'+p_id+'"></td> ' +
                '<td style="padding:0;"><input class="qantity" type="number" name="quantity[]" style="width: 100%; height: 35px;margin:0;" onkeyup="addQty(event, $(this));"></td> ' +
                '<td style="padding:0;"><span></span>' +
                '<input class="cost" type="number" name="cost[]" style="width: 100%; height: 35px;margin:0;" hidden="hidden">' +
                '<input class="cost-per-unit" type="number" name="costperunit[]" style="width: 100%; height: 35px;margin:0;" hidden="hidden" value="'+cost+'">' +
                '</td> ' +
                '<td style="padding:0;"><input class="discount" type="number" name="discount[]" style="width: 100%; height: 35px;margin:0;" onkeyup="discountChange(event, $(this));"></td> ' +
                '<td style="padding:0;" class="totalCost"><span></span><input type="number" name="total[]" style="width: 100%; height: 35px;margin:0;" hidden="hidden"></td> ' +
                '<td class=rUnitPrice>'+cp+'</td>'+
                '<td class="rPrice">'+cp+'</td>'+
                '<td> ' +
                '<a href="" onclick="removeRow(event, $(this));"><i class="fa fa-close"></i></a> ' +
                '</td> ' +
                '</tr>');
        }


    }

    function clearCart() {
        $('#cart-table tbody').empty();
        for(var i=0; i<4; i++){
            $('#cart-table').find('.footer').children('th:eq('+(i+1)+')').empty().text(0);
        }

    }


    function selectClient(e, sib) {
        e.preventDefault();
        $('#c-name').empty().text(sib.attr("c-name"));
        $('#cart-form input[name=client_id]').val(sib.attr("c-id"));
        $('#offer').empty().text(sib.attr("offer"));


        var offer = parseInt(sib.attr("offer"));

        var l = $('#cart-table tbody tr').length;



        for(var i=0; i<l; i++){
            var sp = $('#cart-table tbody tr:eq('+i+')').find('.cost').val();
            var discount = parseInt(sp*offer/100);
            var cost_per_unit = $('#cart-table tbody tr:eq('+i+')').find('.discount').val(discount);
            var cost_per_unit = $('#cart-table tbody tr:eq('+i+')').find('.totalCost').children('span').empty().text(sp-discount);
            var cost_per_unit = $('#cart-table tbody tr:eq('+i+')').find('.totalCost').children('input').val(sp-discount);
        }

        calculateTotalCost();

    }

    function removeClient() {
        $('#c-name').empty();
        $('#srd-form input[name=client_id]').val('');
        $('#offer').empty().text(0);
        var discount = 0;
        var l = $('#cart-table tbody tr').length;
        for(var i=0; i<l; i++){
            var sp = $('#cart-table tbody tr:eq('+i+')').find('.cost').val();
            var cost_per_unit = $('#cart-table tbody tr:eq('+i+')').find('.discount').val(discount);
            var cost_per_unit = $('#cart-table tbody tr:eq('+i+')').find('.totalCost').children('span').empty().text(sp-discount);
            var cost_per_unit = $('#cart-table tbody tr:eq('+i+')').find('.totalCost').children('input').val(sp - discount);
        }

        calculateTotalCost();

    }

    function addQty(e, sib) {
        e.preventDefault();
            var cost_per_unit = sib.parent('td').parent('tr').find(".cost-per-unit").val();
            var sp = parseInt(sib.val()*cost_per_unit);
           sib.parent('td').parent('tr').find(".cost").val(sp);
           sib.parent('td').parent('tr').find(".cost").siblings('span').empty().text(sp);
        var offer = parseInt($('#offer').text() == ''?0:$('#offer').text());
        var discount = parseInt(sp*offer/100);
        sib.parent('td').parent('tr').find(".discount").val(discount);
        var total = parseInt(sp - discount);
        sib.parent('td').parent('tr').find(".totalCost").children('span').empty().text(parseInt(total));
        sib.parent('td').parent('tr').find(".totalCost").children('input').val(parseInt(total));
        var rUnitPrice = sib.parent('td').parent('tr').find(".rUnitPrice").text();

        sib.parent('td').parent('tr').find(".rPrice").empty().text(parseInt(rUnitPrice) * sib.val());
        calculateTotalCost();
    }


    function discountChange(e, sib) {
        e.preventDefault();
        var discount = parseInt(sib.val());
        var sp = sib.parent('td').parent('tr').find('.cost').val();
        sib.parent('td').parent('tr').find('.totalCost').children('span').empty().text(sp - discount);
        sib.parent('td').parent('tr').find('.totalCost').children('input').val(sp - discount);

        calculateTotalCost();
    }

    function calculateTotalCost() {


            var l = $('#cart-table tbody tr').length;

            var qty = 0;
            var cost = 0;
            var dis = 0;
            var total = 0;

            for(var i=0; i<l; i++){
                qty += parseInt($('#cart-table tbody tr:eq('+i+') td:eq(1) input').val());
                cost += parseInt($('#cart-table tbody tr:eq('+i+') td:eq(2) .cost').val());
                dis += parseInt($('#cart-table tbody tr:eq('+i+') td:eq(3) input').val());
                total += parseInt($('#cart-table tbody tr:eq('+i+') td:eq(4) input').val());
            }
            $('#cart-table').find('.footer').children('th:eq(1)').empty().text(parseInt(qty));
            $('#cart-table').find('.footer').children('th:eq(2)').empty().text(parseInt(cost));
            $('#cart-table').find('.footer').children('th:eq(3)').empty().text(parseInt(dis));
            $('#cart-table').find('.footer').children('th:eq(4)').empty().text(parseInt(total));

    }


    function submitShoppingCart(e, sib) {
     e.preventDefault();
     var data = sib.serialize();
     var url = e_url('admin/product/submitCart');
     showSpin();
     $.post(url, data).then(function(res){
         hideSpin();
        if(res.error == 0){
                location.reload();
        } else{
            showErrorMsg(res);
        }
     });
    }


    (function () {
        CKEDITOR.replace('editor');

        $('#example').DataTable();
        $('#example1').DataTable();
        $('#example_selling').DataTable();



    })();
    
</script>