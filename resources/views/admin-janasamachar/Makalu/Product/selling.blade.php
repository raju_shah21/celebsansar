@extends('admin.layouts.app')
@section('title','product')
@section('styles')
    <link rel="stylesheet" href="{{url('assets/css/tableFilter.css')}}">
    <style>
        form div {
            background: #eeeeee;

        }
        form{
            background: #eeeeee;
            padding: 50px;
        }

        form input {
            width: 100%;
            height: 50px;
            font-family: Arial;
            font-size: 15px;
            text-align: center;
            background: white;
            text-align-last: center;
            margin-bottom: 10px;
            COLOR: #000071;
            background: white;
        }

        form select {
            width: 100%;
            height: 50px;
            font-family: Arial;
            font-size: 10px;
            text-align: center;
            background: white;
            text-align-last: center;
            margin-bottom: 10px;
            COLOR: #000071;
            background: white;
        }

        form textarea {
            height: 100px;
            width: 100%;
        }

        .hidden-div {
            width: 100%;
            height: 100%;
            overflow: auto;
            background: black;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999;
        }

    </style>

    @include('admin.Makalu.Product.style')
@endsection

@section('content')
    <div class="col-md-12" ng-controller="myCtrl">
        <h1>Selling Info</h1>
        <div id="example_wrapper" class="dataTables_wrapper">
            <table class="table table-bordered table-hover" id="example_selling">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Date</th>
                    <th>
                        Product Name
                    </th>
                    <th>Client Name</th>
                    <th>Quantity</th>
                    <th>SP</th>
                    <th>Action</th>
                </tr>

                </thead>
                <tbody>
                @foreach($spList as $l)
                    <tr>
                        <td>{{$l->srd_id}}</td>
                        <td>{{$l->srd_ct}}</td>
                        <td>{{$l->p_name}}</td>
                        <td>{{$l->c_name}}</td>
                        <td>{{$l->srd_q}}</td>
                        <td>{{$l->srd_cost}}</td>
                        <td p-id="{{$l->srd_id}}">
                            <a href="" onclick="returnProduct(event, $(this));">
                                RETURN
                            </a>
                        </td>
                    </tr>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>


        Total Selling Price : <span id="tsp"></span>
    </div>

@endsection

@section('scripts')
    <script src="{{url('assets/js/tableFilter.js')}}"></script>
    @include('admin.Makalu.Product.selling_script')

@endsection