@extends('admin.layouts.app')
@section('title','product')
@section('styles')
    <link rel="stylesheet" href="{{url('assets/css/tableFilter.css')}}">
    <style>
        form div {
            background: #eeeeee;

        }
        form{
            background: #eeeeee;
            padding: 50px;
        }

        form input {
            width: 100%;
            height: 50px;
            font-family: Arial;
            font-size: 15px;
            text-align: center;
            background: white;
            text-align-last: center;
            margin-bottom: 10px;
            COLOR: #000071;
            background: white;
        }

        form select {
            width: 100%;
            height: 50px;
            font-family: Arial;
            font-size: 10px;
            text-align: center;
            background: white;
            text-align-last: center;
            margin-bottom: 10px;
            COLOR: #000071;
            background: white;
        }

        form textarea {
            height: 100px;
            width: 100%;
        }

        .hidden-div {
            width: 100%;
            height: 100%;
            overflow: auto;
            background: black;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999;
        }

    </style>

    @include('admin.Makalu.Product.style')
@endsection

@section('content')




    <div class="col-md-12">
        <a href="#" onclick="addMore(event, $(this));">Add New Product</a>
    </div>


    <div class="col-md-12">
        <div id="example_wrapper" class="dataTables_wrapper">
            <table class="table table-bordered table-hover" id="example">
                <thead>
                <tr>
                    <th>
                        Product Name
                    </th>
                    <th>Stock(Rent)</th>
                    <th>Stock</th>
                    <th>CP</th>
                    <th>SP (Retail)</th>
                    <th>SP (WholeSale)</th>
                    <th>Offer</th>
                    <th>Rent</th>
                    <th>Action</th>
                </tr>

                </thead>
                <tbody>
                @foreach($list as $l)
                    <tr role="row" id="product{{$l->id}}">

                        <?php
                        $sp = $l->cp + (($l->profit_retail * $l->cp)/100);

                        if($l->offer != 0){
                            $offer = $sp - ($l->offer*$sp)/100;
                        }else{
                            $offer = 'na';
                        }

                        if($l->rent != 0){
                            $rent = $l->rent;
                        }else{
                            $rent = 'na';
                        }
                        ?>
                        <td>{{$l->name}}</td>
                            <td>{{$l->r_stock}}</td>
                        <td>{{$l->stock}}</td>
                        <td>{{$l->cp}}</td>
                        <td>{{ $sp}}</td>
                        <td>{{ $l->cp + (($l->profit_whole_sale * $l->cp)/100) }}</td>
                        <td>{{ $offer}}</td>
                            <td>{{$rent}}</td>
                        <td rent="{{$rent}}" cp="{{$l->cp}}" product-id="{{$l->id}}" product-name="{{$l->name}}" retail="{{$sp - ($l->offer*$sp)/100}}" wholesale="{{$l->cp + (($l->profit_whole_sale * $l->cp)/100) }}">
                            <a href="" onclick="editProduct(event, $(this));"><i class="fa fa-edit"></i></a>
                            <a href="" onclick="softDeleteProduct(event,$(this));"><i class="fa fa-trash"></i></a>
                            <a href="" onclick="addProduct(event, $(this));"><i class="fa fa-plus"></i></a>
                            <a href="" onclick="addSRD(event, $(this));"><i>SRD</i></a>
                            <a href="" onclick="addToCart(event, $(this));"><i class="fa fa-cart-plus"></i></a>
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>


    <div class="col-xs-12" style="background:#242669;color:#f3ac92;">
        <h3>Shopping Card</h3>
        <p>Client : <span id="c-name"></span> | <span id="offer"></span>% Offer
            <small onclick="removeClient();" style="cursor: pointer;float:right">Remove Client</small>
        </p>





        <form id="cart-form" style="background: #242669;padding:0;text-align: center" onsubmit="submitShoppingCart(event, $(this));">
            {{csrf_field()}}
            <input type="number" name="client_id" hidden="hidden">

                <select name="saleType" onchange="clearCart();" style="height: 30px;">
                    <option value="">CHOOSE TYPE</option>
                    <option value="retail">Retail</option>
                    <option value="wholesale">Wholesale</option>
                    <option value="rent">Rent</option>
                </select>


            <table class="table table-bordered" id="cart-table">
                <thead>
                <tr>
                    <th>Product</th>
                    <th>Qty</th>
                    <th>SP</th>
                    <th>Dis</th>
                    <th>Total</th>
                    <th>CP</th>
                    <th>Total Cost</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>

    <thead class="footer">
    <tr class="footer">
        <th>Total</th>
        <th>0</th>
        <th>0</th>
        <th>0</th>
        <th>0</th>
        <th></th>
    </tr>
    </thead>
            </table>


            <input type="submit" value="SUBMIT" class="pull-right btn btn-success" style="width: 70px; height: 35px;">
        </form>

    </div>
    <div class="col-xs-12" style="background: white;margin-top: 10px;">
        <h3>List of User</h3>

        <div id="example_wrapper" class="dataTables_wrapper">
            <table class="table table-bordered table-hover" id="example1">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Email</th>
                    <th>Offer</th>
                </tr>
                </thead>
                <tbody>
                @foreach($client as $l)
                    <tr c-id="{{$l->id}}" offer="{{$l->offer}}" c-name="{{$l->name}}" onclick="selectClient(event, $(this));" style="cursor: pointer;">
                        <td>{{$l->name}}</td>
                        <td>{{$l->address}}</td>
                        <td>{{$l->contact}}</td>
                        <td>{{$l->email}}</td>
                        <td>{{$l->offer}}</td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>

    </div>


@include('admin.Makalu.Product.new-product')
@include('admin.Makalu.Product.addProduct')
@include('admin.Makalu.Product.srd')


@endsection

@section('scripts')
    <script src="{{url('assets/js/tableFilter.js')}}"></script>
    <script src="{{url('plugins/ckeditor/ckeditor.js')}}"></script>
    @include('admin.Makalu.Product.script')

@endsection