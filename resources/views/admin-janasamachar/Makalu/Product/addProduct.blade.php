<div class="hidden-div" id="add-product-div" hidden="hidden">


    <div class="col-md-8 col-md-offset-2" style="background: white;margin-top: 10px;">
        <form onsubmit="submitProduct(event, $(this));" id="product-form1">
            <div class="col-md-12">
                <h1>Add Product</h1>
                {{csrf_field()}}
                <input type="number" name="id" hidden="hidden">
                <input type="number" name="product_id" hidden="hidden">

            </div>
            <input type="text" name="cost" placeholder="COST PER UNIT OF THE PRODUCT">
            <small>Renting: Total Cost <br> Direct Sale : Unit Cost</small>

            <br>


            <input type="number" name="quantity" placeholder="QUANTITY OF THE PRODUCT">


            <label>
                <input type="checkbox" name="pType" value="1" style="width: 20px;height: 14px;">
                Check if the product is for renting purpose. Not for direct sale.</label>

            <input type="submit" value="SUBMIT">


        </form>
    </div>

    <i onclick="closeParent(event, $(this));" class="fa fa-close fa-3x show-pointer delete" style="position: absolute; top:10px;right:10px;"></i>

</div>