<div class="hidden-div" id="product-form-div" hidden="hidden">


    <div class="col-md-8 col-md-offset-2" style="background: white;margin-top: 10px;">
        <form onsubmit="submitForm(event, $(this));" id="product-form">
            <div class="col-md-12">
                <h1>Product Entry Form</h1>
                {{csrf_field()}}
                <input type="number" name="id" hidden="hidden">


                <input type="text" name="bar_code" placeholder="BAR CODE OF THE PRODUCT"> <br>
                <input type="text" name="name" placeholder="NAME OF THE PRODUCT"> <br>


            </div>

            <div class="col-md-6">
                <select name="brand" style="text-transform: uppercase">
                    <option value="">CHOSE BRAND NAME</option>
                    @foreach($brands as $b)
                        <option value="dd{{$b->id}}dd">
                            <i class="text-uppercase"> {{$b->name}}</i>
                        </option>
                    @endforeach

                </select>
            </div>

            <div class="col-md-6">
                <select name="category" style="text-transform: uppercase">
                    <option value="" >CHOSE Category</option>
                    @foreach($cat as $c)
                        <option value="dd{{$c->id}}dd">
                            <i class="text-uppercase"> {{$c->name}}</i>
                        </option>
                            @foreach($c->sub as $sub)
                        <option value="dd{{$c->id}}dd{{$sub[0]}}dd" style="background: #a3a3a3">{{$c->name}} / {{$sub[1]}}</option>
                                @foreach($sub[2] as $f)
                                <option value="dd{{$c->id}}dd{{$sub[0]}}dd{{$f[2]}}dd" style="background: #e9e9e9">{{$c->name}} / {{$sub[1]}} / {{$f[1]}}</option>
                                    @endforeach
                                @endforeach
                    @endforeach

                </select>
            </div>


            <div class="col-md-3">
                <input type="number" name="profit_retail" placeholder="PROFIT PERCENT IN RETAIL">
            </div>
            <div class="col-md-3">
                <input type="number" name="profit_whole_sale" placeholder="PROFIT PERCENT IN WHOLESALE">
            </div>
            <div class="col-md-3">
                <input type="number" name="offer" placeholder="DISCOUNT PERCENT FOR OFFER">
            </div>
            <div class="col-md-3">
                <input type="text" name="rent" placeholder="RENTAL PRICE">
            </div>


            <div class="col-md-6">
                <input type="file" name="image1">
            </div>
            <div class="col-md-6">
                <input type="file" name="image2">
            </div>

            <textarea name="info" rows="50" id="editor">INFO..</textarea>

            <input type="submit" value="SUBMIT">


        </form>
    </div>

    <i onclick="closeParent(event, $(this));" class="fa fa-close fa-3x show-pointer delete" style="position: absolute; top:10px;right:10px;"></i>

</div>