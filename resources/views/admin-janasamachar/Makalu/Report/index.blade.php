@extends('admin.layouts.app')
@section('title','product')
@section('styles')
    <link rel="stylesheet" href="{{url('assets/css/tableFilter.css')}}">
    @include('admin.Makalu.Report.style')
@endsection

@section('content')
<div class="col-md-12" ng-controller="myCtrl">
    <div id="example_wrapper" class="dataTables_wrapper">
        <table class="table table-bordered table-hover" id="example">
            <thead>
            <tr>
                <th>
                    <span>PRODUCT INFO</span> <br>
                    <span>Name</span>
                </th>
                <th>
                    <span>COST INFO</span> <br>
                    <span>Qty | Cost</span>
                </th>
                <th>
                    <span>SELLING INFO</span> <br>
                    <span>Qty | Income</span>
                </th>
                <th>
                    <span>RETURN</span> <br>
                    <span>Qty | Income</span>

                </th>
                <th>
                    <span>DAMAGE</span> <br>
                    <span>Qty | Income</span>
                </th>

                <th>
                    <span>RENTAL INFO</span> <br>
                    <span>Qty | Cost | Income</span>
                </th>

                <th>
                    <span>STOCK INFO</span> <br>
                    <span>Qty | MP</span>
                </th>
                <th>
                    <span>MARGINAL PROFIT</span> <br>
                    <span>Profit</span>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php

            $t_cost_qty = 0;
            $t_cost_cost = 0;

            $t_sold_qty = 0;
            $t_sold_cost = 0;

            $t_return_qty = 0;
            $t_return_cost = 0;

            $t_damage_qty = 0;
            $t_damage_cost = 0;

            $t_stock_qty = 0;
            $t_stock_cost = 0;

            $t_rent_stock = 0;
            $t_rent_cost = 0;
            $t_rent_income = 0;


            $t_profit = 0;
            ?>
            @foreach($list as $l)
                <?php

            $t_cost_qty += $l->cost_info[0];
            $t_cost_cost += $l->cost_info[1];


            $t_sold_qty += $l->sold[0];
            $t_sold_cost += $l->sold[1];

            $t_return_qty += $l->return_info[0];
            $t_return_cost += $l->return_info[1];

            $t_damage_qty += $l->damage_info[0];
            $t_damage_cost += $l->damage_info[1];

            $t_stock_qty += $l->stock;
            $t_stock_cost += $l->cp;

                $t_rent_stock += $l->rent_info[0];
                $t_rent_cost += $l->rent_info[1];
                $t_rent_income += $l->rent_info[2];


                $m_profit = ($l->sold[1] + $l->damge[1] + $l->return[1] + $l->rent_info[2] + $l->stock * $l->cp) - ($l->cost_info[1] - $l->rent_info[1]);




                $t_profit += $m_profit;
                ?>


            <tr role="row">
                <td>{{$l->name}}</td>
                <td>{{$l->cost_info[0] }} | {{  $l->cost_info[1]}}</td>
                <td>{{$l->sold[0] }} | {{  $l->sold[1]}}</td>
                <td>{{$l->return[0] }} | {{  $l->return[1]}}</td>
                <td>{{$l->damage[0] }} | {{  $l->damage[1]}}</td>
                <td>

                    {{$l->rent_info[0]}} |
                    {{$l->rent_info[1]}} |
                    {{$l->rent_info[2]}}
                </td>

                <td>
                    {{$l->stock}} | {{$l->stock * $l->cp}}
                </td>

                <td>

                    {{$m_profit}}
                </td>
            </tr>
                @endforeach
            </tbody>

         <thead>
         <tr>
             <th>
               Total

             </th>
             <th>
                 {{$t_cost_qty}} | {{$t_cost_cost}}
             </th>
             <th>
                 {{$t_sold_qty}} | {{$t_sold_cost}}
             </th>
             <th>
                 {{$t_return_qty}} | {{$t_return_cost}}
             </th>
             <th>
                 {{$t_damage_qty}} | {{$t_damage_cost}}
             </th>
             <th>
                 {{$t_rent_stock}} | {{$t_rent_cost}} | {{$t_rent_income}}
             </th>
             <th>
                 {{$t_stock_qty}} | {{$t_stock_cost}}
             </th>
             <th>
                 {{$t_profit}}
             </th>

         </tr>
         </thead>

        </table>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{url('assets/js/tableFilter.js')}}"></script>
    @include('admin.Makalu.Report.script')

@endsection