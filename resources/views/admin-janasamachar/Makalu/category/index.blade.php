@extends('admin.layouts.app')
@section('title','product')
@section('styles')

    <link rel="stylesheet" href="{{url('assets/css/tableFilter.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>

    </style>

    @include('admin.Makalu.category.style')
@endsection

@section('content')



    <div class="col-md-3">
        <h3>Add New Category</h3>
        <form action="{{url('admin/makalu/category/new')}}" method="post">
            {{csrf_field()}}
            <input type="text" name="id" hidden="hidden">
            <input type="text" name="name" placeholder="MAIN CATEGORY NAME..." class="form-control">
            <input type="submit" value="submit">
        </form>
    </div>
    <div class="col-md-6">



        <h1>List</h1>
        <ul id="sortable">
            @foreach($cat as $c)
                <li class="ui-state-default main-menu cat-tree " cat-id="{{$c->id}}">
                    <div m_id="{{$c->id}}">
                        {{$c->name}}
                        <input class="input-tag" type="text" name="name" value="{{$c->name}}" style="color:red;"
                               hidden="hidden" onkeyup="postEditMain(event,$(this));">
                        <span class="pull-right">
                           <a href="" style="color:#e9e9e9;" onclick="editMain(event, $(this));"><i
                                       class="fa fa-edit"></i></a> |
                            <a href="" style="color:#e9e9e9;" onclick="deleteMain(event, $(this));"><i
                                        class="fa fa-trash"></i></a>
                       </span>
                    </div>
                    <ul id="sortable{{$c->id}}" hidden="hidden">
                        @foreach($c->sub as $s)
                            @if(isset($s[0]))
                                <li class="ui-state-default sub-menu cat-tree" cat-id="{{$s[0]}}">
                                    <div m_id="{{$s[0]}}">
                                        {{$s[1]}}
                                        <input class="input-tag" type="text" name="name" value="{{$s[1]}}"
                                               style="color:red;" hidden="hidden"
                                               onkeyup="postEditMain(event,$(this));">
                                        <span class="pull-right">
                           <a href="" style="color:#e9e9e9;" onclick="editMain(event, $(this));"><i
                                       class="fa fa-edit"></i></a> |
                                            <a href="" style="color:#e9e9e9;" onclick="deleteMain(event, $(this));"><i
                                                        class="fa fa-trash"></i></a>
                       </span>
                                    </div>
                                    @if(isset($s[2]))
                                        <ul hidden="hidden">
                                            @foreach($s[2] as $f)
                                                <li style="list-style: none;margin-bottom:2px;">
                                                    <div m_id="{{$f[2]}}">{{$f[1]}}
                                                        <input class="input-tag" type="text" name="name"
                                                               value="{{$f[1]}}" style="color:red;" hidden="hidden"
                                                               onkeyup="postEditMain(event,$(this));">

                                                        <span class="pull-right">
                           <a href="" style="color:#e9e9e9;" onclick="editMain(event, $(this));"><i
                                       class="fa fa-edit"></i></a> |
                                                            <a href="" style="color:#e9e9e9;"
                                                               onclick="deleteMain(event, $(this));"><i
                                                                        class="fa fa-trash"></i></a>
                       </span>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>


    </div>
    <div class="col-md-3">
        <h3>Add Sub Section</h3>
        <form action="{{url('admin/makalu/category/sub')}}" method="post" id="sub-form">
            {{csrf_field()}}

            <p id="main-menu"></p>
            <p id="sub-menu"></p>
            <input type="text" name="id" hidden="hidden">
            <input type="number" name="m_id" hidden="hidden">
            <input type="number" name="s_id" hidden="hidden">
            <input type="text" name="name" placeholder="MAIN CATEGORY NAME..." class="form-control">
            <input type="submit" value="submit">

        </form>


    </div>



@endsection

@section('scripts')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#sortable").sortable();
            $("#sortable").disableSelection();
        });
    </script>


    <script>
        $sortable = $('#sortable');
        $l = $sortable.children('li').length;


        for(var j=0; j<$l; j++){
         var id1 = $('#sortable').children('li:eq('+j+')').attr("d-id");
         $( "#sortable"+id1 ).sortable();
         $( "#sortable"+id1 ).disableSelection();
         }
        (function () {
            $("#sortable").sortable({
                update: function () {
                    var obj = [];

                    for (var i = 0; i < $l; i++) {
                        var id = $('#sortable').children('li:eq(' + i + ')').attr("cat-id");
                        console.log(id);
                        var aa = {
                            id: id,
                            sort: (i + 1)
                        }
                        obj.push(aa);
                    }


                    $.get("{{url('sortCat/update')}}", {'obj': obj}).then(function (res) {
                    });
                }
            });

            $( "#sortable li ul" ).sortable({
             update: function() {
             var s_id = $(this).attr("id");
             var l1= $('#'+s_id).children(' li').length;
             var obj = [];
             for(var i=0; i<l1; i++){

             var id = $('#'+s_id).children('li:eq('+i+')').attr("cat-id");

             var aa = {
             id : id,
             sort: (i+1)
             }
             obj.push(aa);
             }
             $.get("{{url('sortCat/update')}}",{'obj':obj}).then(function (res) {
             });
             }
             });

        })();
    </script>


    <script src="{{url('assets/js/tableFilter.js')}}"></script>
    @include('Admin.Makalu.category.script')




@endsection