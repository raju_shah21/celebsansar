<script>
    var myApp = angular.module("myModule",[]);


    function editMain(e,sib) {
        e.preventDefault();
        $('.input-tag').hide();

        var rrr = sib.parent('span').siblings('input');
            if(rrr.is(':visible')){
                rrr.hide();
            }else{
                rrr.show();
            }
    }

    function postEditMain(e,sib) {
       if(e.keyCode == 13){
           e.preventDefault();
           $.get(e_url('admin/makalu/category/edit'),{'name':sib.val(),'id':sib.parent('div').attr("m_id")}).then(function(res){
               location.reload();
           });
       }
    }

    function deleteMain(e,sib) {
        e.preventDefault();
        $.get(e_url('admin/makalu/category/delete/'+sib.parent('span').parent('div').attr("m_id"))).then(function(res){
            if(res.error == 2){
                showErrorMsg(res);
            }else{
                location.reload();

            }
        });
    }


    (function () {
        $('.cat-tree div').click(function(){
            var sib = $(this);
            if(sib.parent('li').hasClass("main-menu")){
                $('#sub-form input[name=m_id]').val(sib.attr("m_id"));
                $('#main-menu').empty().text(sib.text());

                $('#sub-form input[name=s_id]').val('');
                $('#sub-menu').empty();

            }else if(sib.parent('li').hasClass("sub-menu")){
                $('#sub-form input[name=s_id]').val(sib.attr("m_id"));
                $('#sub-menu').empty().text(sib.text());
            }
            var child1 = sib.siblings('ul');
           if(child1.is(':visible')){
               child1.hide("slow");
           } else{
               child1.show("slow");
           }
            sib.parent('li').siblings('li').children('ul').hide("slow");
        });
    })();
</script>