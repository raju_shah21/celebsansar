<div class="hidden-div" id="client-form-div" hidden="hidden">


    <div class="col-md-8 col-md-offset-2" style="background: white;margin-top: 10px;">
        <form action="{{url('admin/client/post')}}" method="post" id="client-form">
            <h1>Client Entry Form</h1>
            <hr>
            {{csrf_field()}}
            <input type="number" name="id" hidden="hidden">
            <input type="text" name="name" placeholder="CLIENT NAME..">
            <input type="text" name="address" placeholder="CLIENT ADDRESS..">
            <input type="text" name="contact" placeholder="CLIENT CONTACT NUMBER..">
            <input type="text" name="email" placeholder="CLIENT EMAIL ADDRESS..">
            <input type="number" name="offer" placeholder="DISCOUNT TO THE CUSTMER..">
            <input type="submit" value="SUBMIT">

        </form>
    </div>

    <i onclick="closeParent(event, $(this));" class="fa fa-close fa-3x show-pointer delete" style="position: absolute; top:10px;right:10px;"></i>

</div>