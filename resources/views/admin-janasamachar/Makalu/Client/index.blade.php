@extends('admin.layouts.app')
@section('title','product')
@section('styles')
    <link rel="stylesheet" href="{{url('assets/css/tableFilter.css')}}">

    <style>
        form div {
            background: #eeeeee;

        }
        form{
            background: #eeeeee;
            padding: 50px;
        }

        form input {
            width: 100%;
            height: 50px;
            font-family: Arial;
            font-size: 15px;
            text-align: center;
            background: white;
            text-align-last: center;
            margin-bottom: 10px;
            COLOR: #000071;
            background: white;
        }

        form select {
            width: 100%;
            height: 50px;
            font-family: Arial;
            font-size: 10px;
            text-align: center;
            background: white;
            text-align-last: center;
            margin-bottom: 10px;
            COLOR: #000071;
            background: white;
        }

        form textarea {
            height: 100px;
            width: 100%;
        }

        .hidden-div {
            width: 100%;
            height: 100%;
            overflow: auto;
            background: black;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 999;
        }

    </style>
    @include('admin.Makalu.Client.style')
@endsection

@section('content')

<div class="col-md-12">
    <a href="" onclick="addNewClient(event, $(this));">Add New</a> Total Client [{{count($list)}}]

</div>
<div id="example_wrapper" class="dataTables_wrapper">
    <table class="table table-bordered table-hover" id="example">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Address</th>
                <th>Contact</th>
                <th>Email</th>
                <th>Offer</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $l)
                <tr>
                    <td>  {{$l->id}}</td>
                    <td>{{$l->name}}</td>
                    <td>{{$l->address}}</td>
                    <td>{{$l->contact}}</td>
                    <td>{{$l->email}}</td>
                    <td>{{$l->offer}}</td>
                    <td c-id="{{$l->id}}">
                        <a href="" onclick="editClient(event, $(this));"><i class="fa fa-edit"></i></a>
                        <a href="" onclick="(event, $(this));"><i class="fa fa-trash"></i></a>
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>








    @include('admin.Makalu.Client.addNew')
@endsection

@section('scripts')
    <script src="{{url('assets/js/tableFilter.js')}}"></script>
    @include('admin.Makalu.Client.script')

@endsection