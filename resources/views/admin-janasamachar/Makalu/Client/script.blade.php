<script>

    var myApp = angular.module("myModule",[]);

    function addNewClient(e,sib) {
        e.preventDefault();
        $('#client-form-div').show();
    }


    function editClient(e, sib) {
        e.preventDefault();
        var id = sib.parent('td').attr("c-id");
        $.get(e_url('admin/client/edit/'+id)).then(function (res) {
            var find = res.find;
            $('#client-form-div').show();
            $('#client-form input[name=id]').val(find.id);
            $('#client-form input[name=name]').val(find.name);
            $('#client-form input[name=address]').val(find.address);
            $('#client-form input[name=contact]').val(find.contact);
            $('#client-form input[name=offer]').val(find.offer);
            $('#client-form input[name=email]').val(find.email);
            $('#client-form input[type=submit]').val('UPDATE');
        });



    }

    (function () {
        $('#example').DataTable();
    })();
</script>