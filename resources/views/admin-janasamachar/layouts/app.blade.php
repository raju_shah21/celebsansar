<!DOCTYPE html>
<html lang="en" ng-app="myModule">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="_token" content="{{ csrf_token() }}"/>
  <title>@yield('title')</title>


  {{ HTML::style("cms/css/bootstrap.min.css")}}
  <!-- Font Awesome Icons -->
  {{ HTML::style("cms/fonts/css/font-awesome.min.css")}}
    <!-- Theme style -->
  {{ HTML::style("cms/css/animate.min.css")}}
{{ HTML::style("vue/css/toastr.css")}}

  {{ HTML::style('cms/css/custom.css')}}
  <!-- iCheck -->
  {{--{{ HTML::style('cms/css/maps/jquery-jvectormap-2.0.3.css')}}--}}

  {{ HTML::style('cms/css/icheck/flat/green.css')}}
  {{--{{ HTML::style('cms/css/floatexamples.css')}}--}}

  {{ HTML::script('cms/js/jquery.min.js')}}
  {{ HTML::script('cms/js/nprogress.js')}}
	<script src="{{url('vue/vue.js')}}"></script>

	<script src="{{url('vue/vue-resource.js')}}"></script>
	<script src="{{url('vue/toastr.js')}}"></script>
@yield('styles')

  <style>




    #msg-div{
      position: fixed;top:50%; left:50%;}


    .preeti{
      font-family:preeti;
    }
    .pcs{
      font-family:pcs;
      font-size:12px;
    }


    .ganess{
      font-family: ganess;
    }


    .show-pointer{
      cursor: pointer;
      color:black;
    }

    .show-pointer:hover{
      color:red;
    }

    .edit{
      color:blue;
    }

    .delete{
      color:orange;
    }


    #success-msg, #error-msg{
      position: fixed;
      top:30%;
      z-index: 999999;
    }


    .back-cover{
      background: black;width: 100%;height: 100%;overflow: auto;position: fixed;top:0;left:0;z-index: 999;
    }
  </style>

{{ HTML::style('cms/css/dbros.css')}}

  </head>


  <body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('db-admin') }}" class="site_title"><i class="fa fa-home"></i> <span>{{$siteName or null}}</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="{{url('v/images/img.jpg')}}" alt="{{$siteName or null }}" class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2>{{ Auth::user()->name }}</h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />

          <!-- sidebar menu -->
          @include('admin.layouts.partials.sidebar')
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a href="{{url('logout')}}" data-toggle="tooltip" data-placement="top" title="Logout">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="images/img.jpg" alt="">
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">

                  <li>
                    <a href="javascript:;">Help</a>
                  </li>
                  <li><a href="{{url('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </li>
                </ul>
              </li>

            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
          <div class="row">
						<span class="section alert alert-danger" id="message"></span>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{{ $pageName or 'DBROS TECHNOLOGY PVT. LTD | Contact: 9860486008 | 9843408105 | Email:dbrostechnology@gmail.com'}}</h2>

                  <div class="clearfix"></div>
                </div>
								@yield('content')
              </div>

            </div>
          </div>


        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">Maintained by <a href="http://dbrostech.com">Dbros Technology Pvt. Ltd.</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>





  <div class="col-md-4 col-md-offset-4" id="success-msg" hidden="hidden">
    <div class="panel-group">
      <div class="panel panel-success">
        <div class="panel-heading">Success Message</div>
        <div class="panel-body">

        </div>
      </div>
    </div>
  </div>


  <div class="col-md-4 col-md-offset-4" id="error-msg" hidden="hidden">
    <div class="panel-group">
      <div class="panel panel-danger">
        <div class="panel-heading">Error!</div>
        <div class="panel-body">

        </div>
      </div>
    </div>
  </div>

  @if(count($errors->all())>0)
    <div style="background-color: red; color:white; position: fixed;top:50%;left:50%;" id="error-div">
      @foreach($errors->all() as $err)
        <p>{{$err}}</p>
      @endforeach
    </div>
  @endif



  @if(Session::has('msg'))
    <div style="background-color: red; color:white; position: fixed;top:50%;left:50%;" id="success-div">
      <p>{{session('msg')}}</p>
    </div>
  @endif

  <div id="spinner" class="text-center" style="padding:5px;position:fixed; top:50%; left:50%; background-color: red;border:2px solid blue; color: white;display:none;">
    <i class="fa fa-spin fa-spinner fa-3x"></i><br>Loading
  </div>

{{ HTML::script('cms/js/bootstrap.min.js')}}

  <!-- bootstrap progress js -->
    {{--{{ HTML::script('cms/js/progressbar/bootstrap-progressbar.min.js')}}--}}
    {{ HTML::script('cms/js/nicescroll/jquery.nicescroll.min.js')}}
  <!-- icheck -->
    {{ HTML::script('cms/js/icheck/icheck.min.js')}}

{{ HTML::script('cms/js/custom.js')}}

{{ HTML::script('admin/js/dbros.js')}}
  <!-- pace -->
  {{ HTML::script('cms/js/pace/pace.min.js')}}
{{ HTML::script('cms/js/datepicker/daterangepicker.js')}}


  <script src="{{url('app/lib/angular/angular.min.js')}}"></script>
@yield('scripts')
<script type="text/javascript">
       $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
    </script>



  <script>



      function e_url(index) {
          return "{{url('/')}}"+"/"+index;
      }


      function resetForm(id) {
          $('#'+id)[0].reset();
      }



      function showSuccessMsg(data) {
          toastr.success(data.msg, 'Success Alert', {timeOut: 5000});

      }
      function closeParent(e,sib) {
          e.preventDefault();
          sib.parent('div').hide();
      }

      function showErrorMsg(data) {
         // $('#error-msg').show();

          var html = '';
          if(data.error == 1){
              var l = data.errors.length;
              for(var i=0; i<l; i++){
                  html += '<li>'+data.errors[i]+'</li>'
              }
          }else{
              html += '<li>'+data.msg+'</li>';
          }

          toastr.error(html, 'Error Alert', {timeOut: 5000});

         // $('#error-msg').find('.panel-body').empty().append('<ul>'+html+'</ul>');
      }

      function hideSpin(){
          $('#spinner').hide();
      }

      function showSpin(){
          $('#spinner').show();
      }


      function CKupdate(){
          for ( instance in CKEDITOR.instances )
              CKEDITOR.instances[instance].updateElement();
      }
      (function(){

          $('#success-msg').click(function(){
              $(this).hide();
          });

          $('#error-msg').click(function(){
              $(this).hide();
          });

          $('#error-div').click(function(){
              $(this).hide();
          });

          $('#success-div').click(function(){
              $(this).hide();
          });




      })();
  </script>
</body>

</html>
