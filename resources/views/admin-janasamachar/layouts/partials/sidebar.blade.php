<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

	<div class="menu_section">
		<h3><a style="color:yellow" href="{{url('/')}}">Visit Site</a></h3>
		<ul class="nav side-menu">

			<li><a><i class="fa fa-book"></i> Posts <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{ url('posts/create') }}">Create New Post</a>
					</li>
					<li><a href="{{ url('posts/list') }}">Posts List</a>
					</li>
					<li><a href="{{ url('cat/create') }}">Category</a>
						<li><a href="{{ url('v/posts') }}">posts</a>
					</li>

				</ul>
			</li>
			<li><a><i class="fa fa-sticky-note-o"></i> Pages <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{ url('pages/create') }}">Create New Page</a>
					</li>
					<li><a href="{{ url('pages/list') }}">Page List</a>
					</li>

				</ul>
			</li>
			<li><a><i class="fa fa-picture-o"></i> Albums1 <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{ url('albums/create') }}">Create New Album</a>
					</li>

				</ul>
			</li>


			<li><a><i class="fa fa-picture-o"></i> REPORT <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{ url('admin/report/index') }}">Index</a>
					</li>

				</ul>
			</li>

			<li><a><i class="fa fa-picture-o"></i> PRODUCT <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{ url('admin/product/index') }}">INDEX</a>
					<li><a href="{{ url('admin/product/selling') }}">SELLING INFO</a>
					</li>

				</ul>
			</li>
			<li><a><i class="fa fa-picture-o"></i> SINGLE PAGE <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{ url('admin/single/rent') }}">RENT</a></li>
					<li><a href="{{ url('admin/single/wholesale') }}">WHOLE SALE</a></li>
					<li><a href="{{ url('admin/single/testo') }}">TESTOMONIALS</a></li>
					<li><a href="{{ url('admin/single/msg') }}">MESSAGES</a></li>
					<li><a href="{{ url('admin/single/online-orders') }}">ONLINE ORDERS</a></li>

				</ul>
			</li>

			<li><a><i class="fa fa-picture-o"></i> BRAND <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{ url('admin/brand/index') }}">INDEX</a>
					</li>

				</ul>
			</li>
			<li><a><i class="fa fa-picture-o"></i> CLIENTS <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{ url('admin/client/index') }}">Index</a>
					</li>

				</ul>
			</li>

			<li><a><i class="fa fa-picture-o"></i> CATEGORY <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{ url('admin/makalu/category/index') }}">INDEX</a>
					</li>

				</ul>
			</li>


			<li><a href="{{ url('admin/product/index') }}">Index</a></li>



			<li><a><i class="fa fa fa-bars"></i> Menu <span class="fa fa-chevron-down"></span></a>
			 <ul class="nav child_menu" style="display: none">
               <li>
                  <a href="{{ url('cat/create') }}">
                  </i>Create New Category
                  </a>
               </li>
               <li>
                  <a href="{{ url('menus/create') }}">
                Create New Menu
                  </a>
               </li>
							 <li>
                  <a href="{{ url('menu') }}">
                  </i>Menu builder
                  </a>
               </li>

            </ul>
         </li>
				 <li><a><i class="fa fa-camera"></i> Media <span class="fa fa-chevron-down"></span></a>
				 <ul class="nav child_menu" style="display: none">
							<li>
								 <a href="{{ url('media/list') }}">
								 Media
								 </a>
							</li>
							<li>
								<a href="{{ url('file/list') }}">
								</i>Media table
								</a>
						 </li>
            </ul>
         </li>
				 <li><a><i class="fa fa-fire"></i> Onair Schedule <span class="fa fa-chevron-down"></span></a>
				 <ul class="nav child_menu" style="display: none">
					 <li>
							<a href="{{ url('rj/create') }}">
							Create RJ
							</a>
					 </li>
					 <li>
							<a href="{{ url('program/create') }}">
							Create Program
							</a>
					 </li>
							<li>
								 <a href="{{ url('schedule/create') }}">
								 Create Onair Schedule
								 </a>
							</li>
							<li>
								<a href="{{ url('schedule/list') }}">
								</i>Schedule list
								</a>
						 </li>
            </ul>
         </li>

				 <li><a><i class="fa fa-caret-square-o-right"></i> Slider <span class="fa fa-chevron-down"></span></a>
				 <ul class="nav child_menu" style="display: none">
               <li>
                  <a href="{{ url('slider/create') }}">
                  Create Slider
                  </a>
               </li>
            </ul>
         </li>
				 <li><a><i class="fa fa-user"></i> Users <span class="fa fa-chevron-down"></span></a>
				 <ul class="nav child_menu" style="display: none">
               <li>
                  <a href="{{ url('users') }}">
                  Users Lists
                  </a>
               </li>
            </ul>
         </li>
				 <li><a><i class="fa fa-unlock"></i> Roles <span class="fa fa-chevron-down"></span></a>
				 <ul class="nav child_menu" style="display: none">
               <li>
                  <a href="{{ url('roles') }}">
                Role Lists
                  </a>
               </li>
               <li>
                  <a href="{{ url('roles/create') }}">
                  Create New Role
                  </a>
               </li>
            </ul>
         </li>

				 <li><a><i class="fa fa-users"></i> Teams <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu" style="display: none">
               <li>
                  <a href="{{ url('team/create') }}">
                  Create New Member
                  </a>
               </li>
            </ul>
         </li>
				 <li><a><i class="fa fa-bullhorn"></i> Advertisement Mgnt <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu" style="display: none">
								<li>
									 <a href="{{ url('ads/create') }}">
									 Create New Ads
									 </a>
								</li>

						 </ul>
					</li>
				 <li><a><i class="fa fa fa-cogs"></i> Site Settings <span class="fa fa-chevron-down"></span></a>
				 <ul class="nav child_menu" style="display: none">
               <li>
                  <a href="{{ url('options') }}">
                  Theme Options
                  </a>
               </li>
					 <li>
						 <a href="{{ url('options/postSetting') }}">
							 Post Settings
						 </a>
					 </li>
            </ul>
         </li>


		</ul>
	</div>

</div>
