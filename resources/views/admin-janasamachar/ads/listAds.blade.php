@extends('admin.layouts.app')
@section('content')
    <div class="x_content">


        @if(!empty($ads))
            <?php $i = 0;?>
            @foreach($ads as $value)
                <div class="row" id="{{$value->aid}}" style="border-bottom:1px solid #ddd; margin-bottom:20px">

                    <div class="col-md-6">
                        <?php
                            $remain = [];
                            $warnning = 0;
                            //init if empty date flag false
                            $noEntryDate = false;
                            //init percentage to handle error
                            $percentage = 0;

                        if(empty($value->start_date) || empty($value->valid_date)){
                            $noEntryDate = true;
                        }else {
                             $now = Carbon\Carbon::now();

                            $microStart = strtotime($value->start_date);
                            $microEnd = strtotime($value->valid_date);

                            $microNow = strtotime($now);
                             //total duration for ad or valid duration
                             $duration = $microEnd - $microStart;
                            // remaining time to expire -->
                            $timeRemain = $microEnd - $microNow;


                            if($timeRemain <= 0){
                                 $warnning = 1;

                                $end = new Carbon\Carbon($value->valid_date);
                                $remain = $end->diff($now);
                            }else{
                                $end = new Carbon\Carbon($value->valid_date);
                                $remain = $end->diff($now);
                            }

                             //time used by ad
                             $timePassed = $microNow - $microStart;

                            if ($duration > 0 && $timeRemain >0)
                                $percentage = ($timePassed / $duration) * 100;
                            else
                                $percentage =100;


                        }
                           ?>

                    </div>

                    <div class="col-xs-12">
                        <div class="information">
                            <div class="caption">
                                <div class="col-md-12">
                                    @if(!empty($value->filename))
                                        <?php $url = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . "icons" . DIRECTORY_SEPARATOR . $value->filename;
                                        $url1 = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . $value->filename;
                                        $exists = File::exists($url);
                                        $exists1 = File::exists($url1);
                                        ?>
                                        @if($exists || $exists1)
                                            <?php
                                            if ($exists)
                                                $path = "icons";
                                            elseif ($exists1)
                                                $path = "uploads";
                                            ?>
                                            <img style="width: 100%; display: block;"
                                                 src="{{ url($path."/".$value->filename)}}"/>

                                        @endif
                                    @else
                                        <img style="width: 100%; display: block;" src="{{ url("images/default.png")}}"/>
                                    @endif
                                </div>
                                <div class='col-lg-12'>
                                    <div class="adstop" style="padding:10px">


                                        <span class="pull-right label label-<?php echo ($value->status == null) ? "success" : "danger";?>">
                                            <?php echo ($value->status == null) ? "Enabled" : "Disabled";?></span>

                                            <span>
                                                <input class="action" type="checkbox" value="{{$value->aid}}" title="<?=($value->status == null) ? "click to remove" : "click to publish";?>" <?=($value->status == null) ? "checked='checked'" : "";?>>
                                            </span>
                                        <span class=" glyphicon glyphicon-user"> by <a href="#">{{App\User::getUserNameById($value->user_id)}}</span>
                                        <a href="">
                                            <span id="{{ $value->aid }}" class="glyphicon glyphicon-edit pull-right text-primary"></span>
                                        </a>
                                        <a href="">
                                            <span id="{{ $value->aid }}" class="glyphicon glyphicon-trash pull-right text-danger"></span>
                                        </a>
                                    </div>

                                </div>
                                <div class='col-lg-12 well well-add-card'>
                                    <a name="poll_bar"></a>
                                    @if($percentage > 0)
                                        <span name="poll_val">{{number_format($percentage,2)}}%</span>
                                    @endif

                                </div>
                                <div class='col-lg-6'>
                                    <h2>Title: <span name="title">{{$value->adsName}}</span></h2>
                                    <h5><b>Start at:</b><span name="startDate" >{{substr($value->start_date,0,-8)}}</span></h5>
                                    <h5><b>End at:</b><span name="endDate">{{substr($value->valid_date,0,-8)}}</span> </h5>

                                    <h5><b>Rate Amount:</b> <span name="amount" id="amt">{{$value->amount}}</span></h5>
                                    <h5><b>Link Url:</b> <span name="link">{{$value->link_url}}</span></h5>
                                    <p id="desc" class="text-muted">{{ $value->adsDesc }}</p>


                                </div>
                                <div class='col-lg-4 pull-right'>
                                    <h3>Valid Date</h3>
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                        @if($noEntryDate)
                                        {{"Please Publish Start Date and Expire Date of Ad For More Details"}}

                                        @else

                                             @if($warnning == 1)
                                            <h2 class="disableIt" id="{{$value->aid}}" style="color:red;" >{{"Date Expired! Please renew"}}</h2>

                                            @else
                                                @if($remain->y >0)
                                                    {{$remain->y." year"}}
                                                @endif
                                                @if($remain->m > 0)
                                                    {{$remain->m." months"}}
                                                @endif
                                                @if($remain->d > 0)
                                                    {{$remain->d." days"}}
                                                @endif
    {{$remain->h."hr ".$remain->i."min ".$remain->s."sec"}}

                                            @endif

                                        @endif

                                        </li>



                                    </ul>

                                </div>

                                <span class='glyphicon glyphicon-exclamation-sign text-primary pull-right icon-style'></span>
                            </div>
                        </div>
                    </div>

                </div>

    </div>



    <?php $i++;?>
    @endforeach
    @endif
    </div>



    @include('admin.ads.createForm')
@endsection
@section('styles')
    <style>
        .adstop span {
            padding-right: 10px;

        }

        .information {
            border: 1px solid #ddd;
        }

        .list-group-horizontal .list-group-item {
            display: inline-block;
        }

        .well {
            min-height: 20px;
            padding: 0px;
            margin-bottom: 20px;
            background-color: #D9D9D9;
            border: 1px solid #D9D9D9;
            border-radius: 0px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            padding-left: 15px;
            border: 0px;
        }

        .thumbnail .caption {
            padding: 9px;
            color: #333;
            padding-left: 0px;
            padding-right: 0px;
        }

        .icon-style {
            margin-right: 15px;
            font-size: 18px;
            margin-top: 20px;
        }

        p {
            margin: 3px;
        }

        .well-add-card {
            margin-bottom: 10px;
        }

        .btn-add-card {
            margin-top: 20px;
        }

        .thumbnail {
            display: block;
            padding: 4px;
            margin-bottom: 20px;
            line-height: 1.42857143;
            background-color: #fff;
            border: 6px solid #D9D9D9;
            border-radius: 15px;
            -webkit-transition: border .2s ease-in-out;
            -o-transition: border .2s ease-in-out;
            transition: border .2s ease-in-out;
            padding-left: 0px;
            padding-right: 0px;
        }

        .btn {
            border-radius: 0px;
        }

        .btn-update {
            margin-left: 15px;
        }

    </style>
@stop
@section('scripts')

    <script type="text/javascript">
        /*
         * trigger modal add description form
         */
        $(".glyphicon-edit").click(function (e) {
            e.preventDefault();
            $(".modal").modal('show');
            //add ads id in form
            $("input[name='id']").val($(this).attr('id'));

            var name = $("span[name='title']").text()
            var startDate = $("span[name='startDate']").text();
            var endDate = $("span[name='endDate']").text();
            var link = $("span[name='link']").text();
            var amount = $('#amt').text();

            var desc = $("p#desc").text();


            $("input[name='name']").val(name);
            $("input[name='start_date']").val(startDate);
            $("input[name='valid_date']").val(endDate);
            $("input[name='description']").val(desc);
            $("input[name='link_url']").val(link);
            $("input[name='amount']").val(amount);

        })
        /*
         soft delete
         publish or unpublish ads at frontend
         */
        $('.action').change(function () {
            var id = $(this).val();
            $.ajax({
                url: "{{ url('ads/publish') }}",
                type: 'post',
                data: {'id': id},
                success: function (data) {
                    console.log("success");
                }
            })
        });
        /*
         save data when form submit
         reset form if success

         */
         //Serialize the Form
var values = {};


//Value Retrieval Function
var getValue = function (valueName) {
    return values[valueName];
};



        $("#saveDesc").click(function (e) {
            e.preventDefault();
            var data = $("form").serialize();
//             $.each($("form").serializeArray(), function (i, field) {
//     values[field.name] = field.value;
// });
            $.ajax({
                type: 'POST',
                url: "{{ url('ads/store') }}",
                data: data,
                dataType: 'html',
                success: function (data) {
                    var res = JSON.parse(data);
                    if (res.code == 200) {

                        $("form").trigger("reset");
                        $(".modal").modal('hide');
                        location.reload();
                        // var name = $("span[name='title']").html(getValue('name'))
                        // var startDate = $("span[name='startDate']").text(getValue('start_date'));
                        // var endDate = $("span[name='endDate']").text(getValue('valid_date'));
                        // var link = $("span[name='link']").text(getValue('link_url'));
                        // var amount = $('#amt').text(getValue('amount'));

                        // var desc = $("p#desc").text(getValue('description'));
                        alert("successfully added");

                    }

                }
            });

        })

        // $(".fa-edit").click(function (e) {
        //     e.preventDefault();
        //     $(".modal").modal('show');
        //     var sib = $(this).siblings('input');
        //     var id = sib.val();
        //     $.ajax({
        //         type: 'POST',
        //         url: "{{ url('editImageCaption') }}",
        //         data: {id: id},
        //         dataType: 'JSON',
        //         success: function (data) {
        //             //var rep = JSON.parse(data);
        //             if (data.code == 200) {
        //                 //append hidden id
        //                 $("#id").val(id);
        //                 $("#title").val(data.title);
        //                 $("#description").val(data.description);

        //             } else {
        //                 alert("data not found");
        //             }

        //         }
        //     });
        // });

        /*
         forceDelete ads
         */
        $(".glyphicon-trash").click(function (e) {
            e.preventDefault();
            var id = $(this).attr("id");

            $.ajax({
                type: 'POST',
                url: "{{ url('ads/destroy') }}",
                data: {id: id},
                dataType: 'html',
                success: function (data) {

                    if (data.code == 200) {
                        $(div + '#' + id).remove();

                    }

                }
            });
        });




    </script>


@endsection
