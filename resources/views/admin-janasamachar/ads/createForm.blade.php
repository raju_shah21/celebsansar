<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Description</h4>
                <h4 class="msg"></h4>
            </div>
            <div class="modal-body">
                <div class="box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create New</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="ads">
                        <input type="hidden" name="id">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Ads Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Ads Name">
                            </div>
                            <div class="form-group">
                                <label>Ads Description</label>
                                <input type="text" class="form-control" name="description" placeholder="Ads Description">
                            </div>
                            <div class="form-group">
                                <label>Payment Amount</label>
                                <input type="number" class="form-control" name="amount" placeholder="Payment Amount">
                            </div>
                            <div class="form-group">
                                <label>Link Url</label>
                                <input type="text" class="form-control" name="link_url" placeholder="link url">
                            </div>
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="text" class="form-control" name="start_date" placeholder="Start Date">
                            </div>
                            <div class="form-group">
                                <label>End Date</label>
                                <input type="text" class="form-control" name="valid_date" placeholder="End Date">
                            </div>
                            <!-- /.box-body -->

                    </form>
                </div>
                <!-- /.box -->
            </div>


        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button id="saveDesc" type="button" class="btn btn-primary">Save changes</button>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
