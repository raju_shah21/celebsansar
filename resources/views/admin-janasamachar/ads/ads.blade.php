
<div class="col-md-12">
  <div class="box-body">
      <div class="dataTables_wrapper form-inline dt-bootstrap" id="example1_wrapper">
          <div class="row">
            <div class="col-md-12 pull-right">
              
            </div>
              <div class="col-md-12 pull-right">
                 <h4 class="pull-right">Currently Published Ads</h4>
              </div>

          </div>
          <div class="row">
            <div class="col-md-12">
               <table id="example1" class="table table-bordered table-striped dataTable">
                  <thead>
                     <tr role="row">
                        <th>S.N</th>
                        <th>Image</th>
                        <th>Ads Place</th>
                        <th>Author</th>
                        <th>Amount</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                     
                    
                     </tr>
                  </thead>
                  <tbody>
                    @if(!$ads->isEmpty())
                    <?php $i=0; ?>
                     @foreach($ads as $value)
                       <?php $i++;?>
                     <tr role="row" class="odd" id="{{$value->id}}">
                        <td sn="{{$i}}">{{ $i }}</td>
                        <td>@if(!empty($value->filename))<img src="{{ url('uploads/'.$value->filename )}}" width="200px" />@endif</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->user_id }}</td>
                        <td>{{ $value->amount }}</td>
                        <td>{{ $value->start_date }}</td>
                        <td>{{ $value->end_date }}</td>
                  
                     
                     </tr>
                     @endforeach
                   @endif
                  </tbody>
               </table>
            </div>
          </div>
          <div class="row">
              <div class="col-sm-5">
                  <div aria-live="polite" role="status" id="example1_info" class="dataTables_info">
                    <p>These Advertisement are currently displayed in homepage</p>
                  </div>
              </div>
              <div class="col-sm-7">
                  <!-- pagination here-->
              </div>
          </div>
      </div>
  </div>
</div>
