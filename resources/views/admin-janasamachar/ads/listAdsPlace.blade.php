<ul id="sortable">
    @if(!$adsPlaces->isEmpty())
        <?php $i = 0;?>
        @foreach($adsPlaces as $value)

            <li class="ui-state-default" id="{{ $value->id }}">
                <div class="col-md-12" style="padding:0" id="{{ $value->id }}">
                    <a href="">
                        <div class="thumbnail ">
                            <div class="image view view-first">

                                    <h1>{{$value->id}}</h1>

                                <div class="mask">
                                    <p>
                                        {{$value->name or 'your name here'}}
                                    </p>
                                    <a href="{{ url('ads/list/'.$value->id) }}" alt="click to see ads">
                                        <h2>{{$value->name or 'ads name here'}}</h2>
                                    </a>
                                    <p>
                                        {{$value->link_url or 'ads link url here'}}
                                    </p>
                                    <div class="tools tools-bottom">
                                        <a href=""><i id="{{ $value->id }}" class="fa fa-link"></i></a>
                                        <a href="#"><i id="{{ $value->id }}" class="fa fa-plus"></i></a>
                                        <?php $check = $value->deleted_at;?>
                                        <input class="action" type="checkbox" value="{{$value->id}}"
                                               title="<?=($check == null) ? "click to remove" : "click to publish";?>" <?=($check == null) ? "checked='checked'" : "";?>>
                                    </div>
                                </div>
                            </div>
                            <div class="caption">
                                <?php $ads = App\Ads::where('place_id',$value->id)->withTrashed()->get();
                                echo '<p>'.count($ads).' ads</p>';
                                ?>
                                <a href="#" alt="click to upload ads"><i id="{{ $value->id }}" class="fa fa-plus"></i> Upload New Ads</a>

                            </div>
                        </div>
                    </a>
                </div>
            </li>
            <?php $i++;?>
        @endforeach
    @endif
</ul>
