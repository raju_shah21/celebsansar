<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Upload Ads Image</h4>
                <h4 class="msg"></h4>
            </div>
            <div class="modal-body">

                <div class="col-md-12" id="upload">
                    {!! Form::open(['url' => route('ads.upload'), 'class' => 'dropzone', 'files'=>true, 'id'=>'my-awesome-dropzone']) !!}
                    <input type="hidden" name="place_id" value="">
                    <div class="dz-message">
                    </div>
                    <div class="fallback">
                        <input name="file" type="file" multiple/>
                    </div>
                    <div class="dropzone-previews" id="dropzonePreview"></div>
                    <h4 style="text-align: center;color:#428bca;">Drop images in this area <span
                                class="glyphicon glyphicon-hand-down"></span></h4>
                    {!! Form::close() !!}
                    <p></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>           
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
