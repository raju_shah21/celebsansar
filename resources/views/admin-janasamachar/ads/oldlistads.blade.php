<div class="row">
    <p>Ads</p>
    <ul id="sortable">
        @if(!empty($ads))
            <?php $i=0;?>
            @foreach($ads as $value)

                <li class="ui-state-default" id ="{{ $value->aid }}" >

                    <div class="col-md-12" style="padding:0;" id="{{ $value->aid }}" >
                        <a href="">
                            <div class="thumbnail" style="padding:0;<?php echo ($value->status != null)?"border: 2px solid red;":"";?>" >
                                <div class="image view view-first">
                                    @if(!empty($value->filename))
                                        <?php $url = base_path().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR."icons".DIRECTORY_SEPARATOR.$value->filename;
                                        $url1 = base_path().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR.$value->filename;
                                        $exists = File::exists($url);
                                        $exists1 = File::exists($url1);
                                        ?>
                                        @if($exists || $exists1)
                                            <?php
                                            if($exists)
                                                $path = "icons";
                                            elseif($exists1)
                                                $path = "uploads";
                                            ?>
                                            <img style="width: 100%; display: block;" src="{{ url($path."/".$value->filename)}}"/>

                                        @endif
                                    @else
                                        <img style="width: 100%; display: block;" src="{{ url("images/default.png")}}"/>
                                    @endif
                                    <div class="mask">
                                        <p>
                                            {{$value->name or 'your name here'}}{{ "place ".$value->place_id }}
                                        </p>
                                        <div class="tools tools-bottom">
                                            <a href=""><i id="{{ $value->aid }}" class="fa fa-trash-o"></i></a>
                                            <a href="#"><i id="{{ $value->aid }}" class="fa fa-pencil"></i></a>

                                            <input class="action" type="checkbox" value="{{$value->aid}}" title="<?=($value->status==null)?"click to remove":"click to publish";?>" <?=($value->status==null)?"checked='checked'":"";?>>
                                        </div>
                                    </div>
                                </div>
                                <div class="caption">
                                    <p>{{$value->name or 'ads name here'}} {{ "place ".$value->place_id }}</p>
                                    <p>
                                        {{$value->link_url or 'ads link url here'}}
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <?php $i++;?>
            @endforeach
        @endif
    </ul>
</div>