@extends('admin.layouts.app')
@section('title','Advertisement | Management')
@section('pageName','Create New Ad' )
@section('content')

    <div class="row">

        <div class="x_content">
            <div class="row">
                @include('admin.ads.listAdsPlace')
            </div>
        </div>
        {{--@include('admin.ads.listAds')--}}

        @include('admin.ads.uploadAds')

        @include('admin.ads.ads')
    </div>

@endsection
@section('styles')
    <style>
        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #sortable li {
            float: left;
            margin-left: 20px;
            width: 200px;
            overflow: hidden;
        }

    </style>
    {!! HTML::style('admin/assets/dropzone/dropzone.css') !!}
    {!! HTML::style('/admin/css/jquery-ui.css') !!}

@stop
@section('scripts')
    {!! HTML::script('admin/assets/dropzone/dropzone.js') !!}
    {!! HTML::script('/admin/js/jquery-ui.js') !!}
    {{ HTML::script('admin/js/checkbox.js')}}
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        //hide dropzone

        (function ($) {
            $('#sortable').sortable({
                update: function () {
                    var data = save_new_order()
                    $.ajax({
                        url: "{{ url("ads/order") }}",
                        type: 'post',
                        data: data,
                    })
                }
            });
            function save_new_order() {
                var a = [];
                $('#sortable').children().each(function (i) {
                    a.push($(this).attr('id'));
                });
                var s = {start: a}
                return s;
            }

            $("#ads").click(function (e) {
                e.preventDefault();
                var data = $("form").serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ url('ads/store') }}",
                    data: data,
                    dataType: 'html',
                    success: function (data) {
                        var res = JSON.parse(data);
                        if (res.code == 200) {
                            alert('ok' + res.id);

                            $("form").trigger("reset");
                            $("#upload").show();
                            $("[name='place_id']").val(res.id);
                        }

                    }
                });

            })

            $(".fa-plus").click(function (e) {
                e.preventDefault();
                $('input[name="place_id"]').val($(this).attr('id'));

                $(".modal").modal('show');
            })
            $(".fa-pencil").click(function (e) {
                e.preventDefault();
                $('input[name="place_id"]').val($(this).attr('id'));

                $(".createmodal").modal('show');
            })
        })(jQuery);


        var maxImageWidth = 1000, maxImageHeight = 700;

        Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element

            // The configuration we've talked about above
            autoProcessQueue: false,
            uploadMultiple: false,
            parallelUploads: 100,
            maxFilesize: 8, // MB
            addRemoveLinks: true,
            dictRemoveFile: 'Remove',
            dictFileTooBig: 'Image is bigger than 8MB',
            dictRemoveFileConfirmation: "Are you sure you wish to delete this image?",

            init: function () {
                var th = this;
                this.on("uploadprogress", function (file, progress) {
                    console.log("File progress", progress);
                });
                this.on("thumbnail", function (file) {
                    console.log("height:" + file.height + " width:" + file.width);


                });
                this.on("complete", function (file) {

                    setTimeout(function () {
                        th.removeAllFiles();
                    }, 5000);

                });
                this.on("success", function (file, response) {
                    var url = "{{ url('uploads')}}" + "/";
                    console.log(response.id);
                    var h = '<li class="ui-state-default thumbnail" id="' + response.id + '">';
                    h += '<label class="checkbox-inline">';
                    h += '<input class="action" type="checkbox" value="' + response.id + '" title="click to remove" checked="checked">';
                    h += '<img src="' + url + response.filename + '"/>';
                    h += '</label></li>';

                    $("#sortable").append(h);

                })

            },

        }

        (function ($) {


            $(".thumbnail").mouseover(function () {
                var child = $(this).children('.fa-trash-o');
                child.removeClass("hidden");
                $(".thumbnail").mouseout(function () {
                    $(this).children('.fa-trash-o').addClass("hidden");


                })
            })



            $('.action').change(function () {
                var id = $(this).val();
                $.ajax({
                    url: "{{ url('albums/check') }}",
                    type: 'post',
                    data: {'id': id},
                    success: function (data) {
                        console.log("success");
                    }
                })
            });


        })(jQuery);


    </script>
@endsection
