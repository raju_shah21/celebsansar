

@extends('admin.layouts.app')
@section('title','Type | create')
@section('pageName','Create New Team' )
@section('content')
<div class="row">
<div class="col-md-6">
   <!-- general form elements -->
   <div class="box-primary">
      <div class="box-header with-border">
         <h3 class="box-title">Create New</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" id="ads">
         <div class="box-body">
            <div class="form-group">
               <label for="exampleInputEmail1">Ads Name</label>
               <input type="text" class="form-control" name="name" placeholder="Ads Name">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Link Url</label>
               <input type="text" class="form-control" name="link_url" placeholder="link url">
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
               <button type="submit" id="createAlbum" class="btn btn-primary">Submit</button>
            </div>
      </form>
      </div>
      <!-- /.box -->
   </div>
</div>
<div class="clearfix"></div>
<div class="x_content">
   <div class="row">
      <p>Ads</p>
      <ul id="sortable">
      @if(!empty($ads))
        <?php $i=0;?>
      @foreach($ads as $value)

          <li class="ui-state-default" id ="{{ $value->aid }}" name="cat">
      <div class="col-md-12" style="padding:0" id="{{ $value->aid }}">
         <a href="">
            <div class="thumbnail">
               <div class="image view view-first">
                 @if(!empty($value->filename))
                  <?php $url = base_path().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR."icons".DIRECTORY_SEPARATOR.$value->filename;
                  $url1 = base_path().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR.$value->filename;
                     $exists = File::exists($url);
                     $exists1 = File::exists($url1);
                     ?>
                  @if($exists || $exists1)
                    <?php
                    if($exists)
                    $path = "icons";
                    elseif($exists1)
                       $path = "uploads";
                    ?>
                    <img style="width: 100%; display: block;" src="{{ url($path."/".$value->filename)}}"/>

                  @endif
                @else

                    <img style="width: 100%; display: block;" src="{{ url("images/default.png")}}"/>


                @endif
                  <div class="mask">
                     <p>
                        {{$value->name or 'your name here'}}
                     </p>
                     <div class="tools tools-bottom">
         <a href=""><i id="{{ $value->aid }}" class="fa fa-link"></i></a>
         <a href="#"><i id="{{ $value->aid }}" class="fa fa-plus"></i></a>
         <?php $check = $value->deleted_at;?>
         <input class="action" type="checkbox" value="{{$value->aid}}" title="<?=($check==null)?"click to remove":"click to publish";?>" <?=($check==null)?"checked='checked'":"";?>>
         </div>
         </div>
         </div>
         <div class="caption">
         <p>{{$value->name or 'ads name here'}}</p>
         <p>
         {{$value->link_url or 'ads link url here'}}
         </p>
         </div>
         </div>
         </a>
      </div>
    </li>
<?php $i++;?>
      @endforeach
      @endif
      </ul>
   </div>
</div>
<div class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Add Description</h4>
            <h4 class="msg"></h4>
         </div>
         <div class="modal-body">
            <div class="col-md-12" id="upload">
               {!! Form::open(['url' => route('media.upload'), 'class' => 'dropzone', 'files'=>true, 'id'=>'my-awesome-dropzone']) !!}
               <input type="hidden" name="post_type" value="ads">
               <input type="hidden" name="link_id" value="@if(!empty($link_id)){{ $link_id }}@endif">
               <div class="dz-message">
               </div>
               <div class="fallback">
                  <input name="file" type="file" multiple />
               </div>
               <div class="dropzone-previews" id="dropzonePreview"></div>
               <h4 style="text-align: center;color:#428bca;">Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4>
               {!! Form::close() !!}
            </div>
            <p>Please fill form for slider descriptions</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button id="saveDesc" type="button" class="btn btn-primary">Save changes</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('styles')
<style>
#sortable { list-style-type: none; margin: 0; padding: 0; }

#sortable li{float: left; margin-left: 20px; width:200px; overflow:hidden;}

</style>
{!! HTML::style('admin/assets/dropzone/dropzone.css') !!}
{!! HTML::style('/admin/css/jquery-ui.css') !!}

@stop
@section('scripts')
{!! HTML::script('admin/assets/dropzone/dropzone.js') !!}
{!! HTML::script('/admin/js/jquery-ui.js') !!}
{{ HTML::script('admin/js/checkbox.js')}}
<script type="text/javascript">
   //hide dropzone

   (function($){
     $('#sortable').sortable({
         update: function () {
             var data = save_new_order()
             $.ajax({
                 url:"{{ url("ads/order") }}",
                 type:'post',
                 data: data,
             })
         }
     });
     function save_new_order() {
         var a = [];
         $('#sortable').children().each(function (i) {
             a.push($(this).attr('id'));
         });
         var s={start:a}
         return s;
     }
     $("#ads").click(function(e){
       e.preventDefault();
       var data = $("form").serialize();
       $.ajax({
                   type: 'POST',
                   url: "{{ url('ads/store') }}",
                   data: data,
                   dataType: 'html',
                   success: function(data){
                       var res= JSON.parse(data);
                       if(res.code == 200)
                       {
                         alert('ok'+res.id);

                       $("form").trigger("reset");
                       $("#upload").show();
                       $("[name='link_id']").val(res.id);
                       }

                   }
       });

     })

     $(".fa-plus").click(function(e){
       e.preventDefault();
       $('input[name="link_id"]').val($(this).attr('id'));
       $(".modal").modal('show');
     })
   })(jQuery);


   var maxImageWidth = 1000, maxImageHeight = 700;
   Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element

     // The configuration we've talked about above
     autoProcessQueue: true,
     uploadMultiple: false,
     parallelUploads: 100,
     maxFilesize: 8, // MB
     addRemoveLinks: true,
     dictRemoveFile: 'Remove',
     dictFileTooBig: 'Image is bigger than 8MB',
     dictRemoveFileConfirmation: "Are you sure you wish to delete this image?",

     init: function() {
       var th = this;
         this.on("uploadprogress", function(file, progress) {
           console.log("File progress", progress);
         });
         this.on("thumbnail", function (file) {
           console.log("height:"+file.height+" width:"+file.width);


     });
         this.on("complete", function(file) {

           setTimeout(function(){
             th.removeAllFiles();
           },5000);

         });
         this.on("success", function(file, response){
           var url = "{{ url('uploads')}}"+"/";
           console.log(response.id);
           var h = '<li class="ui-state-default thumbnail" id="'+response.id+'">';
           h+='<label class="checkbox-inline">';
           h+='<input class="action" type="checkbox" value="'+response.id+'" title="click to remove" checked="checked">';
           h+='<img src="'+url+response.filename+'"/>';
           h+='</label></li>';

           $("#sortable").append(h);

         })

       },

   }

     (function($){


       $(".thumbnail").mouseover(function(){
         var child = $(this).children('.fa-trash-o');
         child.removeClass("hidden");
         $(".thumbnail").mouseout(function(){
           $(this).children('.fa-trash-o').addClass("hidden");


         })
       })
       $(".fa-trash-o").click(function(){
         var sib = $(this).siblings('label.checkbox-inline').children('input');
         var id = sib.val();

         $.ajax({
                 type: 'GET',
                 url: "{{ url('albums/destroy') }}",
                 data: {id:id},
                 dataType: 'html',
                 success: function(data){
                     var rep = JSON.parse(data);
                     if(rep.code == 200)
                     {


                       $('#'+id).remove();

                     }

                 }
             });
       })



       $('.action').change(function() {
       var id = $(this).val();
           $.ajax({
             url:"{{ url('albums/check') }}",
             type:'post',
             data: {'id':id},
             success:function(data){
               console.log("success");
             }
           })
       });



     })(jQuery);




</script>
@endsection
