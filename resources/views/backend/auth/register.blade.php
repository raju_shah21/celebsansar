@extends('backend.layout.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto ">
           
            <!-- Default form register -->
            <form class="text-center border border-login p-5 mt-4" method="post">
              {{ csrf_field() }}

                <p class="h4 mb-4">Sign up</p>

                <div class="form-row mb-4">
                    <div class="col">
                        <!-- First name -->
                        <input type="text" id="defaultRegisterFormFirstName" class="form-control" placeholder="First name" name="firstname">
                    </div>
                    <div class="col">
                        <!-- Last name -->
                        <input type="text" id="defaultRegisterFormLastName" class="form-control" placeholder="Last name" name="lastname">
                    </div>
                </div>

                <!-- E-mail -->
                <input type="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="E-mail" name="email">

                <!-- Password -->
                <input type="password" id="defaultRegisterFormPassword" class="form-control" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock" name="password">
                <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                    At least 8 characters and 1 digit
                </small>

              

                <!-- Sign up button -->
                <button class="btn btn-primary my-4 btn-block" type="submit">Sign Up</button>

                <hr>

                <!-- Terms of service -->
                <p>By clicking
                    <em>Sign up</em> you agree to our
                    <a href="{{ route('register')  }}" target="_blank">terms of service</a>

            </form>
            <!-- Default form register -->

    </div>
</div>
@endsection
