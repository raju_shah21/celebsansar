@extends('backend.layout.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto ">
          
              <!-- Default form login -->
              <form class="text-center border border-login p-5 mt-5" action="{{ route('login') }}" method="POST">
                {{ @csrf_field() }}

                  <h1 class="h4 mb-4 text-primary" style="font-weight: 500;font-size: 28px;">Sign in</h1>
                  <img src="img/logo.png" alt="" width="120px" class="mb-4">

                  @if ($errors->has('email'))
                      <span class="help-block text-danger">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif


                 

                  <!-- Email -->
                  <input type="email" class="form-control mb-4" placeholder="E-mail" name="email">
                

                  <!-- Password -->
                  <input type="password" class="form-control mb-4" placeholder="Password" name="password">

                  <div class="d-flex justify-content-around">
                      <div>
                          <!-- Remember me -->
                          <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                              <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
                          </div>
                      </div>
                      <div>
                          <!-- Forgot password -->
                          <a href="">Forgot password?</a>
                      </div>
                  </div>


                  <!-- Sign in button -->
                  <button class="btn btn-primary btn-block my-4" type="submit">Sign in</button>

                  <!-- Register -->
                  <p>Not a member?
                      <a href="{{ route('register') }}">Register</a>
                  </p>

              </form>
              <!-- Default form login -->
        </div>
    </div>
</div>
@endsection
