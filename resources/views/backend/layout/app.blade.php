<!DOCTYPE html>
<html lang="en">

<head>
	@include('backend.partials.head')
</head>
<body>

	<div class="wrapper">
		    
		    @yield('content')


		  	@include('backend.partials.script')

		  	<!-- for other custom js required by one or other file -->
		  	@yield('script')		


  	</div>	
</body>
</html>



