@extends('backend.layout.app')


@section('title','User | Edit')

@section('content')



         

      @include('backend.partials.sidebar')


      
    <div class="main-panel">
      <!-- Navbar -->
      @include('backend.partials.navbar')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="text-center">
            
                  <button type="button" class="btn btn-danger btn-round" data-toggle="modal" data-target="#createModal" data-whatever="@mdo">
                    
                       Edit User
                  </button>
            
          </div>


        


          <div class="col-md-10 mx-auto">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">Edit User</h4>
              </div>
              <div class="card-body mt-4">
                  {!! Form::model($row ,['route' => ['user.update',$row["id"]] , 
                      'class'=>'text-center' , 
                      'method' => 'post' ,
                      'id' => 'form1' ,
                      ]) !!}
                  {{  @csrf_field()}}


                  <div class="form-group bmd-form-group">

                    {!!  Form::label('firstname', 'Firstname', ['class' => 'bmd-label-floating']) !!}
                    {!! Form::text('firstname', null , ['class' => 'form-control' ]) !!}

                  </div>


                  <div class="form-group bmd-form-group">

                    {!!  Form::label('lastname', 'Lastname', ['class' => 'bmd-label-floating']) !!}
                    {!! Form::text('lastname', null , ['class' => 'form-control' ]) !!}

                  </div>


                  <div class="form-group bmd-form-group">

                    {!!  Form::label('email', 'Email', ['class' => 'bmd-label-floating']) !!}
                    {!! Form::email('email', null , ['class' => 'form-control' ]) !!}
                  </div>

                  <div class="form-group bmd-form-group is-focused">

                    {!!  Form::label('password', 'password', ['class' => 'bmd-label-floating is-filled']) !!}
                    {!! Form::password('password' , ['class' => 'form-control' ]) !!}

                  </div>


                  <a href="{{ route('category.show') }}" class="btn btn-danger">Back</a>
                  {!! Form::submit('Update',['class'=>'btn btn-primary']) !!}


              </div>    <!-- card body end -->
            </div>
          </div>
        </div>
      </div>

     @include('backend/partials/footer')
    
  </div>

 

    
@endsection