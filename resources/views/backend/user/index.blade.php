@extends('backend.layout.app')


@section('title','User')

@section('content')

	

	@include('backend.partials.sidebar')



	<div class="main-panel">
	    <!-- Navbar -->
	    @include('backend.partials.navbar')
	    <!-- End Navbar -->
	  <div class="content">
	    <div class="container-fluid">
	      <div class="text-center">
	        
	              <button type="button" class="btn btn-danger btn-round" data-toggle="modal" data-target="#createModal" data-whatever="@mdo">
	                
	                   Add New User
	              </button>



	        
	              @if(request()->session()->has('success_message'))
	                <div class="alert alert-primary mx-auto alert-dismissable col-md-6 col-md-offset-2">
	                       <a href="#" class="close " data-dismiss="alert" aria-label="close">
	                        <i class="pe-7s-close" style="font-size:30px;font-weight:bold;color:black"></i>
	                         </a>
	                          <strong>Success!</strong> {!! request()->session()->get('success_message') !!}

	                </div>

	                @endif
	      </div>



	      {{-- form modal for adding user --}}
	      <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog" role="document">
	          <div class="modal-content">
	            <div class="modal-header">
	              <h4 class="modal-title" id="exampleModalLabel">Add New User</h4>
	              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	              </button>
	            </div>
	                {!! Form::open(['route' => 'user.store' , 
	                    'method' => 'post' ,
	                    ]) !!}
	                {{  @csrf_field()}}
	            <div class="modal-body">
	             <!--  <form> -->
	                <div class="md-form">
	                  {!!  Form::label('firstname', 'Firstname', ['class' => 'bmd-label-floating']) !!}
	                  {!! Form::text('firstname', null , ['class' => 'form-control' ]) !!}

	                </div>
	                <div class="md-form">
	                  {!!  Form::label('lastname', 'Lastname', ['class' => 'bmd-label-floating']) !!}
	                  {!! Form::text('lastname', null , ['class' => 'form-control' ]) !!}

	                </div>
	                <div class="md-form">
	                  {!!  Form::label('email', 'Email', ['class' => 'bmd-label-floating']) !!}
	                  {!! Form::text('email', null , ['class' => 'form-control' ]) !!}

	                </div>
	                <div class="md-form">
	                  {!!  Form::label('password', 'password', ['class' => 'bmd-label-floating']) !!}
	                  {!! Form::password('password' , ['class' => 'form-control' ]) !!}

	                </div>
	              <!-- </form> -->
	            </div>
	            <div class="modal-footer">
	              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	              <button type="submit" class="btn btn-primary">Save</button>
	            </div>
	              {!! Form::close() !!}
	          </div>
	        </div>
	      </div>



	    


	      <div class="col-md-12">
	        <div class="card">
	          <div class="card-header card-header-primary">
	            <h4 class="card-title ">All User</h4>
	          </div>
	          <div class="card-body">
	            <div class="table-responsive">
	              <table class="table table-hover">
	                <thead class="text-primary">
	                  <tr><th>
	                    ID
	                  </th>
	                  <th>
	                    Name
	                  </th>
	                  <th>
	                    Email
	                  </th>
	                  <th>
	                    Action
	                  </th>
	                </tr></thead>
	                <tbody>

	                  @foreach($users as $user)
	                  <tr>
	                    <td>
	                      {{ $user->id }}
	                    </td>
	                    <td>
	                      {{ $user->firstname}} &nbsp; {{ $user->lastname }}
	                    </td>
	                    <td>
	                     {{ $user->email }}
	                    </td>
	                    
                    	<td style="color:white;" >

		                     <a class="btn btn-primary btn-sm editcategory" href="{{ route('user.edit',$user->id) }}">
		                      <i class="fa fa-pencil"></i><div class="ripple-container"></div></a>
							
		                      
		                     <a  class="btn btn-danger btn-sm deletecategory btn-delete-m" 
		                      href="{{ route('user.delete',$user->id) }}" >
		                   				 <i class="fa fa-trash"></i>
		                   	 </a>
                    
                    	</td>
	                  </tr>
	                  @endforeach
	                 
	                </tbody>
	              </table>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>

	    @include('backend/partials/footer')
	</div>
    
@endsection


	  @include('backend/partials/delete_confirm')
