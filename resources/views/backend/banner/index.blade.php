@extends('backend.layout.app')

@section('title','Banner Manager')

@section('style')
<style>
  .ad-col{
        box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        margin: 4px;
        flex: 0 0 32.333333%;
        /*margin: 0 auto !important;*/
  }
  .btn-group > .btn{
      padding: 12px 20px;
  }
</style>
@endsection

@section('content')

	

	@include('backend.partials.sidebar')



	<div class="main-panel">
	    <!-- Navbar -->
	    @include('backend.partials.navbar')
	    <!-- End Navbar -->
	  <div class="content">
	    <div class="container-fluid">
	      

        <!-- header banner -->
	      <div class="col-md-12 pb-4">
	        <div class="card">
	          <div class="card-header card-header-primary">
                 <h3>Header Banner</h3>
	          </div>
              <div class="card-body p-5">
                  {!! Form::open(['route' => 'banner.store' , 
                            'class'=>'text-center' , 
                            'method' => 'post' ,
                            'id' => 'form1' ,
                            'enctype' =>'multipart/form-data'
                            ]) !!}
                 {{  @csrf_field()}}
                 <input type="hidden" value="header" name="position" />
                 <input type="hidden" value="long" name="type" />

                <div class="input-group">
                    <div class="custom-file">
                      {!! Form::file('banner_image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}
                      {!! Form::label('banner_image' , 'Choose Long Ad', ['class'=>'custom-file-label']) !!}
                    </div>
                </div>
                <button class="btn btn-md btn-primary" id="headerbanner">Upload</button>
                {!! Form::close() !!}

                <div class="row">
                  @if(count($headerBanner) > 0)
                    @foreach($headerBanner as $banner)
                      <div class="col-md-4 ad-col">


                        <input type="text" class="link form-control mb-1" placeholder="Link...." value="{{ $banner->link }}" name="link">
                         <input type="text" name="id" value="{{ $banner->id }}" hidden="hidden">
                        <img src="{{ asset('/photos/1/banner/header/'.$banner->image) }}" class="img-fluid">
                        <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn  {{ $banner->status==1?'btn-primary':'btn-default' }}  live" value="{{ $banner->id }}" name="live">
                              Live
                          </button>
                          <button type="button" class="btn {{ $banner->status==0?'btn-primary':'btn-default' }}  offline " value="{{ $banner->id }}" name="offline">
                              Offline
                          </button>
                          <button type="button" class="btn btn-default save">Save</button>
                        </div>

                      </div>
                    @endforeach
                  @else
                    <p>No Long Ads</p>

                  @endif
                </div>

              </div>
	         
	        </div>
	      </div>
        <!-- header banner -->

        <!-- slider banner -->
        <div class="col-md-12 pb-4">
          <div class="card">
            <div class="card-header card-header-success">
              <h3>Slider Banner</h3>
            </div>
            <div class="card-body">

              <div class="row">
                <div class="col-md-6">
                  <p class="text-danger">Long ad</p>
                  
                    {!! Form::open(['route' => 'banner.store' , 
                                'class'=>'text-center' , 
                                'method' => 'post' ,
                                'id' => 'form2' ,
                                'enctype' =>'multipart/form-data'
                                ]) !!}
                     {{  @csrf_field()}}
                     <input type="hidden" value="slider" name="position" />
                     <input type="hidden" value="long" name="type" />

                    <div class="input-group">
                        <div class="custom-file">
                          {!! Form::file('banner_image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}
                          {!! Form::label('banner_image' , 'Choose Long Ad', ['class'=>'custom-file-label']) !!}
                        </div>
                    </div>
                    <button class="btn btn-md btn-primary" id="sliderlongbanner">Upload</button>
                    {!! Form::close() !!}

                </div>


                <div class="col-md-6">
                  
                  <p class="text-danger">Short ad</p>
                  {!! Form::open(['route' => 'banner.store' , 
                              'class'=>'text-center' , 
                              'method' => 'post' ,
                              'id' => 'form3' ,
                              'enctype' =>'multipart/form-data'
                              ]) !!}
                   {{  @csrf_field()}}
                   <input type="hidden" value="slider" name="position" />
                   <input type="hidden" value="short" name="type" />

                  <div class="input-group">
                      <div class="custom-file">
                        {!! Form::file('banner_image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}
                        {!! Form::label('banner_image' , 'Choose Short Ad', ['class'=>'custom-file-label']) !!}
                      </div>
                  </div>
                  <button class="btn btn-md btn-primary" id="slidershortbanner">Upload</button>
                  {!! Form::close() !!}

                </div>
              </div>

              <div class="row">
                @if(count($sliderLongBanner) > 0)
                  @foreach($sliderLongBanner as $banner)
                    <div class="col-md-4 ad-col">


                      <input type="text" class="link form-control mb-1" placeholder="Link...." value="{{ $banner->link }}" name="link">
                       <input type="text" name="id" value="{{ $banner->id }}" hidden="hidden">
                      <img src="{{ asset('/photos/1/banner/slider/'.$banner->image) }}" class="img-fluid">
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn  {{ $banner->status==1?'btn-primary':'btn-default' }}  live" value="{{ $banner->id }}" name="live">
                            Live
                        </button>
                        <button type="button" class="btn {{ $banner->status==0?'btn-primary':'btn-default' }}  offline " value="{{ $banner->id }}" name="offline">
                            Offline
                        </button>
                        <button type="button" class="btn btn-default save">Save</button>
                      </div>

                    </div>
                  @endforeach
                @else
                  <p>No Long Ads</p>

                @endif
              </div>
              <hr>
                <div class="row">
                  @if(count($sliderShortBanner) > 0)
                    @foreach($sliderShortBanner as $banner)
                      <div class="col-md-4 ad-col">


                        <input type="text" class="link form-control mb-1" placeholder="Link...." value="">
                         <input type="text" name="id" value="{{ $banner->id }}" hidden="hidden">
                        <img src="{{ asset('/photos/1/banner/slider/'.$banner->image) }}" class="img-fluid">
                        <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn  {{ $banner->status==1?'btn-primary':'btn-default' }}  live" value="{{ $banner->id }}" name="live">
                              Live
                          </button>
                          <button type="button" class="btn {{ $banner->status==0?'btn-primary':'btn-default' }}  offline " value="{{ $banner->id }}" name="offline">
                              Offline
                          </button>
                          <button type="button" class="btn btn-default save">Save</button>
                        </div>

                      </div>
                    @endforeach
                  @else
                    <p>No short Ads</p>
                  @endif
                </div>



              
            </div>
          </div>
        </div>
        <!-- slider banner -->

        <!-- 1st section banner -->
        <div class="col-md-12 pb-4">
          <div class="card">
            <div class="card-header card-header-warning">
                <h3>1st Section Banner</h3>
            </div>
            <div class="card-body">
                
                  
                    
                      {!! Form::open(['route' => 'banner.store' , 
                                  'class'=>'text-center' , 
                                  'method' => 'post' ,
                                  'id' => 'form4' ,
                                  'enctype' =>'multipart/form-data'
                                  ]) !!}
                       {{  @csrf_field()}}
                       <input type="hidden" value="section-1" name="position" />
                       <input type="hidden" value="long" name="type" />

                      <div class="input-group">
                          <div class="custom-file">
                            {!! Form::file('banner_image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}
                            {!! Form::label('banner_image' , 'Choose Long Ad', ['class'=>'custom-file-label']) !!}
                          </div>
                      </div>
                      <button class="btn btn-md btn-primary" id="sliderlongbanner">Upload</button>
                      {!! Form::close() !!}


               

                <div class="row">
                  @if(count($sectionFirstBanner) > 0)
                    @foreach($sectionFirstBanner as $banner)
                      <div class="col-md-4 ad-col">


                        <input type="text" class="link form-control mb-1" placeholder="Link...." value="{{ $banner->link }}" name="link">
                         <input type="text" name="id" value="{{ $banner->id }}" hidden="hidden">
                        <img src="{{ asset('/photos/1/banner/section-1/'.$banner->image) }}" class="img-fluid">
                        <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn  {{ $banner->status==1?'btn-primary':'btn-default' }}  live" value="{{ $banner->id }}" name="live">
                              Live
                          </button>
                          <button type="button" class="btn {{ $banner->status==0?'btn-primary':'btn-default' }}  offline " value="{{ $banner->id }}" name="offline">
                              Offline
                          </button>
                          <button type="button" class="btn btn-default save">Save</button>
                        </div>

                      </div>
                    @endforeach
                  @else
                    <p>No Long Ads</p>

                  @endif
                </div>
            </div>
          </div>
        </div>
        <!-- 1st section banner -->



        <!-- 2nd section banner -->
        <div class="col-md-12 pb-4">
          <div class="card">
            <div class="card-header card-header-info">
               <h3>2nd Section Banner</h3>
            </div>
            <div class="card-body">
              

              <div class="row">
                <div class="col-md">
                  <p class="text-danger">Long Ad</p>
                   {!! Form::open(['route' => 'banner.store' , 
                            'class'=>'text-center' , 
                            'method' => 'post' ,
                            'id' => 'form5' ,
                            'enctype' =>'multipart/form-data'
                            ]) !!}
                     {{  @csrf_field()}}

                   <!--        change this filed             -->

                   <input type="hidden" value="section-2" name="position" />
                   <input type="hidden" value="long" name="type" />

                    <div class="input-group">
                        <div class="custom-file">
                          {!! Form::file('banner_image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}
                          {!! Form::label('banner_image' , 'Choose Long Ad', ['class'=>'custom-file-label']) !!}
                        </div>
                    </div>
                    <button class="btn btn-md btn-primary" id="sliderlongbanner">Upload</button>
                    {!! Form::close() !!}
                </div>
                <div class="col-md">
                    <p class="text-danger">Short Ad</p>
                    {!! Form::open(['route' => 'banner.store' , 
                             'class'=>'text-center' , 
                             'method' => 'post' ,
                             'id' => 'form6' ,
                             'enctype' =>'multipart/form-data'
                             ]) !!}
                      {{  @csrf_field()}}

                    <!--        change this filed             -->

                    <input type="hidden" value="section-2" name="position" />
                    <input type="hidden" value="short" name="type" />

                     <div class="input-group">
                         <div class="custom-file">
                           {!! Form::file('banner_image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}
                           {!! Form::label('banner_image' , 'Choose Short Ad', ['class'=>'custom-file-label']) !!}
                         </div>
                     </div>
                     <button class="btn btn-md btn-primary" id="sliderlongbanner">Upload</button>
                     {!! Form::close() !!}
                </div>
              </div>
               


               

                <div class="row">
                  @if(count($sectionSecondLongBanner) > 0)
                    @foreach($sectionSecondLongBanner as $banner)
                      <div class="col-md-4 ad-col">


                        <input type="text" class="link form-control mb-1" placeholder="Link...." value="{{ $banner->link }}" name="link">
                         <input type="text" name="id" value="{{ $banner->id }}" hidden="hidden">
                        <img src="{{ asset('/photos/1/banner/section-2/'.$banner->image) }}" class="img-fluid">
                        <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn  {{ $banner->status==1?'btn-primary':'btn-default' }}  live" value="{{ $banner->id }}" name="live">
                              Live
                          </button>
                          <button type="button" class="btn {{ $banner->status==0?'btn-primary':'btn-default' }}  offline " value="{{ $banner->id }}" name="offline">
                              Offline
                          </button>
                          <button type="button" class="btn btn-default save">Save</button>
                        </div>

                      </div>
                    @endforeach
                  @else
                    <p>No Long Ads</p>
                  @endif
                </div>

                <hr>

                <div class="row">
                  @if(count($sectionSecondShortBanner) > 0)
                    @foreach($sectionSecondShortBanner as $banner)
                      <div class="col-md-4 ad-col">


                        <input type="text" class="link form-control mb-1" placeholder="Link...." value="{{ $banner->link }}" name="link">
                        <img src="{{ asset('/photos/1/banner/section-2/'.$banner->image) }}" class="img-fluid">
                         <input type="text" name="id" value="{{ $banner->id }}" hidden="hidden">
                        <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn  {{ $banner->status==1?'btn-primary':'btn-default' }}  live" value="{{ $banner->id }}" name="live">
                              Live
                          </button>
                          <button type="button" class="btn {{ $banner->status==0?'btn-primary':'btn-default' }}  offline " value="{{ $banner->id }}" name="offline">
                              Offline
                          </button>
                          <button type="button" class="btn btn-default save">Save</button>
                        </div>

                      </div>
                    @endforeach
                  @else
                    <p>No Short Ads</p>
                  @endif
                </div>

            </div>
          </div>
        </div>
        <!-- 2nd section banner -->


        <!-- 3rd section banner -->
        <div class="col-md-12 pb-4">
          <div class="card">
            <div class="card-header card-header-warning">
                <h3>3rd Section Banner</h3>
            </div>
            <div class="card-body">
                
                  
                    
                      {!! Form::open(['route' => 'banner.store' , 
                                  'class'=>'text-center' , 
                                  'method' => 'post' ,
                                  'id' => 'form7' ,
                                  'enctype' =>'multipart/form-data'
                                  ]) !!}
                       {{  @csrf_field()}}
                       <input type="hidden" value="section-3" name="position" />
                       <input type="hidden" value="short" name="type" />

                      <div class="input-group">
                          <div class="custom-file">
                            {!! Form::file('banner_image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}
                            {!! Form::label('banner_image' , 'Choose Short Ad', ['class'=>'custom-file-label']) !!}
                          </div>
                      </div>
                      <button class="btn btn-md btn-primary" id="sliderlongbanner">Upload</button>
                      {!! Form::close() !!}


               

                <div class="row">
                  @if(count($sectionThirdBanner) > 0)
                    @foreach($sectionThirdBanner as $banner)
                      <div class="col-md-4 ad-col">

                    
                     

                          <input type="text" class="link form-control mb-1" placeholder="Link...." value="{{ $banner->link }}" name="link">
                          <img src="{{ asset('/photos/1/banner/section-3/'.$banner->image) }}" class="img-fluid">
                          <input type="text" name="id" value="{{ $banner->id }}" hidden="hidden">

                          <div class="btn-group" role="group" aria-label="Basic example">

                            <button type="button" class="btn  {{ $banner->status==1?'btn-primary':'btn-default' }}  live" value="{{ $banner->id }}" 
                              name="live">
                                Live
                            </button>
                            <button type="button" class="btn {{ $banner->status==0?'btn-primary':'btn-default' }}  offline " value="{{ $banner->id }}" name="offline">
                                Offline
                            </button>
                            <button type="button" class="btn btn-default save">Save</button>

                          </div>


                      </div>
                    @endforeach
                  @else
                    <p>No Short Ads</p>

                  @endif
                </div>
            </div>
          </div>
        </div>
        <!-- 3rd section banner -->



         <!-- last section banner -->
        <div class="col-md-12 pb-4">
          <div class="card">
            <div class="card-header card-header-warning">
                <h3>4th Section Banner</h3>
            </div>
            <div class="card-body">
                
                  
                    
                      {!! Form::open(['route' => 'banner.store' , 
                                  'class'=>'text-center' , 
                                  'method' => 'post' ,
                                  'id' => 'form4' ,
                                  'enctype' =>'multipart/form-data'
                                  ]) !!}
                       {{  @csrf_field()}}
                       <input type="hidden" value="section-4" name="position" />
                       <input type="hidden" value="long" name="type" />

                      <div class="input-group">
                          <div class="custom-file">
                            {!! Form::file('banner_image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}
                            {!! Form::label('banner_image' , 'Choose Long Ad', ['class'=>'custom-file-label']) !!}
                          </div>
                      </div>
                      <button class="btn btn-md btn-primary" id="sliderlongbanner">Upload</button>
                      {!! Form::close() !!}


               

                <div class="row">
                  @if(count($sectionFourthBanner) > 0)
                    @foreach($sectionFourthBanner as $banner)
                      <div class="col-md-4 ad-col">


                        <input type="text" class="link form-control mb-1" placeholder="Link...." value="{{ $banner->link }}" name="link">
                         <input type="text" name="id" value="{{ $banner->id }}" hidden="hidden">
                        <img src="{{ asset('/photos/1/banner/section-4/'.$banner->image) }}" class="img-fluid">
                        <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn  {{ $banner->status==1?'btn-primary':'btn-default' }}  live" value="{{ $banner->id }}" name="live">
                              Live
                          </button>
                          <button type="button" class="btn {{ $banner->status==0?'btn-primary':'btn-default' }}  offline " value="{{ $banner->id }}" name="offline">
                              Offline
                          </button>
                          <button type="button" class="btn btn-default save">Save</button>
                        </div>

                      </div>
                    @endforeach
                  @else
                    <p>No Long Ads</p>

                  @endif
                </div>
            </div>
          </div>
        </div>
        <!-- last section banner -->






	    </div>  <!-- container end -->
	  </div>    <!-- content end -->

	    @include('backend/partials/footer')
	</div>
    
@endsection


@section('script')

  <!-- <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script> -->

<!--               for lfm stand alone      -->
  <!--
<script>
  $(document).ready(function () {

    $('.lfm').filemanager('image');

    $('.lfm').click(function(event) {
      
        if($('.thumbnail') == '')
        {
          console.log('cuk');
          console.log($(this).parent($(this)).parent());
        }

    });


    $('#thumbnail').change(function(event) {
        var a = $(this).val();
        console.log(a);

    });
 
  });
</script>
-->
<script>
$(".live").click(function(event){  
  const clickedElement = $(event.target);
  var id = $(this).val();
  console.log(id);  
  
  $.ajax({
    url: '{{ route('status.change') }}',
    data:{id:id,name:'live'},
    'dataType':'json',
    type: 'GET',

     success: function(response){   
        console.log(response.success);
        clickedElement.removeClass('btn-default');
        clickedElement.next().removeClass('btn-primary');
        clickedElement.next().addClass('btn-default');
        clickedElement.addClass("btn-primary");

         // success message
           $.notify({

                message: "BannerAd Status Changed Successfully"
              }
              ,{
                type:"primary",
                delay:1000,
                timer:200

            });
     }

  });
});

$(".offline").click(function(event){  
  var id = $(this).val();
  const clickedElement = $(event.target);
  console.log(id);  
  
  $.ajax({
    url: '{{ route('status.change') }}',
    data:{id:id,name:'offline'},
    'dataType':'json',
    type: 'GET',

     success: function(response){   
        console.log(response.success);
         clickedElement.removeClass('btn-default');
        clickedElement.addClass("btn-primary");
          clickedElement.prev().removeClass('btn-primary');
          clickedElement.prev().addClass('btn-default');

          // success message
           $.notify({

                message: "BannerAd Status Changed Successfully"
              }
              ,{
                type:"info",
                delay:1000,
                timer:200

          });
     }

  });

});


$(".save").on('click',function(event){ 

  var link = $(this).closest('div.ad-col').find("input[name='link']").val();
  var id = $(this).closest('div.ad-col').find("input[name='id']").val();

  if(link != '')
  {

      $.ajax({
        url: '{{ route('link.save') }}',
        data:{id:id,link:link},
        type: 'GET',

         success: function(response){   
            console.log(response.success);
          
              // success message
               $.notify({

                    message: "BannerAd Link Saved Successfully"
                  }
                  ,{
                    type:"info",
                    delay:1000,
                    timer:200

              });
         }

      });

  }

});


</script>

@endsection
