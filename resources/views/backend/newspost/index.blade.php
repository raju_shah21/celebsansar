@extends('backend.layout.app')


@section('title','Posts')

@section('content')




         

      @include('backend.partials.sidebar')


      
    <div class="main-panel">
      <!-- Navbar -->
      @include('backend.partials.navbar')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="text-center">
            <a class="btn btn-secondary btn-round" href="{{ route('newspost.create') }}">Add New Post</a>
            <a class="btn btn-primary btn-round">All Post</a>
            <a class="btn btn-info btn-round">My Post</a>
            <a class="btn btn-danger btn-round">Live Post</a>




            @if(request()->session()->has('success_message'))
              <div class="alert alert-primary mx-auto alert-dismissable col-md-6 col-md-offset-2">
                     <a href="#" class="close " data-dismiss="alert" aria-label="close">
                      <i class="pe-7s-close" style="font-size:30px;font-weight:bold;color:black"></i>
                       </a>
                        <strong>Success!</strong> {!! request()->session()->get('success_message') !!}

              </div>

              @endif
          </div>


          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">All Posts</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-hover">
                    <thead class=" text-primary">
                      <tr><th>
                        ID
                      </th>
                      <th>
                        Post Title
                      </th>
                      <th>
                        Feature Image
                      </th>
                      <th>
                        Author
                      </th>
                      <th>
                        Status
                      </th>
                      <th>
                        Action
                      </th>
                    </tr></thead>
                    <tbody>
                      @foreach($posts as $id => $post)
                      <tr>
                        <td>
                          {{ $post->id }}
                        </td>
                        <td>
                          {{ $post->title }}
                        </td>
                        <td>
                          <img src="{{ asset('dashboard/post/' .  $post->feature_image) }}" alt="" width="130px">
                        </td>
                        <td>
                          {{ $post->author }}
                        </td>
                        <td class="bg-secondary text-center" style="color:#fff;">
                          Live
                        </td>
                        <td>
                          <a href="{{ route('newspost.edit',$post->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>

 
                          <a  href="{{ route('newspost.delete',$post->id) }}" class="btn btn-danger btn-sm btn-delete" id="btn-delete-m"><i class="fa fa-trash-o"></i></a>
                        </td>
                      </tr>

                      @endforeach
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      

      {{-- footer included --}}
      @include('backend/partials/footer')

    
  </div>

  

 @endsection


 @section('script')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script>
  
    $("#btn-delete-m").click(function(e){
        e.preventDefault();
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).attr('href');
            console.log($(this).attr('href'));
           /**   Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            ) **/
          } else if (
            // Read more about handling dismissals
            result.dismiss === Swal.DismissReason.cancel
          ) {
             Swal.fire(
              'Cancelled',
              'Your Post is safe :)',
              'error'
            )
          }
        })

    });
         
</script>
 @endsection