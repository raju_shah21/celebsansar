@extends('backend.layout.app')


@section('title','Posts | Create')

@section('style')

  
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>  
@endsection

     

@section('content')    

    @include('backend.partials.sidebar')


      
    <div class="main-panel">



      <!-- Navbar -->
      @include('backend.partials.navbar')
      <!-- End Navbar -->



      <div class="content">
        <div class="container-fluid">
            
              <div class="card">

                    <div class="card-header card-header-primary">
                      <h4 class="card-title ">Create Post</h4>
                    </div>
                <div class="row">
                  <div class="col-md-8">
                    <div class="card-body">



                    
                        {!! Form::open(['route' => 'newspost.store' , 
                            'class'=>'text-center' , 
                            'method' => 'post' ,
                            'id' => 'form1' ,
                            'enctype' =>'multipart/form-data'
                            ]) !!}
                        {{  @csrf_field()}}

                        <div class="form-group bmd-form-group">
                          {!!  Form::label('title', 'Title', ['class' => 'bmd-label-floating']) !!}
                               {!! Form::text('title', null , ['class' => 'form-control' ]) !!}
                        </div>
                        <div class="form-group bmd-form-group">

                          {!!  Form::label('subtitle', 'Subtitle', ['class' => 'bmd-label-floating']) !!}
                               {!! Form::text('subtitle', null , ['class' => 'form-control' ]) !!}
                        </div>
                        <div class="form-group bmd-form-group">
                          {!!  Form::label('author', 'Author', ['class' => 'bmd-label-floating']) !!}
                               {!! Form::text('author',  'सेलेवसंसार'  , ['class' => 'form-control' ]) !!}
                        </div>
                        <div class="form-group md-form-group">
                          <!-- <label class="bmd-label-floating">Highlight</label>
                           <textarea type="text" id="materialContactFormMessage" class="form-control md-textarea" rows="3" name="highlight"></textarea> -->
                           {!!  Form::label('highlight', 'Highlight', ['class' => 'bmd-label-floating']) !!}
                           {!! Form::textarea('highlight', null , ['class' => 'form-control md-textarea' , 'rows'=>3]) !!}
                       </div>



                        <div class="input-group mt-5 mb-5">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="upload">Upload</span>
                          </div>

                          <div class="custom-file">
                            {!! Form::file('image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}

                            {!! Form::label('image' , 'Choose Image', ['class'=>'custom-file-label']) !!}
                          </div>
                        </div>







                        <div class="form-group md-form-group">
                         
                           <!-- {!!  Form::label('category', null, ['class' => 'bmd-label-floating']) !!} -->
                           {!! Form::select('category[]', (['0'=>'Choose a category']+$categories), null,['class'=>'browser-default custom-select','multiple'=>'multiple'])  !!}
                           
                       </div>
                       
                        <div>
                          {!!  Form::label('paragraph', 'Paragraph') !!}
                        </div>
                        <div class="form-group md-form-group">
                        
                            {!! Form::textarea('paragraph', null , ['class' => 'form-control md-textarea my-editor']) !!}
                            
                       </div>







                     


                      <!-- </form> -->
                      {!! Form::close() !!}

                      
                    
                   
                    </div> <!-- card body end -->
                </div>   <!-- col end-->

             
              </div>  <!-- row end -->
            </div> <!-- card end -->
              


              <!-- vertical fixed group btn -->

            <div class="btn-group-vertical fixed-btn" role="group" aria-label="Vertical button group">
              <button type="submit" class="submit-btn btn btn-md btn-primary ml-0 " name="publish" id="publish-btn">Publish Now</button>
              <button type="submit" class="submit-btn btn btn-md btn-primary " name="draft" id="draft-btn">Save To Draft</button>
              <button type="submit" class="submit-btn btn btn-md btn-primary " name="publishlater" id="publishlater-btn">Publish Later</button>
            </div>
              <!-- vertical fixed group btn -->



        </div>  <!-- container end -->
      </div>  <!-- content end -->


      @include('backend/partials/footer')

    </div>
 
 


@endsection



@section('script')

     <!-- editor -->

      <script>
        var editor_config = {
          path_absolute : "/",
          selector: "textarea.my-editor",
          plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
          ],
          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
          relative_urls: false,
          file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
              cmsURL = cmsURL + "&type=Images";
            } else {
              cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
              file : cmsURL,
              title : 'Filemanager',
              width : x * 0.8,
              height : y * 0.8,
              resizable : "yes",
              close_previous : "no"
            });
          }
        };

        tinymce.init(editor_config);
      </script>

      <!-- select 2 js -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

      <script>
        $(document).ready(function() {
         

          $('.custom-select').select2({
            placeholder: 'Choose Category',
              closeOnSelect: false
          });

        });
      </script>



      
      <script type="text/javascript">
        document.getElementById("publish-btn").onclick = function() {
          document.getElementById("form1").submit();
        }
        document.getElementById("draft-btn").onclick = function() {
          document.getElementById("form1").submit();
        }
        document.getElementById("publishlater-btn").onclick = function() {
          document.getElementById("form1").submit();
        }

      </script>


@endsection
