@extends('backend.layout.app')



@section('title','Menu Builder')


@section('style')

<style>
    .jumbotron{
      background:#FFFFFF;
    }

</style>



@endsection





@section('content')




         

      @include('backend.partials.sidebar')


      
    <div class="main-panel">
      <!-- Navbar -->
      @include('backend.partials.navbar')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="jumbotron">
  

            @include('backend.partials.flash_message')

            <div class="row">
             
                <div class="col-md-3 ml-2">
                 
                    <h2>Categories</h2>

                    <!--      category list        -->
               
                      <form id="categoryMenu" method="post" action="{{ route('menubuilder.add') }}">
                       
                         
                          @foreach($categories as $category)
                        
                          <div class="form-check">
                            <label class="form-check-label">

                              <input type="checkbox" class="form-check-input" 
                              value="{{ $category->id }}" name="category[]"
                                
                              >
                                     {{ $category->category }}
                              <span class="form-check-sign">
                                <span class="check"></span>
                              </span>
                            </label>
                          </div>

                          @endforeach
                        
                      
                   
                          <button type="submit" class="btn btn-primary mt-5" id="sendMenu">Send To Menu</button>
                      </form>
                  
                </div>



                <!--        menu order list       -->

              <div class="col-md-6 mx-auto mt-1 p-3" style="background-color: #fff; box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14)">
                <h3 class="text-center font-weight-bold">Menu Order</h3>
                  <ul id="sortable-1" class="list-group text-center mb-5">

                  @foreach($menus as $menu)
                       <li class = "default list-group-item mb-1" value="{{ $menu->id }}" id="{{ $menu->id }}">
                              {{ $menu->title }}

                              <a href="{{ route('menubuilder.delete',$menu->id) }}" class="float-right"><i class="material-icons ">
                                delete
                                 </i>
                              </a>
                        </li>
                  @endforeach
                   

                  </ul>

                <!--   <div class="text-center">
                    <button class="btn btn-primary text-center" id="updateOrder">Update</button>
                  </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
      

      {{-- footer included --}}
      @include('backend/partials/footer')

    
  </div>

  

 @endsection



 @section('script')
 <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<!-- loading category to menubuilder -->
 <script>
        $(function () {
            $('#categoryMenu').submit(function (e) {
                e.preventDefault()  // prevent the form from 'submitting'

                var url = e.target.action;  // get the target
               // var formData = $( "input[name^=category]:checked" ).serialize(); // get form data

                //console.log(formData);
                //console.log(url);

                var array_values = [];
                $('input[type=checkbox]').each( function() {
                    if( $(this).is(':checked') ) {
                        array_values.push( $(this).val() );
                    }
                });



                $.ajax({

                            type: 'POST' ,
                            url: '{{ route('menubuilder.add') }}' ,
                            data: {
                                  category:array_values ,
                                   _token: '{{ csrf_token() }}'
                            } ,
                             'dataType' : 'json', 
                            success: function(response){   

                              console.log(response.html);


                             // convert first json into object
                               var data = JSON.parse(response);  // data is object

                              $("#sortable-1 li:last").after(data.html);   // append to last 


                              //  Successfully order updated message or error msg
                              if(data.already_exists){

                                $.notify({
                                  message: "MenuOrder Already Exists"
                                },
                                {
                                  type:"danger",
                                });

                              }else{

                                  $.notify({
                                    message: "MenuOrder Added there Successfully"
                                    },{
                                    type:"primary",
                                  });
                              }

                              //----------- removed all checked items from category list------------------
                              $('input[type=checkbox]').each( function() {
                                  if( $(this).is(':checked') ) {
                                      $(this).prop('checked',false);
                                  }
                              });

                            }

                }); // ajax end


               
              
              
            })
        })
    </script>



<!-- for sorting and saving order of menu/navbar -->
 <script>
    $(function() {
    
       $( "#sortable-1" ).sortable({

          update:function(event,ui){

              var page_id_array = new Array();
              $("#sortable-1 li").each(function(){
                    page_id_array.push($(this).attr("id"));
                    console.log(page_id_array);
                });



             
         //     $("#updateOrder").click(function(){    


                    $.ajax({

                                type: 'POST' ,
                                url: '{{ route('menubuilder.store') }}' ,
                                data: {
                                      page:page_id_array ,
                                       _token: '{{ csrf_token() }}'
                                } ,
                                 'dataType' : 'json', 
                                success: function(response){   

                                  console.log(response.success);

                                  //  Successfully order updated message

                                          $.notify({

                                              message: "MenuOrder Updated Successfully"
                                            }
                                            ,{
                                              type:"primary",
                                              delay:1000,
                                              timer:200

                                          });
                                    

                                    }

                            

                    }); // ajax end


            // }); 
              // on click end
              


            
          }// update function end


        });
    });
 </script>
 <!-- for sorting and saving order of menu/navbar -->
 @endsection







