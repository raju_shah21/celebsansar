@foreach($rows as $key=>$row)
<li class="default list-group-item" value="{!! $row->slug !!}" id="{!! $row->id !!}">
      {!! $row->category !!}  
      <a href="{{ route('menubuilder.delete',$row->id) }}" class="float-right"><i class="material-icons ">
        delete
         </i>
      </a>
</li>
@endforeach
