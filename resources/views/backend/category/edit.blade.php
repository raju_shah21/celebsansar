@extends('backend.layout.app')

@section('title' ,'Category | Edit')



@section('content')
         

    @include('backend.partials.sidebar')


      
    <div class="main-panel">
      <!-- Navbar -->
      @include('backend.partials.navbar')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="text-center">
            
                  <button type="button" class="btn btn-danger btn-round" data-toggle="modal" data-target="#createModal" data-whatever="@mdo">
                    
                       Edit Category
                  </button>

                   @include('backend/partials/error_message')
            
          </div>


        


          <div class="col-md-10 mx-auto">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">Edit Category</h4>
              </div>
              <div class="card-body mt-4">
                  {!! Form::model($row ,['route' => ['category.update',$row["id"]] , 
                      'class'=>'text-center' , 
                      'method' => 'post' ,
                      'id' => 'form1' ,
                      ]) !!}
                  {{  @csrf_field()}}

                  <div class="form-group bmd-form-group">

                    {!!  Form::label('category', 'Category', ['class' => 'bmd-label-floating']) !!}
                         {!! Form::text('category', null , ['class' => 'form-control' ]) !!}
                  </div>
                  <div class="form-group bmd-form-group">

                    {!!  Form::label('slug', 'Slug', ['class' => 'bmd-label-floating']) !!}
                         {!! Form::text('slug', null , ['class' => 'form-control' ]) !!}
                  </div>


                  <a href="{{ route('category.show') }}" class="btn btn-danger">Back</a>
                  {!! Form::submit('Update',['class'=>'btn btn-primary']) !!}


              </div>    <!-- card body end -->
            </div>
          </div>
        </div>
      </div>

     @include('backend/partials/footer')
    
  </div>





 
@endsection