@extends('backend.layout.app')

@section('title' ,'Category')



@section('content')


  


         

      @include('backend.partials.sidebar')


      
      <div class="main-panel">
          <!-- Navbar -->
          @include('backend.partials.navbar')
          <!-- End Navbar -->
          <div class="content">
            <div class="container-fluid">
              <div class="text-center">
                
                      <button type="button" class="btn btn-danger btn-round" data-toggle="modal" data-target="#createModal" data-whatever="@mdo">
                        
                           Add New Category
                      </button>


                        @include('backend/partials/flash_message')
                       
                       
              </div>



              {{-- form modal for adding category --}}
              <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="exampleModalLabel">Add Category</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                        {!! Form::open(['route' => 'category.store' , 
                            'method' => 'post' ,
                            ]) !!}
                        {{  @csrf_field()}}
                    <div class="modal-body">
                     <!--  <form> -->
                        <div class="md-form">
                          {!!  Form::label('category', 'Category', ['class' => 'bmd-label-floating']) !!}
                          {!! Form::text('category', null , ['class' => 'form-control' ]) !!}

                        </div>
                        <div class="md-form">
                          {!!  Form::label('slug', 'Slug', ['class' => 'bmd-label-floating']) !!}
                          {!! Form::text('slug', null , ['class' => 'form-control' ]) !!}

                        </div>
                      <!-- </form> -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                      {!! Form::close() !!}
                  </div>
                </div>
              </div>

              <!-- form model end -->



            


              <div class="col-md-12">
                <div class="card">
                  <div class="card-header card-header-primary">
                    <h4 class="card-title ">All Category</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead class=" text-primary">
                          <tr><th>
                            ID
                          </th>
                          <th>
                            Category
                          </th>
                          <th>
                            Slug
                          </th>
                          <th>
                            Action
                          </th>
                        </tr></thead>
                        <tbody>

                          @foreach($categories as $category)
                          <tr>
                            <td>
                              {{ $category->id }}
                            </td>
                            <td>
                              {{ $category->category }}
                            </td>
                            <td>
                             {{ $category->slug }}
                            </td>
                            <td style="color:white;">

                                 

                                 <a class="btn btn-primary btn-sm editcategory" 
                                  href="{{ route('category.edit',$category->id) }}" >
                                  <i class="fa fa-pencil"></i><div class="ripple-container"></div></a>

                                  
                                 <a class="btn btn-danger btn-sm deletecategory" 
                                  href="{{ route('category.delete',$category->id) }}">
                                <i class="fa fa-trash-o"></i></a>
                            </td>
                          </tr>
                          @endforeach
                         
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>      <!-- container-fluid end -->
          </div>      <!-- cotent end -->

          @include('backend/partials/footer')

      </div> <!-- main-panel end -->  

  

 
@endsection


      
