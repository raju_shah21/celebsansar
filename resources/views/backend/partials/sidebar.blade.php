<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('dashboard/assets/img/sidebar-1.jpg') }}">
    
  <div class="logo">
    <a href="{{ route('front') }}" class="simple-text logo-normal">
      CelebSansar
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li {!! request()->is('backend') ? 'class="active"':'' !!} class="nav-item">
        <a class="nav-link" href="{{ route('dashboard') }}">
          <i class="material-icons">dashboard</i>
          <p>Dashboard</p>
        </a>
      </li>
      <li {!! request()->is('backend/user*') ? 'class="active"':'' !!}class="nav-item ">
        <a class="nav-link" href="{{ route('user.show') }}">
          <i class="material-icons">person</i>
          <p>User list</p>
        </a>
      </li>
      <li  {!! request()->is('backend/newspost*') ? 'class="active"':'' !!} class="nav-item">
        <a class="nav-link" href="{{ route('newspost.show') }}">
          <i class="material-icons">content_paste</i>
          <p>News Post</p>
        </a>
      </li>
      <li {!! request()->is('backend/category*') ? 'class="active"':'' !!}  class="nav-item ">
        <a class="nav-link" href="{{ route('category.show') }}">
          <i class="material-icons">library_books</i>
          <p>Category</p>
        </a>
      </li>
      <li  {!! request()->is('backend/menubuilder*') ? 'class="active"':'' !!} class="nav-item ">
        <a class="nav-link" href="{{ route('menubuilder') }}">
          <i class="material-icons">bubble_chart</i>
          <p>MenuBuilder</p>
        </a>
      </li>
      <li {!! request()->is('backend/video*') ? 'class="active"':'' !!} class="nav-item ">
        <a class="nav-link" href="{{ route('video') }}">
          <i class="material-icons">video_call</i>
          <p>Video</p>
        </a>
      </li>
      <li {!! request()->is('backend/banner*') ? 'class="active"':'' !!} class="nav-item ">
        <a class="nav-link" href="{{route('banner')}}">
          <i class="material-icons">notifications</i>
          <p>BannerAd Mangaer</p>
        </a>
      </li>
     
    </ul>
  </div>
</div>