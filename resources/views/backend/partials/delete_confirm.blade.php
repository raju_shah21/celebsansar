@section('script')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script>
  
    $(".btn-delete-m").click(function(e){
        e.preventDefault();
        console.log('click');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).attr('href');
            console.log($(this).attr('href'));
           /**   Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            ) **/
          } else if (
            // Read more about handling dismissals
            result.dismiss === Swal.DismissReason.cancel
          ) {
             Swal.fire(
              'Cancelled',
            )
          }
        })

    });
         
</script>
 @endsection