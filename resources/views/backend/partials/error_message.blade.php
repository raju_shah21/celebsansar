@if(count( $errors ) > 0 )
    @foreach ($errors->all() as $error) 
        	<div class="alert alert-danger alert-dismissable col-md-6 text-center offset-md-3">
                <div style="position:relative;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                        </button>
                {{$error}}
               </div>
            </div>
    @endforeach
  
@endif