<meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('dashboard/assets/img/apple-icon.png') }}">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
 
  <title>@yield('title')</title>
  
  <title>
    CelebSansar Dashboard
  </title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />

  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"> -->
  <link href="{{ asset('dashboard/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />

  <!-- CSS Files -->
  <link href="{{ asset('dashboard/assets/css/material-dashboard.css?v=2.1.1') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('dashboard/assets/demo/demo.css') }}" rel="stylesheet" />


      <!-- new css file -->
    <link href="{{ asset('dashboard/assets/css/style.css') }}" rel="stylesheet" />

   
      @yield('style')           <!-- for other custom css required by one or other file -->

