@if(request()->session()->has('success_message'))
	<div class="alert alert-info text-center alert-dismissable col-md-6 mx-auto">
				 <a href="#" class="close " data-dismiss="alert" aria-label="close">
					<i class="pe-7s-close" style="font-size:30px;font-weight:bold;color:black"></i>
					 </a>
			  		<strong>Success!</strong> {!! request()->session()->get('success_message') !!}

	</div>
@endif


@if(request()->session()->has('error_message'))
	<div class="alert alert-danger text-center alert-dismissable">
	  	<strong>Error!</strong> {!! request()->session()->get('error_message') !!}
	</div>
@endif