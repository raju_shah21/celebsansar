@extends('backend.layout.app')


@section('title','Video')

@section('content')

	

	@include('backend.partials.sidebar')



	<div class="main-panel">
	    <!-- Navbar -->
	    @include('backend.partials.navbar')
	    <!-- End Navbar -->
	  <div class="content">
	    <div class="container-fluid">
	      <div class="text-center">
	        
	             <a class="btn btn-primary btn-round" href="{{ route('video.create') }}">Add New Video</a>



	        
	              @if(request()->session()->has('success_message'))
	                <div class="alert alert-primary mx-auto alert-dismissable col-md-6 col-md-offset-2">
	                       <a href="#" class="close " data-dismiss="alert" aria-label="close">
	                        <i class="pe-7s-close" style="font-size:30px;font-weight:bold;color:black"></i>
	                         </a>
	                          <strong>Success!</strong> {!! request()->session()->get('success_message') !!}

	                </div>

	              @endif
	      </div>




	    


	      <div class="col-md-12">
	        <div class="card">
	          <div class="card-header card-header-primary">
	            <h4 class="card-title ">All Videos</h4>
	          </div>
	          <div class="card-body">
	            <div class="table-responsive">
	              <table class="table table-hover">
	                <thead class="text-primary">
	                  <tr><th>
	                    ID
	                  </th>
	                  <th>
	                    Title
	                  </th>
	                  <th>
	                    Image
	                  </th>
	                  <th>
	                    Action
	                  </th>
	                </tr></thead>
	                <tbody>

						
						@foreach ($rows as $row)
							<tr>
								<td>{{$row->id}}</td>
								<td> {{$row->title}} </td>
								<td><img src="{{asset('/dashboard/videos/'.$row->image)}}" width="200"></td>
								 <td>
									<a href="{{ route('video.edit',$row->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
									<a  class="btn btn-danger btn-sm btn-delete-m" href="{{ route('video.delete',$row->id) }}"
									 ><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						@endforeach
											
	                 
	                </tbody>
	              </table>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>

	  @include('backend/partials/footer')
	</div>
    
@endsection

	  @include('backend/partials/delete_confirm')



