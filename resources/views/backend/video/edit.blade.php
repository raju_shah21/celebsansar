@extends('backend.layout.app')


@section('title','Video | Add')

@section('style')
<style>
.alert{
    padding:10px 20px !important
}
</style>
@endsection
     

@section('content')    

    @include('backend.partials.sidebar')


      
    <div class="main-panel">



      <!-- Navbar -->
      @include('backend.partials.navbar')
      <!-- End Navbar -->



      <div class="content">
        <div class="container-fluid">
            
              <div class="card">

                    <div class="card-header card-header-primary">
                      <h4 class="card-title ">Edit Video</h4>
                    </div>
                <div class="row">
                  <div class="col-md-8">
                    <div class="card-body">

                         @if ($errors->has('title'))
                                    <div class="alert alert-danger text-center" style="border-radius:20px">
                                        <strong>Error!</strong> {!! $errors->first('title') !!}
                                    </div>
                         @endif
                         @if ($errors->has('url'))
                                    <div class="alert alert-danger text-center" style="border-radius:20px">
                                        <strong>Error!</strong> {!! $errors->first('url') !!}
                                    </div>
                         @endif

                    
                        {!! Form::model($row , ['route' => ['video.update',$row->id] , 
                            'class'=>'text-center' , 
                            'method' => 'post' ,
                            'id' => 'form1' ,
                            'enctype' =>'multipart/form-data'
                            ]) !!}
                        {{  @csrf_field()}}

                        <div class="form-group bmd-form-group">
                          {!!  Form::label('title', 'Title', ['class' => 'bmd-label-floating']) !!}
                               {!! Form::text('title', null , ['class' => 'form-control' ]) !!}
                              
                        </div>

                        <div class="form-group bmd-form-group">
                          {!!  Form::label('url', 'Video Url', ['class' => 'bmd-label-floating']) !!}
                               {!! Form::text('url', null , ['class' => 'form-control' ]) !!}
                        </div>
                      
                        <div class="form-group md-form-group">
                           {!!  Form::label('body', 'Body', ['class' => 'bmd-label-floating']) !!}
                           {!! Form::textarea('body', null , ['class' => 'form-control md-textarea' , 'rows'=>3]) !!}
                       </div>



                        <div class="input-group mt-5 mb-5">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="upload">Upload</span>
                          </div>

                          <div class="custom-file">
                            {!! Form::file('video_image' , ['class'=>'custom-file-input' ,'aria-describedby' => 'upload']) !!}

                            {!! Form::label('image' , 'Choose New Image', ['class'=>'custom-file-label']) !!}
                          </div>
                        </div>

                      
                        {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
                      

                      {!! Form::close() !!}

                      
                    
                   
                    </div> <!-- card body end -->
                </div>   <!-- col end-->

             
              </div>  <!-- row end -->
            </div> <!-- card end -->
              




        </div>  <!-- container end -->
      </div>  <!-- content end -->


      @include('backend/partials/footer')

    </div>
 
 


@endsection

