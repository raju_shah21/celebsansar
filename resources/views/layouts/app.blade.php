<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

          <!-- icon in titlebar -->
        <link rel="icon" type="image/ico" href="{{ asset('img/favicon.png') }}" />
        <title>
         @yield('title')
        </title>
           
       
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">


            @yield('meta')
        

        <!-- Styles -->
        <style>
            .navbar{
                padding: 0.5rem 0 !important;
            }
        </style>
      
    </head>
<body class="container-fluid">
    
        @yield('content')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>


    @yield('js')
</body>
</html>
